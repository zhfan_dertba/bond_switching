Bond Switching (for Si/a-SiO2 interface)
=======================

## Input files description

   - **temperature_table**

     This file describes the thermal control of the MC modeling. 

     e.g.

        8
        251   10000
        9214   7000
        16278   4900
        21253   3430
        33362   2401
        52374   1681
        91963   1177
        261519   824
 
     "8" is the number of set temperature. First column is number of steps. Second colum is the temperature used after the step index set in the first column. *Note*: from 1st step to step 251, every bond switching is accepted. 

   - **BONDS.000000.dat**

     This file describes the bonding topology, which is updated for every accepted step. Its format is describe the bonding and angle for every atom.

     e.g.

        513                     # 513: number of entries (should be same to number of atoms)
        1 14                    # 1: atom index, 14: atom type. Here, 14 is Si
        12 25 1                 # bond 1 => type 12: bond type Si-Si bond, 25: atom 1 bonds to atom 25, 1: this bond could be switched
        12 33 1                 # 
        11 489 1                # bond 3 => type 11: bond type Si-O bond, 489: atom 1 bonds to atom 489, 1: this bond could be switched
        11 506 1                #
        24 25 33 1              # angle 1 => type 24: angle type Si-Si-Si angle, atom 1 is in middle of atom 25 and atom 33, 1: no meaning
        23 25 489 1             # angle 2 => type 23: angle type Si-Si-O angle, atom 1 is in middle of atom 25 and atom 489, 1: no meaning
        23 25 506 1             #
        23 33 489 1             #
        23 33 506 1             #
        21 489 506 1            # angle 6 => type 21: angle type O-Si-O angle, atom 1 is in middle of atom 25 and atom 489, 1: no meaning  
        2 14
        12 22 1
        12 30 1
        ...
        507 8                   # 507: atom index, 8: atom type O
        11 2 1                  # bond 1 => type 11: bond type Si-O bond, atom 507 is bonded to atom 2, 1: this bond could be switched
        11 237 1
        22 2 237 1              # angle 1 => type 22: angle type Si-O-Si angle, atom 507 is in middle of atom 2 and atom 237, 1: no meaning
        508 8
        11 3 1
        11 239 1
        22 3 239 1
        ...

     *Note*: main program will check BONDS.000000.dat. It will check its consistency for every atom. You may want to correct the bug in BONDS.000000.dat and run main program repeately to **remove all the errors and warnings**.

   - **v**

     This file gives initial coordinates for the system. You can specify any filename for it.

     e.g.

         16.33508873  0.00000000  0.00000000                          # lattice vectors a1
         0.00000000  16.33508873  0.00000000                          # lattice vectors a2
         0.00000000  0.00000000  31.91459337                          # lattice vectors a3
         1.00  513                                                    # 1.00: no meaning, 513: number of atoms
         14   11.080159   16.133443   -0.080802    1    1    1        # atom type: 14: Si; 8: O, x/y/z coordinates (Angstrom unit), fix info: 1=> no fix, 0=> fix
         14    5.636023   16.135450   -0.079101    1    1    1
         14    5.640014   10.686771   -0.082535    1    1    1
         14   16.533901   16.133635   -0.084931    1    1    1
         14   16.532396    5.246467   -0.085830    1    1    1
         ...

## Run program

   e.g.

      ./mc_omp.x  v  0.004  0

   or if restart

      ./mc_omp.x  v  0.004  1  83282

   **v**: initial structure coordinate

   **0.004**: not used

   **0**: start from scratch

   or set to **1**: restart from step **83282**. program will try to find BONDS.083282.dat and v.083282 automatically.

## Pre-processing

   Here is a small script to generate BONDS.000000.dat based on a reasonable initial structure. This structure is best to be relaxed by DFT since the script justify the bonded atoms based on their bond length

   run (python2):

      python gen_bondss.py v > BONDS.000000.dat

   gen_bondss.py will also generate multiple warnings. These warning should have human input to remove.
   
## Cite

   You can cite the paper "Effects of the c-Si/a-SiO2 interfacial atomic structure on its band alignment: an ab initio study" (Phys. Chem. Chem. Phys., 2017, 19,32617) and some of its references. 
