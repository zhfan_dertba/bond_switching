      SUBROUTINE IONSD(IFLAG,NIONS,TOTEN,A,B,NFREE,POSION,POSIOC, &
     &      FACT,F,FACTSI,FSIF,FL,S,DISMAX,IU6,IU0, &
     &      EBREAK,EDIFFG,E1TEST,LSTOP2,dt,dt_trial,inode_tot,imov_at)
      !USE prec
      !USE lattice
      !USE ini
      !USE chain
!      use data_variable_1

      IMPLICIT REAL(8) (A-H,O-Z)
      
      !INTEGER, PARAMETER :: q =SELECTED_REAL_KIND(10)
      !INTEGER, PARAMETER :: qs=SELECTED_REAL_KIND(5)
      REAL POSOPT(3,NIONS),DPOS(3,NIONS)

      integer imov_at(3,NIONS)
      DIMENSION F(3,NIONS),FL(3,NIONS),S(3,NIONS)
      DIMENSION POSION(3,NIONS),POSIOC(3,NIONS)
      DIMENSION A(3,3),B(3,3)
      DIMENSION TMP(3)
      LOGICAL   LRESET,LTRIAL,LBRENT
      LOGICAL   LSTOP2

      SAVE  AC,FSIFL,SSIF
      DIMENSION AC(3,3),FSIFL(3,3),SSIF(3,3),FSIF(3,3)
      
      !new
      integer :: ionstep=0,correction_step=0
      SAVE ionstep,correction_step
      real*8 max_force

      SAVE TOTEN1,DMOVED,E1ORD1,ORTH,GAMMA,GNORM,GNORMF,GNORML
      SAVE GNORM1,GNORM2,STEP,CURVET,SNORM,SNORMOLD
      SAVE DMOVEL,LTRIAL,LBRENT
      DATA ICOUNT/0/, ICOUNT2/0/, STEP /1.00000/,SNORM/1E-10/
      DATA LTRIAL/.FALSE./
      
      ionstep=ionstep+1
      
      if (inode_tot.eq.1) then !ZHANGHUI CHEN
        if(ionstep==1) then
          open(unit=11,file="Progress_relax",status="replace")
        else
          open(unit=11,file="Progress_relax",status="old",position="append")
        endif
        write(11,55) ionstep,TOTEN
     55 format('The ',I3,' SCF step for ionic relaxation. Total Energy:',F10.5) 
        write(11,*) 'Position vector X= (in unit of direct lattice)'
        write(11,*) POSION
        write(11,*) 'Force Vector F= (in unit of eV/A with the factor 0.0964*ion_stepsize)'
        write(11,*) F
        close(11)
      endif
      
      !This is to determine the initial step size (ionstep==1) by the maximum of force
      if(ionstep==1) then
        max_force=0.0
        do NI=1,NIONS
          do I=1,3
            if(dabs(F(I,NI))>max_force) max_force=dabs(F(I,NI))
          enddo
        enddo
        !Maximum of movement: 0.3 Ang
        if(max_force>0.3) STEP=0.3/(max_force)
        if(max_force/FACT>100.0)then
          if (inode_tot.eq.1) then
             open(unit=11,file="Progress_relax",status="old",position="append")
             write(11,*) "***************************************************************************"
             write(11,*) "***************************************************************************"
             write(11,*) "Warning: too large force:", max_force/FACT,"eV/Ang"
             write(11,*) "You'd better give a good initial structure."
             write(11,*) "If you insist running this job, set a smaller value of ionic_stepsize"
             write(11,*) "***************************************************************************"
             write(11,*) "***************************************************************************"
             close(11)
          endif
        endif
      endif
      
      !open(123,file='POSOPT')
      !read(123,*) POSOPT
      !close(123)
      POSOPT = 0.0
!=======================================================================
!  if IFLAG =0 initialize everything
!=======================================================================
      IF (IFLAG==0) THEN
        DO NI=1,NIONS
        DO I=1,3
          S(I,NI) =0
          FL(I,NI)=0
        ENDDO
        ENDDO

        DO I=1,3
        DO J=1,3
          SSIF (I,J)=0
          FSIFL(I,J)=0
        ENDDO
        ENDDO
        LTRIAL=.FALSE.
        ICOUNT=0
        SNORM =1E-10
        SNORMOLD =1E10
        CURVET=0
      ENDIF

! if previous step was a trial step then continue with line minimization
      IF (LTRIAL) THEN
        GOTO 400
      ENDIF
!=======================================================================
!  calculate quantities necessary to conjugate directions
!=======================================================================
      GNORML=0
      GNORM =0
      GNORMF=0
      ORTH  =0
      IF (FACT/=0) THEN
      DO NI=1,NIONS
         GNORML = GNORML+1.00/FACT* &
     &    (FL (1,NI)*FL (1,NI)+FL(2,NI) *FL(2,NI) +FL(3,NI) *FL(3,NI))
         GNORM  = GNORM+ 1.00/FACT*( &
     &    (F(1,NI)-FL(1,NI))*F(1,NI) &
     &   +(F(2,NI)-FL(2,NI))*F(2,NI) &
     &   +(F(3,NI)-FL(3,NI))*F(3,NI))
         GNORMF = GNORMF+1.00/FACT* &
     &    (F(1,NI)*F(1,NI)+F(2,NI)*F(2,NI)+F(3,NI)*F(3,NI))
         ORTH   = ORTH+  1.00/FACT* &
     &    (F(1,NI)*S(1,NI)+F(2,NI)*S(2,NI)+F(3,NI)*S(3,NI))
      ENDDO
      ENDIF

      GNORM1=GNORMF

      IF (ABS(FACTSI)>1E-20) THEN
      DO I=1,3
      DO J=1,3
         GNORML=GNORML+ FSIFL(I,J)*FSIFL(I,J)/FACTSI
         GNORM =GNORM +(FSIF(I,J)-FSIFL(I,J))*FSIF(I,J)/FACTSI
         GNORMF=GNORMF+ FSIF(I,J)* FSIF(I,J)/FACTSI
         ORTH  =ORTH  + FSIF(I,J)* SSIF(I,J)/FACTSI
      ENDDO
      ENDDO
      ENDIF
      GNORM2=GNORMF-GNORM1

      !CALL  sum_chain( GNORM )
      !CALL  sum_chain( GNORML )
      !CALL  sum_chain( GNORMF)
      !CALL  sum_chain( GNORM1)
      !CALL  sum_chain( GNORM2)
      !CALL  sum_chain( ORTH )

!=======================================================================
!  calculate Gamma
!  improve line optimization if necessary
!=======================================================================
      IF (IFLAG==0) THEN
        ICOUNT=0
        GAMMA=0
      ELSE
!       this statement for Polak-Ribiere
        GAMMA=GNORM /GNORML
!       this statement for Fletcher-Reeves
!        GAMMA=GNORMF/GNORML
      ENDIF

      GAMMIN=1.00
      IFLAG=1
!      IF (inode_tot.eq.1) &
!     & WRITE(IU0,30)CURVET,CURVET*GNORMF,CURVET*(ORTH/SQRT(SNORM))**2

   30 FORMAT(' curvature: ',F6.2,' expect dE=',E10.3,' dE for cont linesearch ',E10.3)
! required accuracy not reached in line minimization
! improve line minimization
! several conditions must be met:

! orthonormality not sufficient
!      WRITE(0,*) ORTH, MAX(GAMMA,GAMMIN),GNORMF/5, ABS(CURVET*(ORTH/SQRT(SNORM))**2), LSTOP2
      IF (ABS(ORTH)*MAX(GAMMA,GAMMIN)>ABS(GNORMF)/5 &
! expected energy change along line search must be larger then required accuracy
     &    .AND. &
     &   ( (EDIFFG>0 .AND. &
     &       ABS(CURVET*(ORTH/SQRT(SNORM))**2)>EDIFFG) &
! or force must be large enough that break condition is not met
     &    .OR. &
     &     (EDIFFG<0 .AND..NOT.LSTOP2) &
     &   ) &
! last call must have been a line minimization
     &    .AND. LBRENT &
!      Weile Jia, comment out. no 4 line minization in the atomic relaxation.
!     &    .AND. (correction_step<4) &
     &  ) GOTO 400
     

!---- improve the trial step by adding some amount of the optimum step
      IF (ICOUNT/=0) STEP=STEP+0.2000*STEP*(DMOVEL-1)
!---- set GAMMA to (0._q,0._q) if line minimization was not sufficient
      IF (5*ABS(ORTH)*GAMMA>ABS(GNORMF)) THEN
         GAMMA=0
         ICOUNT=0
      ENDIF
!---- if GNORM is very small signal calling routine to stop
      IF (CURVET/=0 .AND.ABS((GNORMF)*CURVET*2)<EDIFFG) THEN
        IFLAG=2
      ENDIF

      ICOUNT=ICOUNT+1
      ICOUNT2=ICOUNT2+1
!-----------------------------------------------------------------------
! performe trial step
!-----------------------------------------------------------------------
      E1ORD1=0
      DMOVED=0
      SNORM =1E-10

!----- set GAMMA to (0._q,0._q) for initial steepest descent steps (Robin Hirschl)
!      have to discuss this
!      IF (ICOUNT2<=NFREE) GAMMA=0
      
      DO NI=1,NIONS
!----- store last gradient
        FL(1,NI)=F(1,NI)
        FL(2,NI)=F(2,NI)
        FL(3,NI)=F(3,NI)
!----- conjugate the direction to the last direction
!cccccccc this is the conjugate gradient algorithm, here we will turn it off by 
!cccccccc equaling S with F (force), so it will be a steepest decent. 
!        S(1,NI) = F(1,NI)+ GAMMA * S(1,NI)
!        S(2,NI) = F(2,NI)+ GAMMA * S(2,NI)
!        S(3,NI) = F(3,NI)+ GAMMA * S(3,NI)
!cccccccccccccc, steepest decent
        S(1,NI) = F(1,NI)
        S(2,NI) = F(2,NI)
        S(3,NI) = F(3,NI)
!ccccccccccccc  end, steepest decent

!-------  Zhang hui and Weile , 2014, 12, 12

        S(1, NI) = S(1, NI) * imov_at(1, NI)
        S(2, NI) = S(2, NI) * imov_at(2, NI)
        S(3, NI) = S(3, NI) * imov_at(3, NI)
        

        IF (FACT/=0) THEN
        SNORM = SNORM+1/FACT*(S(1,NI)*S(1,NI)+S(2,NI)*S(2,NI) +S(3,NI)*S(3,NI))
        ENDIF
      ENDDO
     
!!    deleted 40 lines.  
 
      DO I=1,3
         DO J=1,3
            FSIFL(I,J)=FSIF(I,J)
            AC(I,J)   = A(I,J)
        SSIF(I,J) = FSIF(I,J)+ GAMMA* SSIF(I,J)
      ENDDO
      ENDDO

      IF (ABS(FACTSI)>1E-20) THEN
         DO I=1,3
            DO J=1,3
               SNORM = SNORM  + 1/FACTSI *      SSIF(I,J)* SSIF(I,J)
            ENDDO
         ENDDO
      ENDIF

      !CALL  sum_chain( SNORM)
      
      if (inode_tot.eq.1) then 
        open(unit=11,file="Progress_relax",status="old",position="append")
        write(11,*) 'Search Vector S='
        write(11,*) S
        write(11,58) SNORM,SNORMOLD
     58 format('|S*S|=',F10.5,'   |SL*SL|=',F10.5)
        write(11,*) ''
      endif

!----- if SNORM increased, rescale STEP (to avoid too large trial steps) 
!      (Robin Hirschl)
      IF (SNORM>SNORMOLD) THEN
         STEP=STEP*(SNORMOLD/SNORM)
      ENDIF
      SNORMOLD=SNORM
      
      DO NI=1,NIONS
!----- search vector from cartesian to direct lattice
         TMP(1) = S(1,NI)
         TMP(2) = S(2,NI)
         TMP(3) = S(3,NI)
         CALL KARDIR(1,TMP,B)
         !if(inode_tot.eq.1) then
         !   WRITE(73,*) "Update in ",NI,"atom"
         !   WRITE(73,*) S(1,NI),S(2,NI),S(3,NI)
         !   WRITE(73,*) TMP
         !endif

!----- trial step in direct grid
         POSIOC(1,NI)= POSION(1,NI)
         POSIOC(2,NI)= POSION(2,NI)
         POSIOC(3,NI)= POSION(3,NI)

         POSION(1,NI)= TMP(1)*STEP+POSIOC(1,NI)
         POSION(2,NI)= TMP(2)*STEP+POSIOC(2,NI)
         POSION(3,NI)= TMP(3)*STEP+POSIOC(3,NI)
         DMOVED= MAX( DMOVED,S(1,NI)*STEP,S(2,NI)*STEP,S(3,NI)*STEP)

!----- keep ions in unit cell (Robin Hirschl)
         POSION(1,NI)= MOD(POSION(1,NI),1.000)
         POSION(2,NI)= MOD(POSION(2,NI),1.000)
         POSION(3,NI)= MOD(POSION(3,NI),1.000)

!----- force * trial step = 1. order energy change
         IF (FACT/=0) THEN
            E1ORD1= E1ORD1 - 1.0 * STEP / FACT * &
     &           (S(1,NI)*F(1,NI)+ S(2,NI)*F(2,NI) + S(3,NI)*F(3,NI))
         ENDIF
      ENDDO

      IF (ABS(FACTSI)>1E-20) THEN
      DO I=1,3
      DO J=1,3
         E1ORD1= E1ORD1 - STEP / FACTSI * SSIF(I,J)* FSIF(I,J)
      ENDDO
      ENDDO
      ENDIF

      DO J=1,3
         DO I=1,3
            A(I,J)=AC(I,J)
            DO K=1,3
               A(I,J)=A(I,J) + SSIF(I,K)*AC(K,J)*STEP
            ENDDO
         ENDDO
      ENDDO

      !CALL  sum_chain( E1ORD1 )

      LRESET = .TRUE.
      X=0
      Y=TOTEN
      FP=E1ORD1
      IFAIL=0

      CALL ZBRENT(IU0,LRESET,EBREAK,X,Y,FP,XNEW,XNEWH,YNEW,YD,IFAIL,inode_tot)
      DMOVEL=1

!      IF (inode_tot.eq.1) THEN
!         WRITE(IU0,10) GAMMA,GNORM1,GNORM2,ORTH,STEP
! 10      FORMAT(' trial: gam=',F8.5,' g(F)= ',E10.3, &
!     &        ' g(S)= ',E10.3,' ort =',E10.3,' (trialstep =',E10.3,')')
!         WRITE(IU0,11) SNORM
! 11      FORMAT(' search vector abs. value= ',E10.3)
!      ENDIF
      TOTEN1= TOTEN
      E1TEST=E1ORD1
      LTRIAL=.TRUE.
      dt=STEP
      dt_trial=STEP
      
      correction_step=0 !a new trial step
!      if (inode_tot.eq.1) then !ZHANGHUI CHEN
!        write(11,7337) ionstep,IFLAG
!   7337 format('This ',I3,' move is a Trial step with the following information: (IFLAG=',I1,')') 
!        write(11,59) STEP,DMOVEL
!     59 format('Step_size=',F8.5,'   DMOVEL=',F8.5)
!        write(11,60) X,Y,FP
!     60 format('X(e.g. A)=',F8.5,'   Y(YA)=',F10.5,'   FP(FA)=',F10.5)
!        write(11,61) Xnew,Ynew,YD
!     61 format('Xnew=',F8.5,'   Ynew=',F10.5,'   YD=',F10.5)
!        write(11,*) '-------------------------------------------------------------------------'
!        write(11,*) ''
!        write(11,*) ''
!        close(11)
!      endif
     
      RETURN
!=======================================================================
! calculate optimal step-length and go to the minimum
!=======================================================================
!-----------------------------------------------------------------------
!  1. order energy change due to displacement at the new position
!-----------------------------------------------------------------------
  400 CONTINUE
      E1ORD2=0
      IF (FACT/=0) THEN
      DO NI=1,NIONS
        E1ORD2= E1ORD2 - 1.0 * STEP / FACT * &
     &  (S(1,NI)*F(1,NI)+ S(2,NI)*F(2,NI) + S(3,NI)*F(3,NI))
      ENDDO
      ENDIF
      
      IFLAG=0
      if (inode_tot.eq.1) then !ZHANGHUI CHEN
        open(unit=11,file="Progress_relax",status="old",position="append")
        write(11,7373) ionstep,correction_step+1,IFLAG
   7373 format('This ',I3,' move is a ',I2,' Correction step with the following information: (IFLAG=',I1,')') 
        write(11,62) X,Y,FP
     62 format('X(e.g. B)=',F8.5,'   Y(YB)=',F10.5,'   FP(FB)=',F10.5)
      endif

      IF (ABS(FACTSI)>1E-20) THEN
      DO I=1,3
      DO J=1,3
        E1ORD2= E1ORD2 - STEP / FACTSI * SSIF(I,J)* FSIF(I,J)
      ENDDO
      ENDDO
      ENDIF

      !CALL  sum_chain( E1ORD2 )
!-----------------------------------------------------------------------
!  calculate position of minimum
!-----------------------------------------------------------------------
      CONTINUE
      LRESET = .FALSE.
      X=DMOVEL
      Y=TOTEN
      FP=E1ORD2
      IFAIL=0

      CALL ZBRENT(IU0,LRESET,EBREAK,X,Y,FP,XNEW,XNEWH,YNEW,YD,IFAIL,inode_tot)
!     estimate curvature
      CURVET=YD/(E1ORD2/STEP/SQRT(SNORM))**2

      DMOVE =XNEW
      DMOVEH=XNEWH

!    previous step was trial step than give long output
      DISMAX=DMOVE*DMOVED
      IF (LTRIAL) THEN
      LBRENT=.TRUE.
      E2ORD  = TOTEN-TOTEN1
      E2ORD2 = (E1ORD1+E1ORD2)/2
!      IF (inode_tot.eq.1) &
!      WRITE(IU0,45) E2ORD,E2ORD2,E1ORD1,E1ORD2
!   45 FORMAT(' trial-energy change:',F12.6,'  1 .order',3F12.6)

      E1TEST=TOTEN1-YNEW
      !DISMAX=DMOVE*DMOVED
!      IF (inode_tot.eq.1) &
!      WRITE(IU0,20) DMOVE*STEP,DMOVEH*STEP,DISMAX,YNEW,YNEW-TOTEN1
!   20 FORMAT(' step: ',F8.4,'(harm=',F8.4,')', &
!     &       '  dis=',F8.5,'  next Energy=',F13.6, &
!     &       ' (dE=',E10.3,')')

!      IF (GAMMA==0) THEN
!       IF ((IU6>=0) .and. (inode_tot .eq. 1)) &
!        WRITE(IU6,*)'Steepest descent step on ions:'
!      ELSE
!      IF ((IU6>=0) .and.(inode_tot .eq. 1)) &
!        WRITE(IU6,*)'Conjugate gradient step on ions:'
!      ENDIF

!      IF ((IU6>=0) .and. (inode_tot .eq. 1) ) &
!      WRITE(IU6,40) E2ORD,(E1ORD1+E1ORD2)/2,E1ORD1,E1ORD2, &
!     &         GNORM,GNORMF,GNORML, &
!     &         GNORM1,GNORM2,ORTH,GAMMA,STEP, &
!     &         DMOVE*STEP,DMOVEH*STEP,DISMAX,YNEW,YNEW-TOTEN1

!   40 FORMAT(' trial-energy change:',F12.6,'  1 .order',3F12.6/ &
!     &       '  (g-gl).g =',E10.3,'      g.g   =',E10.3, &
!     &       '  gl.gl    =',E10.3,/ &
!     &       ' g(Force)  =',E10.3,'   g(Stress)=',E10.3, &
!     &       ' ortho     =',E10.3,/ &
!     &       ' gamma     =',F10.5,/ &
!     &       ' trial     =',F10.5,/ &
!     &       ' opt step  =',F10.5,'  (harmonic =',F10.5,')', &
!     &       ' maximal distance =',F10.8/ &
!     &       ' next E    =',F13.6,'   (d E  =',F10.5,')')
      ELSE
!      IF (inode_tot.eq.1) &
!      WRITE(IU0,25) DMOVE*STEP,YNEW,YNEW-TOTEN1
!   25 FORMAT(' opt : ',F8.4,'  next Energy=',F13.6, &
!     &       ' (dE=',E10.3,')')
!    do not make another call to ZBRENT if reuqired accuracy was reached
      IF (ABS(YD)<EDIFFG) LBRENT=.FALSE.
      ENDIF
!-----------------------------------------------------------------------
!    move ions to the minimum
!-----------------------------------------------------------------------
      DO NI=1,NIONS
!----- search vector from cartesian to direct lattice
        TMP(1) = S(1,NI)
        TMP(2) = S(2,NI)
        TMP(3) = S(3,NI)
        CALL KARDIR(1,TMP,B)

        POSIOC(1,NI)= POSION(1,NI)
        POSIOC(2,NI)= POSION(2,NI)
        POSIOC(3,NI)= POSION(3,NI)

        POSION(1,NI)= TMP(1)*(DMOVE-DMOVEL)*STEP+POSIOC(1,NI)
        POSION(2,NI)= TMP(2)*(DMOVE-DMOVEL)*STEP+POSIOC(2,NI)
        POSION(3,NI)= TMP(3)*(DMOVE-DMOVEL)*STEP+POSIOC(3,NI)

!----- keep ions in unit cell (Robin Hirschl)
        POSION(1,NI)=MOD(POSION(1,NI),1.0)
        POSION(2,NI)=MOD(POSION(2,NI),1.0)
        POSION(3,NI)=MOD(POSION(3,NI),1.0)
        
      ENDDO

      DO J=1,3
      DO I=1,3
      A(I,J)=AC(I,J)
      DO K=1,3
        A(I,J)=A(I,J) + SSIF(I,K)*AC(K,J)*DMOVE*STEP
      ENDDO
      ENDDO
      ENDDO
      DMOVEL=DMOVE
      LTRIAL=.FALSE.
      dt=dt+(DMOVE-DMOVEL)*STEP
      
      correction_step=correction_step+1
!#ifdef TIMING
!      if (inode_tot.eq.1) then !ZHANGHUI CHEN
!        write(11,63) Xnew,Ynew,YD
!     63 format('Xnew(DMOVE)=',F9.5,'   Ynew=',F10.5,'   YD=',F10.5)
!        write(11,*) '-------------------------------------------------------------------------'
!        write(11,*) ''
!        write(11,*) ''
!        close(11)
!      endif
!#endif
      RETURN

      END

