program amorphous
!use mpi
use omp_lib
use sd

    implicit none

    real(dp),parameter                   :: kb = 8.61733d-5

    integer                              :: natom
    integer                              :: restart
    integer                              :: max_num_mov           ! maxiteration for cg relaxation
    integer                              :: max_num_mov_sd        ! maxiteration for sd relaxation
    integer                              :: irmethod
    integer                              :: finished_steps
    integer                              :: maxmciter ! maxite for MC
    real(dp)                             :: epsilon
    real(dp)                             :: tolforce
    integer,dimension(:),allocatable     :: names
    integer,dimension(:,:),allocatable   :: imov_at
    integer,dimension(:),allocatable     :: step_table
    real(dp),dimension(3,3)              :: lat, reclat
    real(dp),dimension(:,:),allocatable  :: coord
    real(dp),dimension(:  ),allocatable  :: temperature_table
    logical                              :: no_isolate
    
    type(bonds),dimension(:),allocatable :: bondss, bondss2
    type(bonds_l)                        :: bonds_list
    
    integer                              :: temperature_update, jumped_temperature
    integer                              :: acceptance_steps, accept_count2
    integer                              :: i,j,k, numsi, numo, it, stat, iteration,accept_count
    integer                              :: ierr, proc_id,source, dest, mcstart_step
    integer                              :: nproc, rtag, iteration_procs_max, print_count
    integer                              :: accept_proc_id, converged
    real(dp)                             :: temperature2,stime1, stime2 ,stime3,stime4,stime5
    real(dp)                             :: temp,energy,energy_new,energy_old
    real(dp)                             :: delta_E, scalar, prob, maxforce, temperature_scalar
    logical                              :: accept, accept_master
    character(len=256)                   :: filename, outputfilename,arg_temp, temperature_table_filename
    character(len=6)                     :: str, finished_steps_str
    integer,dimension(:),allocatable     :: iteration_procs
    real(dp),dimension(:),allocatable    :: energy_procs
    real(dp),dimension(:),allocatable    :: force_l
    real(dp),dimension(:,:),allocatable  :: force, coord_new, coord_old
    logical,dimension(:),allocatable     :: accept_procs

    integer,dimension(:,:),allocatable   :: n_table
    integer,dimension(:,:),allocatable   :: nn_table
    integer,dimension(:,:),allocatable   :: neigh_table
    integer,dimension(:,:,:),allocatable :: neigh_table_lat

!   integer                              :: rstat(MPI_STATUS_SIZE)


!   call MPI_INIT(ierr)
!   call MPI_COMM_RANK(MPI_COMM_WORLD,proc_id,ierr)  !!!Absolute rank - myoptid
!   call MPI_COMM_SIZE(MPI_COMM_WORLD,nproc,ierr)    !!!total number of processes

    call getarg(1,filename)
    call getarg(2,arg_temp);  read(arg_temp,*)epsilon
    call getarg(3,arg_temp);  read(arg_temp,*)restart   ! restart=0: from scratch; =1: restart
    if (restart==1) then
        call getarg(4,arg_temp);  read(arg_temp,*)finished_steps
    else
        finished_steps=0
    end if
    write(finished_steps_str,fmt='(I6.6)') finished_steps
    proc_id=0
    nproc=1
    rtag=1
    call init_random_seed()

    if (restart==1) then
        call read_coord(filename,coord,lat,names,natom,imov_at,finished_steps_str)
    else
        call read_coord(filename,coord,lat,names,natom,imov_at)
    end if

    if (proc_id==0) then
        numsi=0; numo=0
        do i=1,natom
            if (names(i)==ATOM_TYPE1) numsi=numsi+1
            if (names(i)==ATOM_TYPE2) numo=numo+1
        end do
        write(6,*) 'Natom = ', natom, 'Num of Si = ', numsi, 'Num of O = ', numo
    end if
!   call MPI_BCAST(natom,1,MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
!   call MPI_BCAST(numsi,1,MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
!   call MPI_BCAST(numo, 1,MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
!   call MPI_BCAST(lat,9,MPI_DOUBLE_PRECISION,0, MPI_COMM_WORLD, ierr)
!   call MPI_BCAST(names,natom,MPI_INTEGER,0, MPI_COMM_WORLD, ierr)
    allocate(coord_new(3,natom), coord_old(3,natom))
    allocate(n_table(4,natom),nn_table(12,natom))
    allocate(neigh_table(numneigh,natom),neigh_table_lat(3,numneigh,natom))
    allocate(force(3,natom),force_l(natom))
    allocate(accept_procs(nproc),iteration_procs(nproc),energy_procs(nproc))

!**************** define calculation method details *****************
    irmethod=1           ! use CG method
    tolforce=0.30        ! force tolerance is 0.3 eV/Angstrom
    max_num_mov=300      ! max relaxation iter for CG
    max_num_mov_sd=10000 ! max relaxation iter for SD
    maxmciter=1000000    ! max iter for MC
!   imov_at=1            ! all atoms are allowed to move
    no_isolate=.true.    ! if .true., avoid isolated Si-Si during bond switching
!**************** define calculation method details *****************

    temperature_table_filename='temperature_table'
    call read_temperature(temperature_table_filename,step_table,temperature_table,acceptance_steps)
    call decide_temperature(finished_steps,acceptance_steps,temperature_table,step_table, &
temperature2,jumped_temperature)
    if (proc_id==0) then
        write(6,*) 'Current temperature is ', temperature2
    end if

    reclat=transpose(rinverse(lat))
    call shift_to_unitcell(coord,lat,reclat)

    if (proc_id==0) then
        call read_bonds(bondss,names,finished_steps_str,.false.)
        call find_bonds_boundary(bondss,coord,lat)
        call gen_bonds_list(names,bondss,bonds_list,.true.)
        call n_nn_neighbors(bondss,n_table,nn_table)
        call find_neighbors(coord,lat,names,n_table, nn_table,neigh_table,neigh_table_lat)
    
        energy=compute_energy(bonds_list,coord,names,lat,natom,neigh_table,neigh_table_lat)
    
        write(6,*) 'Initial energy = ', energy
!       stop
    
        force=compute_force(bondss,coord,names,lat,natom,neigh_table,neigh_table_lat )
    
!       stop
        if (restart==0) then
             call relaxation(coord,energy_new,lat,natom,tolforce,epsilon,irmethod,900, &
20000,force,imov_at,bondss,bonds_list,names,n_table,nn_table,converged,iteration,0)
!            call atomic_relax_standalone(coord,lat,natom,tolforce,irmethod, &
!max_num_mov,energy_new,force,imov_at, bondss,bonds_list,names,n_table,nn_table,0,iteration)
            write(6,*) 'Print Initial relaxed coordinates', iteration
            outputfilename=trim(filename)
            call print_coord(outputfilename,coord,lat,names,imov_at,'000000')
            if (converged==0) then  ! relaxation not converged, when converged==0
                write(6,*) 'Bad input structure, non-converge'
                write(6,*) 'Try restart with the partial relaxed structure'
                stop
            end if
            coord_old=coord
            energy_old=energy_new
        
            write(6,*) 'Initial relaxed energy = ', energy_old
        else
            coord_old=coord
            energy_old=energy
        end if
    end if
!   stop
!   call MPI_BCAST(atom_bond_table,size(atom_bond_table),MPI_INTEGER,0,MPI_COMM_WORLD,ierr)

    ! starting MC loop

    accept_count=0
    accept_count2=0
    print_count=1
    if (proc_id==0) write(6,*) 'Entering MC loop '

    if (restart==0) mcstart_step=1
    if (restart==1) mcstart_step=finished_steps+1
    do it=mcstart_step,maxmciter

!       stime1=omp_get_wtime()
!       call MPI_BARRIER(MPI_COMM_WORLD, ierr)
!       call MPI_BCAST(coord_old,size(coord_old),MPI_DOUBLE_PRECISION,0, MPI_COMM_WORLD,ierr)
!       call MPI_BCAST(energy_old,1,MPI_DOUBLE_PRECISION,0, MPI_COMM_WORLD,ierr)
!       call MPI_BCAST(bondsisio,size(bondsisio),MPI_INTEGER,0, MPI_COMM_WORLD,ierr)
!       call MPI_BCAST(bondsio,size(bondsio),MPI_INTEGER,0, MPI_COMM_WORLD,ierr)
!       call MPI_BCAST(anglesiosi,size(anglesiosi),MPI_INTEGER,0, MPI_COMM_WORLD,ierr)
!       call MPI_BCAST(angleosio,size(angleosio),MPI_INTEGER,0, MPI_COMM_WORLD,ierr)

600 continue
        coord_new=coord_old
        call shift_to_unitcell(coord_new,lat,reclat)
        bondss2=copybonds(bondss)
        write(600,*) '  ==>  ', it
        call bond_switching(bondss2,names,coord_new,lat,imov_at,no_isolate)
        call find_bonds_boundary(bondss2,coord_new,lat)
        call gen_bonds_list(names,bondss2,bonds_list,.false.)
        call n_nn_neighbors(bondss2,n_table,nn_table)

!       stime2=omp_get_wtime()
!       write(6,*) 'BS RELAXATION ', stime2-stime1
        call relaxation(coord_new,energy_new,lat,natom,tolforce,epsilon,irmethod, &
max_num_mov,max_num_mov_sd,force,imov_at,bondss2,bonds_list,names,n_table,nn_table,converged,iteration,it)
!       stime3=omp_get_wtime()
!       write(6,*) 'TIME RELAXATION ', stime3-stime2
        if (converged==0) goto 600

        if (jumped_temperature==size(step_table)) stop
        if (it==step_table(jumped_temperature+1)) then
            temperature2=temperature_table(jumped_temperature+1)
            jumped_temperature=jumped_temperature+1
            if (proc_id==0)  write(6,'(A,I10)') '# Accepted count is ', accept_count2
            accept_count2=0
            if (proc_id==0)  write(6,'(A,F10.3)') '# Update temperature from this step; &
new temperature is (K) ', temperature2
        end if


        delta_E=energy_new-energy_old
        scalar=exp(-1e0*delta_E/(kb*temperature2))
        prob=get_rand_real(0d0,1d0)
        accept=delta_E < 0.0 .or. prob < scalar .or. it<=acceptance_steps


        if (accept) then
                accept_count=accept_count+1
                accept_count2=accept_count2+1
                coord_old=coord_new
                energy_old=energy_new
                bondss=copybonds(bondss2)
                if (mod(accept_count,print_count)==0) then
                    write(str,fmt='(I6.6)') it
                    outputfilename=trim(filename)
!       stime5=omp_get_wtime()
!       write(6,*) 'ACCE RELAXATION ', stime5-stime3
                    call print_coord(outputfilename,coord_new,lat,names,imov_at,str)
                    call print_bonds(bondss2,trim(str))
                end if
        end if
!       stime4=omp_get_wtime()
!       write(6,*) 'PRINT RELAXATION ', stime4-stime5

        write(6,'(A,I9,2F20.12,I5,L4)') '==> ', it,energy_old,energy_new,iteration,accept

    end do


!    deallocate(coord,names,bondsisio,bondsio,bondsio_lat,angleosio, &
!angleosio_lat,anglesiosi,anglesiosi_lat,force,atom_bond_table, &
!bondsisio2,bondsio2,angleosio2,anglesiosi2,bondsio2_lat,angleosio2_lat, &
!anglesiosi2_lat,bondsisio2_lat)

end program amorphous
