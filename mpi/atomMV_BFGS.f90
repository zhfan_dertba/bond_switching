      SUBROUTINE atomMV_BFGS(IFLAG,NIONS,TOTEN,A,B,NFREE,POSION,POSIOC, &
     &      FACT,F,FACTSI,FSIF,FL,S,DISMAX,IU6,IU0, &
     &      EBREAK,EDIFFG,E1TEST,LSTOP2,inode_tot,imov_at,HessianInv)
     
!      use data_variable_1
      IMPLICIT REAL(8) (A-H,O-Z)
      
      integer imov_at(3,NIONS)
      DIMENSION F(3,NIONS),FL(3,NIONS),S(3,NIONS)
      DIMENSION POSION(3,NIONS),POSIOC(3,NIONS)
      DIMENSION A(3,3),B(3,3)
      DIMENSION TMP(3)
      LOGICAL   LRESET,LTRIAL,LBRENT
      LOGICAL   LSTOP2

      SAVE  AC,FSIFL,SSIF
      DIMENSION AC(3,3),FSIFL(3,3),SSIF(3,3),FSIF(3,3)

      SAVE TOTEN1,DMOVED,E1ORD1,ORTH,GAMMA,GNORM,GNORMF,GNORML
      SAVE GNORM1,GNORM2,STEP,CURVET,SNORM,SNORMOLD
      SAVE DMOVEL,LTRIAL,LBRENT
      DATA ICOUNT/0/, ICOUNT2/0/, STEP /1.0000000/,SNORM/1E-10/
      DATA LTRIAL/.FALSE./
      
      DIMENSION HessianInv(3*NIONS,3*NIONS),SearchDecline(3*NIONS),Dforce(3*NIONS)
      DIMENSION eye(3*NIONS,3*NIONS),SearchSearchMatrix(3*NIONS,3*NIONS)
      DIMENSION ForceSearchMatrix(3*NIONS,3*NIONS),SearchForceMatrix(3*NIONS,3*NIONS)
      real*8 SearchForceDotProduct,ORTH_tmp
      DIMENSION FTEMP(3*NIONS),ZTEMP(3*NIONS),Z(3,NIONS)
      integer :: ionstep=0
      SAVE ionstep
      !SAVE HessianInv
      ionstep=ionstep+1; !ZHANGHUI CHEN
      
      if (IU0>=0) then !ZHANGHUI CHEN
        if(ionstep==1) then
          open(unit=11,file="Progress",status="replace")
        else
          open(unit=11,file="Progress",status="old",position="append")
        endif
        write(11,55) ionstep,TOTEN
     55 format('The ',I3,' SCF step for ionic relaxation. Total Energy:',F10.5) 
        write(11,*) 'Force Vector F= (unit of eV/A/0.0482)'
        write(11,*) F
        close(11)
      endif
      
!=======================================================================
!  if IFLAG =0 initialize everything
!=======================================================================
      IF (IFLAG==0) THEN
        DO NI=1,NIONS
        DO I=1,3
          S(I,NI) =0
          FL(I,NI)=0
        ENDDO
        ENDDO
        DO I=1,3
        DO J=1,3
          SSIF (I,J)=0
          FSIFL(I,J)=0
        ENDDO
        ENDDO
        
        LTRIAL=.FALSE.
        ICOUNT=0
        SNORM =1E-10
        SNORMOLD =1E10
        CURVET=0
      ENDIF

! if previous step was a trial step then continue with line minimization
      IF (LTRIAL) THEN
        GOTO 400
      ENDIF

!=======================================================================
!  calculate quantities necessary to conjugate directions
!=======================================================================
      GNORML=0 !no use now !ZHANGHUI CHEN
      GNORM =0 !no use now !ZHANGHUI CHEN 
      GNORMF=0
      ORTH  =0
      IF (FACT/=0) THEN
      DO NI=1,NIONS
         GNORML = GNORML+1.000/FACT* &
     &    (FL (1,NI)*FL (1,NI)+FL(2,NI) *FL(2,NI) +FL(3,NI) *FL(3,NI))
         GNORM  = GNORM+ 1.000/FACT*( &
     &    (F(1,NI)-FL(1,NI))*F(1,NI) &
     &   +(F(2,NI)-FL(2,NI))*F(2,NI) &
     &   +(F(3,NI)-FL(3,NI))*F(3,NI))
         GNORMF = GNORMF+1.000/FACT* &
     &    (F(1,NI)*F(1,NI)+F(2,NI)*F(2,NI)+F(3,NI)*F(3,NI))
         ORTH   = ORTH+  1.000/FACT* &
     &    (F(1,NI)*S(1,NI)+F(2,NI)*S(2,NI)+F(3,NI)*S(3,NI))
      ENDDO
      ENDIF

      GNORM1=GNORMF

      IF (FACTSI/=0) THEN
      DO I=1,3
      DO J=1,3
         GNORML=GNORML+ FSIFL(I,J)*FSIFL(I,J)/FACTSI
         GNORM =GNORM +(FSIF(I,J)-FSIFL(I,J))*FSIF(I,J)/FACTSI
         GNORMF=GNORMF+ FSIF(I,J)* FSIF(I,J)/FACTSI
         ORTH  =ORTH  + FSIF(I,J)* SSIF(I,J)/FACTSI
      ENDDO
      ENDDO
      ENDIF
      GNORM2=GNORMF-GNORM1

      !CALLMPI_C( sum_chain( GNORM ))
      !CALLMPI_C( sum_chain( GNORML ))
      !CALLMPI_C( sum_chain( GNORMF))
      !CALLMPI_C( sum_chain( GNORM1))
      !CALLMPI_C( sum_chain( GNORM2))
      !CALLMPI_C( sum_chain( ORTH ))
      
      if (IU0>=0) then !ZHANGHUI CHEN
        open(unit=11,file="Progress",status="old",position="append")
        write(11,56) GNORML,GNORM
     56 format('|FL*FL|=',F10.5,'   |(F-FL)*(F-FL)|=',F10.5)
        write(11,57) GNORMF,ORTH
     57 format('|F*F|=',F10.5,'   |F*SL|=',F10.5)
        write(11,*) ''
        close(11)
      endif

!=======================================================================
!  improve line optimization if necessary
!=======================================================================
      IF (IFLAG==0) THEN
        ICOUNT=0
      ENDIF
      IFLAG=1
!      IF(inode_tot .eq. 1) THEN
!      IF (IU0>=0) &
!      WRITE(IU0,30)CURVET,CURVET*GNORMF,CURVET*(ORTH/SQRT(SNORM))**2
!      ENDIF
   30 FORMAT(' curvature: ',F6.2,' expect dE=',E10.3,' dE for cont linesearch ',E10.3)

! required accuracy not reached in line minimization
! improve line minimization
! several conditions must be met:

! orthonormality not sufficient
!      WRITE(0,*) ORTH, MAX(GAMMA,GAMMIN),GNORMF/5, ABS(CURVET*(ORTH/SQRT(SNORM))**2), LSTOP2
      IF (ABS(ORTH)>ABS(GNORMF)/5 &
! expected energy change along line search must be larger then required accuracy
     &    .AND. &
     &   ( (EDIFFG>0 .AND. &
     &       ABS(CURVET*(ORTH/SQRT(SNORM))**2)>EDIFFG) &
! or force must be large enough that break condition is not met
     &    .OR. &
     &     (EDIFFG<0 .AND..NOT.LSTOP2) &
     &   ) &
! last call must have been a line minimization
     &    .AND. LBRENT &
     &  ) GOTO 400
     
     
     !!!Enter into a new trial step and calculate the Hessian and search direction !ZHANGHUI CHEN
      if(ionstep==1) then
         HessianInv=0.0
         do I=1,3*NIONS
            HessianInv(I,I)=1.0
         enddo
      else
         do J=1,NIONS
           do I=1,3  
             SearchDecline(3*J-3+I)=DMOVEL*STEP*S(I,J)
             Dforce(3*J-3+I)=-(F(I,J)-FL(I,J))
           enddo
         enddo
         eye=0
         do I=1,3*NIONS
            eye(I,I)=1.0
         enddo
         do I=1,3*NIONS
           do J=1,3*NIONS  
              SearchForceMatrix(I,J)=SearchDecline(I)*Dforce(J)
              SearchSearchMatrix(I,J)=SearchDecline(I)*SearchDecline(J)
              ForceSearchMatrix(I,J)=Dforce(I)*SearchDecline(J)
           enddo
         enddo
         SearchForceDotProduct=DOT_PRODUCT(SearchDecline,Dforce)
         
         HessianInv=MATMUL(eye-SearchForceMatrix/SearchForceDotProduct,MATMUL(HessianInv,eye-ForceSearchMatrix/SearchForceDotProduct))+SearchSearchMatrix/SearchForceDotProduct        
      endif
      
      DO I=1,NIONS
         DO J=1,3
            FTEMP(3*I-3+J)=F(J,I)
         ENDDO
      ENDDO          
      DO I=1,NIONS*3
         ZTEMP(I)=0.0
         DO J=1,NIONS*3
            ZTEMP(I)=ZTEMP(I)+HessianInv(I,J)*FTEMP(J)        
         ENDDO
      ENDDO
      DO I=1,NIONS
         DO J=1,3
             Z(J,I)=ZTEMP(3*I-3+J)
         ENDDO
      ENDDO
      
      if (IU0>=0) then !ZHANGHUI CHEN
        if(ionstep==1) then
          open(unit=14,file="Hessian_Inv",status="replace")
        else
          open(unit=14,file="Hessian_Inv",status="old",position="append")
        endif
        write(14,55) ionstep,TOTEN
        DO I=1,NIONS*3
           write(14,'(1000(2XF12.4))') HessianInv(I,:)
        ENDDO 
        write(14,*) ''
        write(14,*) ''
        close(14)
      endif
!---- improve the trial step by adding some amount of the optimum step
!      IF (ICOUNT/=0) STEP=STEP+0.2*STEP*(DMOVEL-1) 
      IF (ICOUNT/=0) THEN !!! (2.2) revision for brent ZHANGHUI CHEN
         STEP=abs(STEP)
         STEP=min(STEP+0.2*STEP*(DMOVEL-1),3.0) 
      ENDIF
      
!---- set GAMMA to zero if line minimization was not sufficient
      IF (5*ABS(ORTH)>ABS(GNORMF)) THEN
         ICOUNT=0
      ENDIF
!---- if GNORM is very small signal calling routine to stop
      IF (CURVET/=0 .AND.ABS((GNORMF)*CURVET*2)<EDIFFG) THEN
        IFLAG=2
      ENDIF

      ICOUNT=ICOUNT+1
      ICOUNT2=ICOUNT2+1
!-----------------------------------------------------------------------
! performe trial step
!-----------------------------------------------------------------------
      E1ORD1=0
      DMOVED=0
      SNORM =1E-10
      
      DO NI=1,NIONS
!----- store last gradient
        FL(1,NI)=F(1,NI)
        FL(2,NI)=F(2,NI)
        FL(3,NI)=F(3,NI)
!----- conjugate the direction to the last direction
        S(1,NI) = Z(1,NI)
        S(2,NI) = Z(2,NI)
        S(3,NI) = Z(3,NI)
        
!-------  Zhang hui and Weile , 2014, 12, 12

        S(1, NI) = S(1, NI) * imov_at(1, NI)
        S(2, NI) = S(2, NI) * imov_at(2, NI)
        S(3, NI) = S(3, NI) * imov_at(3, NI)
       
        IF (FACT/=0) THEN
        SNORM = SNORM  +  1/FACT * &
     &  (S(1,NI)*S(1,NI)+ S(2,NI)*S(2,NI) + S(3,NI)*S(3,NI))
        ENDIF
      ENDDO
      DO I=1,3
         DO J=1,3
            FSIFL(I,J)=FSIF(I,J)
            AC(I,J)   = A(I,J)
            SSIF(I,J) = FSIF(I,J)
         ENDDO
      ENDDO
      IF (FACTSI/=0) THEN
         DO I=1,3
            DO J=1,3
               SNORM = SNORM  + 1/FACTSI * SSIF(I,J)* SSIF(I,J)
            ENDDO
         ENDDO
      ENDIF

      !CALLMPI_C( sum_chain( SNORM))
            
      !Normalize the search vector with force to aviod a too large vector ZHANGHUI CHEN !!!!
      S=S*sqrt(GNORMF/SNORM)
      SNORM=GNORMF
      if (IU0>=0) then !ZHANGHUI CHEN
        open(unit=11,file="Progress",status="old",position="append")
        write(11,*) 'Search Vector S='
        write(11,*) S
        write(11,58) SNORM,SNORMOLD
58      format('|S*S|=',F10.5,'   |SL*SL|=',F10.5)
        write(11,*) ''
      endif

!----- if SNORM increased, rescale STEP (to avoid too large trial steps) 
!      (Robin Hirschl)    
      IF (SNORM>SNORMOLD) THEN 
         STEP=STEP*sqrt(SNORMOLD/SNORM) !ZHANGHUI CHEN
      ENDIF
      SNORMOLD=SNORM
      if(STEP<0.2 .AND. SNORM/(NIONS*3)<2.0)  STEP=0.2
      
      !ZHANGHUI CHEN, Use negative step if S*F<0
      ORTH_tmp=0.0
      DO NI=1,NIONS
         ORTH_tmp=ORTH_tmp+1.000/FACT*(F(1,NI)*S(1,NI)+F(2,NI)*S(2,NI)+F(3,NI)*S(3,NI))
      ENDDO
      if(ORTH_tmp<0) STEP=-STEP
      
      
      DO NI=1,NIONS
!----- search vector from cartesian to direct lattice
         TMP(1) = S(1,NI)
         TMP(2) = S(2,NI)
         TMP(3) = S(3,NI)
         CALL KARDIR(1,TMP,B)

!----- trial step in direct grid
         POSIOC(1,NI)= POSION(1,NI)
         POSIOC(2,NI)= POSION(2,NI)
         POSIOC(3,NI)= POSION(3,NI)

         POSION(1,NI)= TMP(1)*STEP+POSIOC(1,NI)
         POSION(2,NI)= TMP(2)*STEP+POSIOC(2,NI)
         POSION(3,NI)= TMP(3)*STEP+POSIOC(3,NI)
         DMOVED= MAX( DMOVED,S(1,NI)*STEP,S(2,NI)*STEP,S(3,NI)*STEP)

!----- keep ions in unit cell (Robin Hirschl)
         POSION(1,NI)= MOD(POSION(1,NI),1.000)
         POSION(2,NI)= MOD(POSION(2,NI),1.000)
         POSION(3,NI)= MOD(POSION(3,NI),1.000)

!----- force * trial step = 1. order energy change
         IF (FACT/=0) THEN
            E1ORD1= E1ORD1 - 1.000 * STEP / FACT * &
     &           (S(1,NI)*F(1,NI)+ S(2,NI)*F(2,NI) + S(3,NI)*F(3,NI))
         ENDIF
      ENDDO

      IF (FACTSI/=0) THEN
      DO I=1,3
      DO J=1,3
         E1ORD1= E1ORD1 - STEP / FACTSI * SSIF(I,J)* FSIF(I,J)
      ENDDO
      ENDDO
      ENDIF

      DO J=1,3
         DO I=1,3
            A(I,J)=AC(I,J)
            DO K=1,3
               A(I,J)=A(I,J) + SSIF(I,K)*AC(K,J)*STEP
            ENDDO
         ENDDO
      ENDDO

      !CALLMPI_C( sum_chain( E1ORD1 ))

      LRESET = .TRUE.
      X=0
      Y=TOTEN
      FP=E1ORD1
      IFAIL=0
      

      CALL ZBRENT_NEW(IU0,LRESET,EBREAK,X,Y,FP,XNEW,XNEWH,YNEW,YD,IFAIL,STEP) !ZHANGHUI CHEN
      DMOVEL=1

      IF (IU0>=0) THEN
!         WRITE(IU0,10) 0,GNORM1,GNORM2,ORTH,STEP
! 10      FORMAT(' trial: gam=',F8.5,' g(F)= ',E10.3, &
!     &        ' g(S)= ',E10.3,' ort =',E10.3,' (trialstep =',E10.3,')')
!         WRITE(IU0,11) SNORM
! 11      FORMAT(' search vector abs. value= ',E10.3)
      ENDIF
      TOTEN1= TOTEN
      E1TEST=E1ORD1
      LTRIAL=.TRUE.
      
!#ifdef TIMING
!      if (IU0>=0) then !ZHANGHUI CHEN
!        write(11,*) 'This move is a trial step with following information:'
!        write(11,59) STEP,DMOVEL
!     59 format('Step_size=',F8.5,'   DMOVEL=',F8.5)
!        write(11,60) X,Y,FP
!     60 format('X(e.g. A)=',F8.5,'   Y(YA)=',F10.5,'   FP(FA)=',F10.5)
!        write(11,61) Xnew,Ynew,YD
!     61 format('Xnew=',F8.5,'   Ynew=',F10.5,'   YD=',F10.5)
!        write(11,*) '-------------------------------------------------'
!        write(11,*) ''
!        write(11,*) ''
!        close(11)
!      endif
!#endif      
      RETURN
!=======================================================================
! calculate optimal step-length and go to the minimum
!=======================================================================
!-----------------------------------------------------------------------
!  1. order energy change due to displacement at the new position
!-----------------------------------------------------------------------
  400 CONTINUE
      E1ORD2=0
      IF (FACT/=0) THEN
      DO NI=1,NIONS
        E1ORD2= E1ORD2 - 1.000 * STEP / FACT * &
     &  (S(1,NI)*F(1,NI)+ S(2,NI)*F(2,NI) + S(3,NI)*F(3,NI))
      ENDDO
      ENDIF

      IF (FACTSI/=0) THEN
      DO I=1,3
      DO J=1,3
        E1ORD2= E1ORD2 - STEP / FACTSI * SSIF(I,J)* FSIF(I,J)
      ENDDO
      ENDDO
      ENDIF

      !CALLMPI_C( sum_chain( E1ORD2 ))
!-----------------------------------------------------------------------
!  calculate position of minimum
!-----------------------------------------------------------------------
      CONTINUE
      LRESET = .FALSE.
      X=DMOVEL
      Y=TOTEN
      FP=E1ORD2
      IFAIL=0
      
      if (IU0>=0) then !ZHANGHUI CHEN
        open(unit=11,file="Progress",status="old",position="append")
        write(11,*) 'This move is a Correction step with the following information:'
        write(11,62) X,Y,FP
     62 format('X(e.g. B)=',F8.5,'   Y(YB)=',F10.5,'   FP(FB)=',F10.5)
      endif

      CALL ZBRENT_NEW(IU0,LRESET,EBREAK,X,Y,FP,XNEW,XNEWH,YNEW,YD,IFAIL,STEP) !ZHANGHUI CHEN
!     estimate curvature
      CURVET=YD/(E1ORD2/STEP/SQRT(SNORM))**2
      

      DMOVE =XNEW
      DMOVEH=XNEWH

!    previous step was trial step than give long output

      DISMAX=DMOVE*DMOVED     ! always calculate DISMAX for this step, always positive, sign?

      IF (LTRIAL) THEN
      LBRENT=.TRUE.
      E2ORD  = TOTEN-TOTEN1
      E2ORD2 = (E1ORD1+E1ORD2)/2
!      IF (IU0>=0) &
!      WRITE(IU0,45) E2ORD,E2ORD2,E1ORD1,E1ORD2
!   45 FORMAT(' trial-energy change:',F12.6,'  1 .order',3F12.6)

      E1TEST=TOTEN1-YNEW
      !DISMAX=DMOVE*DMOVED
!      IF (IU0>=0) &
!      WRITE(IU0,20) DMOVE*STEP,DMOVEH*STEP,DISMAX,YNEW,YNEW-TOTEN1
!   20 FORMAT(' step: ',F8.4,'(harm=',F8.4,')', &
!     &       '  dis=',F8.5,'  next Energy=',F13.6, &
!     &       ' (dE=',E10.3,')')

!      IF (IU6>=0) &
!      WRITE(IU6,40) E2ORD,(E1ORD1+E1ORD2)/2,E1ORD1,E1ORD2, &
!     &         GNORM,GNORMF,GNORML, &
!     &         GNORM1,GNORM2,ORTH,0,STEP, &
!     &         DMOVE*STEP,DMOVEH*STEP,DISMAX,YNEW,YNEW-TOTEN1

!   40 FORMAT(' trial-energy change:',F12.6,'  1 .order',3F12.6/ &
!     &       '  (g-gl).g =',E10.3,'      g.g   =',E10.3, &
!     &       '  gl.gl    =',E10.3,/ &
!     &       ' g(Force)  =',E10.3,'   g(Stress)=',E10.3, &
!     &       ' ortho     =',E10.3,/ &
!     &       ' gamma     =',F10.5,/ &
!     &       ' trial     =',F10.5,/ &
!     &       ' opt step  =',F10.5,'  (harmonic =',F10.5,')', &
!     &       ' maximal distance =',F10.8/ &
!     &       ' next E    =',F13.6,'   (d E  =',F10.5,')')
      ELSE
!      IF (IU0>=0) &
!      WRITE(IU0,25) DMOVE*STEP,YNEW,YNEW-TOTEN1
!   25 FORMAT(' opt : ',F8.4,'  next Energy=',F13.6, &
!     &       ' (dE=',E10.3,')')
!    do not make another call to ZBRENT if reuqired accuracy was reached
      IF (ABS(YD)<EDIFFG) LBRENT=.FALSE.
      ENDIF
!-----------------------------------------------------------------------
!    move ions to the minimum
!-----------------------------------------------------------------------
      DO NI=1,NIONS
!----- search vector from cartesian to direct lattice
        TMP(1) = S(1,NI)
        TMP(2) = S(2,NI)
        TMP(3) = S(3,NI)
        CALL KARDIR(1,TMP,B)

        POSIOC(1,NI)= POSION(1,NI)
        POSIOC(2,NI)= POSION(2,NI)
        POSIOC(3,NI)= POSION(3,NI)

        POSION(1,NI)= TMP(1)*(DMOVE-DMOVEL)*STEP+POSIOC(1,NI)
        POSION(2,NI)= TMP(2)*(DMOVE-DMOVEL)*STEP+POSIOC(2,NI)
        POSION(3,NI)= TMP(3)*(DMOVE-DMOVEL)*STEP+POSIOC(3,NI)

!----- keep ions in unit cell (Robin Hirschl)
        POSION(1,NI)=MOD(POSION(1,NI),1.000)
        POSION(2,NI)=MOD(POSION(2,NI),1.000)
        POSION(3,NI)=MOD(POSION(3,NI),1.000)
        
      ENDDO

      DO J=1,3
      DO I=1,3
      A(I,J)=AC(I,J)
      DO K=1,3
        A(I,J)=A(I,J) + SSIF(I,K)*AC(K,J)*DMOVE*STEP
      ENDDO
      ENDDO
      ENDDO
      DMOVEL=DMOVE
      IFLAG=0
      LTRIAL=.FALSE.
      
      if (IU0>=0) then !ZHANGHUI CHEN
        write(11,63) Xnew,Ynew,YD
     63 format('Xnew(DMOVE)=',F9.5,'   Ynew=',F10.5,'   YD=',F10.5)
        write(11,*) '-------------------------------------------------'
        write(11,*) ''
        write(11,*) ''
        close(11)
      endif

      RETURN

      END






!#include "symbol.inc"
!*********************************************************************
! RCS:  $Id: brent.F,v 1.1 2000/11/15 08:13:54 kresse Exp $
! subroutine ZBRENT
!  variant of Brents root finding method used for
!  minimization of a function
!
!  derivatives and function values have to be supplied
!  the derivatives are considered to be more accurate than the
!  function values !
! ICALL determines behavior and is incremented by the routine
! until a external reset is done
! ICALL
!  0      unit step is done i.e. XNEW=1
!  1      cubic interpolation / extrapolation to minimum
!         hopefully 2. call will give sufficient precision
!  2      init Brents method
!  3      variant of Brendts method
!          bracketing, bisectioning + inverse quadratic interpolation
!          using derivatives only
!
!  on call
!  LRESET    restart algorithm
!  EBREAK    break condition for using the cubic interpolation
!  X,Y,F     position and values for function and derivatives at X
!  on exit
!  XNEW,YNEW position where function has to be evalueated
!            and expected function value for this position
!  XNEWH     harmonic step with calculated for ICALL=1
!  YD        expected change of energy in this step relative to the
!             previous step
!
!*********************************************************************

!Big and Small changes are added with notes of ZHANGHUI CHEN 

      SUBROUTINE ZBRENT_NEW(IU,LRESET,EBREAK,X,Y,F,XNEW,XNEWH,YNEW,YD,IFAIL,stepsize) !ZHANGHUI CHEN
      !USE prec

      IMPLICIT REAL(8) (A-H,O-Z)
      REAL(8) KOEFF1,KOEFF2,KOEFF3

      LOGICAL LRESET,LBRACK
      SAVE ICALL,LBRACK,A,B,YA,YB,FA,FB,C,FC,YC,FSTART
      SAVE D,E,XM
      PARAMETER (EPS=1E-8,TOL=1E-8)

!     parameter controls the maximum step width
!     DMAX is maximum step for 1. step
!     DMAXBR is maximum step for Brents algorithm, we use golden ratio
      PARAMETER (DMAX=4,DMAXBR=2)
      DATA ICALL/0/
!     minimum not found up to now
      IFAIL=1
      IDUMP=1
      IF (IU<0) IDUMP=0

      IF (LRESET) ICALL=0
!-----------------------------------------------------------------------
!    case 0: trial step 1, into trial direction
!-----------------------------------------------------------------------
      IF (ICALL==0) THEN
        A   =X
        YA  =Y
        FA  =F
        YD  =F
        YNEW=Y+F
        XNEW=X+1
        ICALL=1
        IFAIL=1
      RETURN
      ENDIF
!-----------------------------------------------------------------------
!    case 1:  cubic interpolation / extrapolation 
!    if precision of energy is not sufficient use secant method
!-----------------------------------------------------------------------
      IF (ICALL==1) THEN
      B   =X
      YB  =Y
      FB  =F

      FFIN= YB-YA
      FAB = (FA+FB)/2

      KOEFF3=3.000*(FB+   FA-2.000*FFIN)
      KOEFF2=    FB+2.000*FA-3.000*FFIN
      KOEFF1=    FA
!----cubic extrapolation - secant method break condition
      IF ( (ABS(KOEFF3/KOEFF1)< 1E-2)  .OR. &
     &     (ABS(KOEFF3)       < EBREAK*24).OR. &
     &     ( (KOEFF1*KOEFF3/KOEFF2/KOEFF2) >= 1.000 )) THEN

!----- harmonic  case
         DMOVE  = -FA/(FAB-FA)/2.000
         DMOVEH = -FA/(FAB-FA)/2.000
!         IF (DMOVE<0) THEN
         IF (DMOVE<0 .AND. (.NOT. (FA>0 .AND. FB>0))) THEN !(3) ZHANGHUI CHEN
           DMOVE =DMAX
           DMOVEH=DMAX
         ENDIF
         DY=(FA+(FAB-FA)*DMOVE)*DMOVE
         YNEW =YA+DY
      ELSE

!----- anharmonic interpolation (3rd order in energy) (jF)
         DMOVE1=KOEFF2/KOEFF3*(1.000-SQRT(1.000-KOEFF1*KOEFF3/KOEFF2/KOEFF2))
         DMOVE2=KOEFF2/KOEFF3*(1.000+SQRT(1.000-KOEFF1*KOEFF3/KOEFF2/KOEFF2))
!----- 3rd order polynomial has one minimum and one maximum ... :
         DY1=-(KOEFF1-(KOEFF2-KOEFF3*DMOVE1/3.000)*DMOVE1)*DMOVE1
         DY2=-(KOEFF1-(KOEFF2-KOEFF3*DMOVE2/3.000)*DMOVE2)*DMOVE2
!----- select the correct extremum:
         IF (DY1>DY2) THEN
           DMOVE=DMOVE1
           DY=DY1
         ELSE
           DMOVE=DMOVE2
           DY=DY2
         ENDIF
         YNEW=YA-DY

         DMOVEH = -FA/(FAB-FA)/2.000
         !IF (DMOVEH<0) DMOVEH=DMAX   !(1.1) ZHANGHUI CHEN
         IF (DMOVEH<0 .AND. (.NOT. (FA>0 .AND. FB>0)) .AND. (.NOT. (FA<0 .AND. FB<0 .AND. YB<YA .AND. abs(FB)>abs(FA)))) DMOVEH=DMAX 
!-----  extremely unharmonic and/or
!       minimum should be on the right  side of B but it is due to
!       3. order interpolation on left side
!     -> take the save harmonic extrapolation
         !IF (DMOVE>(2*DMOVEH).OR. DMOVEH>(2*DMOVE) & !(1.2) ZHANGHUI CHEN
         IF ( (DMOVE>0 .AND. DMOVEH>0 .AND. DMOVE>(2*DMOVEH)) &  !.OR.(DMOVE*DMOVEH>0.AND.DMOVEH>(2*DMOVE))
!     &   .OR. (FA*FB>0 .AND.DMOVE<1.0000 ) &
     &   .OR. (FA<0 .AND. FB<0 .AND.DMOVE<1.0000 ) & !(1.3) ZHANGHUI CHEN
     &   .OR. (FA*FB<0 .AND.DMOVE>1.0000 ))THEN
           DMOVE=DMOVEH
           DY   =(FA+(FAB-FA)*DMOVE)*DMOVE
           YNEW =YA+DY
         ENDIF
      ENDIF

      !IF (DMOVE>DMAX) DMOVE=DMAX 
      IF (DMOVE>DMAX) then !(2.1) ZHANGHUI CHEN, (2.2) is at the line STEP=STEP+0.2*STEP*(DMOVEL-1) of the dyna.F file
           If(abs(DMOVE*stepsize)<1.0) then
               DMOVE=max(DMAX,sqrt(DMOVE)) 
           else
               DMOVE=DMAX
           endif
      endif
      IF (DMOVE<-1.0*DMAX) then !(2.1.1) ZHANGHUI CHEN
           If(abs(DMOVE*stepsize)<1.0) then
               DMOVE=-1.0*max(DMAX,sqrt(-1.0*DMOVE)) 
           else
               DMOVE=-1.0*DMAX
           endif
      endif
!      IF (DMOVE== DMAX .AND. IDUMP>=1) &
!     &     WRITE(IU,*)'ZBRENT: can''t locate minimum, use default step' 
!     &     WRITE(IU,*)'ZBRENT: might not locate minimum, use default step'!(2.3)
      XNEW =DMOVE+A
      XNEWH=DMOVEH+A
!     estimated change relative to B
      YD   =(FB+(FAB-FB)/(A-B)*(DMOVE-B))*(DMOVE-B)
      ICALL=2
      IFAIL=1
      RETURN
      ENDIF
!-----------------------------------------------------------------------
!    case 2:  cubic interpolation / extrapolation failed
!      and was not accurate enough
!      start to use Brents algorithm
!-----------------------------------------------------------------------
      IF (ICALL==2) THEN
      LBRACK=.TRUE.
!  1.  X > B    start intervall [B,X]
      IF (X>=B) THEN
         A =B
         YA=YB
         FA=FB
         B =X
         YB=Y
         FB=F
!     check for bracketing
      IF (FA*FB>0) LBRACK=.FALSE.
      FSTART=FA
!  2.  X < B
      ELSE
        IF (FA*F<=0) THEN
!  2a. minimum between [A,X]
          B   =X
          YB  =Y
          FB  =F
!  2b. minimum between [X,B]
        ELSE IF (FB*F<=0) THEN
           A =B
           YA=YB
           FA=FB
           B   =X
           YB  =Y
           FB  =F
!  2c. here we have some serious problems no miniumum between [A,B]
!      but X (search lies between) [A,B] -> complete mess
!      happens only beacuse of cubic interpolations
!      work-around search between [X,B] LBRACK=.FALSE.
        ELSE
!          IF (IDUMP>=1) WRITE(IU,*)'ZBRENT:  no minimum in in bracket'
          A =B
          YA=YB
          FA=FB
          B   =X
          YB  =Y
          FB  =F
          LBRACK=.FALSE.
        ENDIF
      ENDIF
      ICALL=3
      C =B
      FC=FB
      YC=YB
      ENDIF
!-----------------------------------------------------------------------
!  fall trough to this line for ICALL > 4 set: B=X
!-----------------------------------------------------------------------
      IF (ICALL>=4) THEN
        B =X
        FB=F
        YB=Y
!   maybe a bracketing interval was found ?
        IF (.NOT. LBRACK.AND. FSTART*F < 0) THEN
!   if bracketing is started forget C
          LBRACK=.TRUE.
           C =B
           FC=FB
           YC=YB
!           IF (IDUMP/=0) WRITE(IU,*)'ZBRENT: bracketing found'
        ENDIF
      ENDIF
!-----------------------------------------------------------------------
! modified brent algorithm if no bracketing intervall exists
! here we have three points [C,A] and B, where B is the new guess
! and lies at the right  hand side of A
! A is the last best guess
!-----------------------------------------------------------------------
      IF (ICALL>=3) THEN
      ICALL=ICALL+1
      IF (ICALL==20) THEN
!        IF (IDUMP>=1) WRITE(IU,*)'ZBRENT:  can not reach accuracy'
        IFAIL=2
        RETURN
      ENDIF
      IF (IDUMP>=2) THEN
!      WRITE(IU,*)
!      WRITE(IU,'(A5,3E14.7)') 'A',A,YA,FA
!      WRITE(IU,'(A5,3E14.7)') 'B',B,YB,FB
!      WRITE(IU,'(A5,3E14.7)') 'C',C,YC,FC
!      WRITE(IU,*) LBRACK,D,E
      ENDIF


      IF (.NOT.LBRACK) THEN
! ABS(FC) < ABS(FB) or AC < BA
        IF(ABS(FC)<=ABS(FB).OR.(A-C)<(B-A)) THEN
          C=A
          FC=FA
          YC=YA
          D=B-A
          E=B-A
        ENDIF
        TOL1=2.000*EPS*ABS(B)+0.5*TOL
        XM=.5*(C-B)
      IF (IDUMP>=2) THEN
!      WRITE(IU,'(A5,3E14.7)') 'A',A,YA,FA
!      WRITE(IU,'(A5,3E14.7)') 'B',B,YB,FB
!      WRITE(IU,'(A5,3E14.7)') 'C',C,YC,FC
      ENDIF
! just for savety check for correct ordering
      IF ( .NOT.(C<=A .AND. A<=B) ) THEN
        IF (IU>0) THEN
!        WRITE(IU,*) 'PWmat encounter atomic relaxation error. '
!        WRITE(IU,*) 'change your config and then run again.. '
!        WRITE(IU,*)'ZBRENT: fatal error: bracketing interval incorrect'
!        WRITE(IU,*)'    please rerun with smaller EDIFF, or copy ATOM.CONFIG to'
!        WRITE(IU,*)'    to ATOM.CONFIG and continue'
        ENDIF
       STOP
      ENDIF
!     return if accuracy ca not be improved
       IF(ABS(XM)<=TOL1 .OR. FB==0.000)THEN
!          IF (IDUMP>=1) WRITE(IU,*) 'ZBRENT:  accuracy reached'
          IFAIL=0
          RETURN
        ENDIF
        IF(ABS(E)>=TOL1 .AND. ABS(FA)>ABS(FB)) THEN
          S=FB/FA
!  A.eq.C secant method (only one information)
          IF(A==C) THEN
            P=2.000*XM*S
            QQ=1.000-S
!  A.ne.C attempt inverse quadratic interpolation
            ELSE
            QQ=FA/FC
            R=FB/FC
            P=S*(2.000*XM*QQ*(QQ-R)-(B-A)*(R-1.000))
            QQ=(QQ-1.000)*(R-1.000)*(S-1.000)
          ENDIF
          IF(P>0.000) QQ=-QQ
          P=ABS(P)
!  are we within the bounds; correct but tricky :-<
!          IF (IDUMP>=2) WRITE(IU,*)'would go to ',A+P/QQ,P/QQ,E
          IF(P < MIN(DMAXBR*(B-A)*QQ-ABS(TOL1*QQ),ABS(E*QQ)/2)) THEN
!            IF (IDUMP>=1) WRITE(IU,*) 'ZBRENT: extrapolating'
            E=D
            D=P/QQ
          ELSE
! interpolation increase intervall
!            IF (IDUMP>=1) WRITE(IU,*) 'ZBRENT: increasing intervall'
            D=DMAXBR*(B-A)
            E=D
          ENDIF
        ELSE
! bounds decrease to slowly increase intervall
!          IF (IDUMP>=1) WRITE(IU,*) 'ZBRENT: increasing intervall'
          D=DMAXBR*(B-A)
          E=D
        ENDIF
! estimate new function value using B and A
         FAB = (FA+FB)/2

         YD   =(FB+(FAB-FB)/(A-B)*D)*D
         YNEW =YB+YD
! move A to C and last best guess (B) to A
        C =A
        FC=FA
        YC=YA
        A =B
        FA=FB
        YA=YB
        IF(ABS(D) > TOL1) THEN
          B=B+D
        ELSE
          B=B+TOL1
        ENDIF
      XNEW = B
      IFAIL=1
      RETURN
      ELSE
!-----------------------------------------------------------------------
! original brents algorithm take from numberical recipies
! (to me this is absolute mess and not
!  a genius pice of code, but I do not want to change a single line...)
! here we have three points [A,B,C] or [C,B,A] where B is the new guess
!   A is the last best guess
! if the new guess is no improvement or min between [A,B] A is set to C
!    -> secant method is applied
! the new intervall is stored in  [B,C]  B is set to best guess
!-----------------------------------------------------------------------
!  just for savety check whether intervall is correct
        IF ( .NOT.(A<=B .AND. B<=C .OR. C<=B .AND. B<=A)) THEN
          IF (IU>0) THEN
!          WRITE(IU,*)'ZBRENT: fatal internal in brackting'
!          WRITE(IU,*)'        system-shutdown; contact gK immediately'
          ENDIF
         STOP
        ENDIF

        IF((FB>0 .AND. FC>0).OR. (FB<0 .AND. FC<0))THEN
          C=A
          YC=YA
          FC=FA
          D=B-A
          E=D
        ENDIF
! C and B are rearanged so that ABS(FC)>=ABS(FB)
        IF(ABS(FC)<ABS(FB)) THEN
          A=B
          B=C
          C=A
          FA=FB
          FB=FC
          FC=FA
          YA=YB
          YB=YC
          YC=YA
        ENDIF
        TOL1=2.000*EPS*ABS(B)+0.5*TOL
        XM=.5*(C-B)
      IF (IDUMP>=2) THEN
!      WRITE(IU,'(A5,3E14.7)') 'A',A,YA,FA
!      WRITE(IU,'(A5,3E14.7)') 'B',B,YB,FB
!      WRITE(IU,'(A5,3E14.7)') 'C',C,YC,FC
      ENDIF
!     return if accuracy is ok
       IF(ABS(XM)<=TOL1 .OR. FB==0.000)THEN
!          IF (IDUMP>=1) WRITE(IU,*) 'ZBRENT:  accuracy reached'
          IFAIL=0
          RETURN
        ENDIF
        IF(ABS(E)>=TOL1 .AND. ABS(FA)>ABS(FB)) THEN
          S=FB/FA
!  A.eq.C secant method (only one information)
          IF(A==C) THEN
            P=2.000*XM*S
            QQ=1.000-S
!  A.ne.C attempt inverse quadratic interpolation
            ELSE
            QQ=FA/FC
            R=FB/FC
            P=S*(2.000*XM*QQ*(QQ-R)-(B-A)*(R-1.000))
            QQ=(QQ-1.000)*(R-1.000)*(S-1.000)
          ENDIF
          IF(P>0.000) QQ=-QQ
          P=ABS(P)
!  this is the strangest line but it is ok (trust me)
          IF(2.000*P < MIN(3.000*XM*QQ-ABS(TOL1*QQ),ABS(E*QQ))) THEN
!            IF (IDUMP>=1) WRITE(IU,*) 'ZBRENT: interpolating'
            E=D
            D=P/QQ
          ELSE
! interpolation failed bisectioning
!            IF (IDUMP>=1) WRITE(IU,*) 'ZBRENT: bisectioning'
            D=XM
            E=D
          ENDIF
        ELSE
! bounds decrease to slowly bisectioning
!          IF (IDUMP>=1) WRITE(IU,*) 'ZBRENT: bisectioning'
          D=XM
          E=D
        ENDIF
! estimate new function value using B and A
         FAB = (FA+FB)/2

         YD   =(FB+(FAB-FB)/(A-B)*D)*D
         YNEW =YB+YD
! move last best guess to A
        A=B
        FA=FB
        YA=YB
        IF(ABS(D) > TOL1) THEN
          B=B+D
        ELSE
          B=B+SIGN(TOL1,XM)
        ENDIF
      XNEW = B
      IFAIL=1
      RETURN
      ENDIF
      ENDIF

      END
