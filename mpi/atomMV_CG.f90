      SUBROUTINE atomMV_CG(IFLAG,NIONS,TOTEN,A,B,NFREE,POSION,POSIOC, &
     &      FACT,F,FACTSI,FSIF,FL,S,DISMAX,IU6,IU0, &
     &      EBREAK,EDIFFG,E1TEST,LSTOP2,dt,dt_trial,inode_tot,imov_at)
      !USE prec
      !USE lattice
      !USE ini
      !USE chain
!      use data_variable_1

      IMPLICIT REAL(8) (A-H,O-Z)
      
      !INTEGER, PARAMETER :: q =SELECTED_REAL_KIND(10)
      !INTEGER, PARAMETER :: qs=SELECTED_REAL_KIND(5)
      REAL POSOPT(3,NIONS),DPOS(3,NIONS)

      integer imov_at(3,NIONS)
      DIMENSION F(3,NIONS),FL(3,NIONS),S(3,NIONS)
      DIMENSION POSION(3,NIONS),POSIOC(3,NIONS)
      DIMENSION A(3,3),B(3,3)
      DIMENSION TMP(3)
      LOGICAL   LRESET,LTRIAL,LBRENT
      LOGICAL   LSTOP2

      SAVE  AC,FSIFL,SSIF
      DIMENSION AC(3,3),FSIFL(3,3),SSIF(3,3),FSIF(3,3)
      
      !new
      integer :: ionstep=0,correction_step=0
      SAVE ionstep,correction_step
      real*8 max_force

      SAVE TOTEN1,DMOVED,E1ORD1,ORTH,GAMMA,GNORM,GNORMF,GNORML
      SAVE GNORM1,GNORM2,STEP,CURVET,SNORM,SNORMOLD
      SAVE DMOVEL,LTRIAL,LBRENT
      DATA ICOUNT/0/, ICOUNT2/0/, STEP /1.00000/,SNORM/1E-10/
      DATA LTRIAL/.FALSE./
      
      ionstep=ionstep+1

!      if (inode_tot.eq.1) then !ZHANGHUI CHEN
      if (inode_tot.eq.-1) then 
        if(ionstep==1) then
          open(unit=11,file="Progress_relax",status="replace")
        else
          open(unit=11,file="Progress_relax",status="old",position="append")
        endif
        write(11,55) ionstep,TOTEN
     55 format('The ',I3,' SCF step for ionic relaxation. Total Energy:',F10.5) 
        write(11,*) 'cell matrix A='
        write(11,*) A
        write(11,*) 'Position vector X= (in unit of direct lattice)'
        write(11,*) POSION
        write(11,*) 'Force Vector F= (in unit of eV/A with the factor 0.0964*ion_stepsize)'
        write(11,*) F
        close(11)
      endif
      !This is to determine the initial step size (ionstep==1) by the maximum of force
      if(ionstep==1) then
        max_force=0.0
        do NI=1,NIONS
          do I=1,3
            if(dabs(F(I,NI))>max_force) max_force=dabs(F(I,NI))
          enddo
        enddo
        !Maximum of movement: 0.3 Ang
        if(max_force>0.3) STEP=0.3/(max_force)
        if(max_force/FACT>100.0)then
          if (inode_tot.eq.-1) then
             open(unit=11,file="Progress_relax",status="old",position="append")
             write(11,*) "***************************************************************************"
             write(11,*) "***************************************************************************"
             write(11,*) "Warning: too large force:", max_force/FACT,"eV/Ang"
             write(11,*) "You'd better give a good initial structure."
             write(11,*) "If you insist running this job, set a smaller value of ionic_stepsize"
             write(11,*) "***************************************************************************"
             write(11,*) "***************************************************************************"
             close(11)
          endif
        endif
      endif
      
      !open(123,file='POSOPT')
      !read(123,*) POSOPT
      !close(123)
      POSOPT = 0.0
!=======================================================================
!  if IFLAG =0 initialize everything
!=======================================================================
      IF (IFLAG==0) THEN
        DO NI=1,NIONS
        DO I=1,3
          S(I,NI) =0
          FL(I,NI)=0
        ENDDO
        ENDDO

        DO I=1,3
        DO J=1,3
          SSIF (I,J)=0
          FSIFL(I,J)=0
        ENDDO
        ENDDO
        LTRIAL=.FALSE.
        ICOUNT=0
        SNORM =1E-10
        SNORMOLD =1E10
        CURVET=0
      ENDIF

! if previous step was a trial step then continue with line minimization
      IF (LTRIAL) THEN
        GOTO 400
      ENDIF
      
!=======================================================================
!  calculate quantities necessary to conjugate directions
!=======================================================================
      GNORML=0
      GNORM =0
      GNORMF=0
      ORTH  =0
      IF (FACT/=0) THEN
      DO NI=1,NIONS
         GNORML = GNORML+1.00/FACT* &
     &    (FL (1,NI)*FL (1,NI)+FL(2,NI) *FL(2,NI) +FL(3,NI) *FL(3,NI))
         GNORM  = GNORM+ 1.00/FACT*( &
     &    (F(1,NI)-FL(1,NI))*F(1,NI) &
     &   +(F(2,NI)-FL(2,NI))*F(2,NI) &
     &   +(F(3,NI)-FL(3,NI))*F(3,NI))
         GNORMF = GNORMF+1.00/FACT* &
     &    (F(1,NI)*F(1,NI)+F(2,NI)*F(2,NI)+F(3,NI)*F(3,NI))
         ORTH   = ORTH+  1.00/FACT* &
     &    (F(1,NI)*S(1,NI)+F(2,NI)*S(2,NI)+F(3,NI)*S(3,NI))
      ENDDO
      ENDIF

      GNORM1=GNORMF

      IF (ABS(FACTSI)>1E-20) THEN
      DO I=1,3
      DO J=1,3
         GNORML=GNORML+ FSIFL(I,J)*FSIFL(I,J)/FACTSI
         GNORM =GNORM +(FSIF(I,J)-FSIFL(I,J))*FSIF(I,J)/FACTSI
         GNORMF=GNORMF+ FSIF(I,J)* FSIF(I,J)/FACTSI
         ORTH  =ORTH  + FSIF(I,J)* SSIF(I,J)/FACTSI
      ENDDO
      ENDDO
      ENDIF
      GNORM2=GNORMF-GNORM1

      !CALL  sum_chain( GNORM )
      !CALL  sum_chain( GNORML )
      !CALL  sum_chain( GNORMF)
      !CALL  sum_chain( GNORM1)
      !CALL  sum_chain( GNORM2)
      !CALL  sum_chain( ORTH )

!=======================================================================
!  calculate Gamma
!  improve line optimization if necessary
!=======================================================================
      IF (IFLAG==0) THEN
        ICOUNT=0
        GAMMA=0
      ELSE
!       this statement for Polak-Ribiere
        GAMMA=GNORM /GNORML
!       this statement for Fletcher-Reeves
!        GAMMA=GNORMF/GNORML
      ENDIF

      GAMMIN=1.00
      IFLAG=1
!      IF (inode_tot.eq.1) &
!     & WRITE(IU0,30)CURVET,CURVET*GNORMF,CURVET*(ORTH/SQRT(SNORM))**2

!   30 FORMAT(' curvature: ',F6.2,' expect dE=',E10.3,' dE for cont linesearch ',E10.3)
! required accuracy not reached in line minimization
! improve line minimization
! several conditions must be met:

! orthonormality not sufficient
!      WRITE(*,*) "ggyt=",abs(ORTH)*MAX(GAMMA,GAMMIN),GNORMF/5,&
!     &  ABS(CURVET*(ORTH/SQRT(SNORM))**2), LSTOP2,&
!     &  EDIFFG,LBRENT
      IF (ABS(ORTH)*MAX(GAMMA,GAMMIN)>ABS(GNORMF)/5 &
! expected energy change along line search must be larger then required accuracy
     &    .AND. &
     &   ( (EDIFFG>0 .AND. &
     &       ABS(CURVET*(ORTH/SQRT(SNORM))**2)>EDIFFG) &
! or force must be large enough that break condition is not met
     &    .OR. &
     &     (EDIFFG<0 .AND..NOT.LSTOP2) &
     &   ) &
! last call must have been a line minimization
     &    .AND. LBRENT &
!      Weile Jia, comment out. we do NOT use only 4 line minimization in Atomic relax.
     &    .AND. (correction_step<4) &
     &  ) GOTO 400
     

!---- improve the trial step by adding some amount of the optimum step
      IF (ICOUNT/=0) STEP=STEP+0.2000*STEP*(DMOVEL-1)
!---- set GAMMA to (0._q,0._q) if line minimization was not sufficient
      IF (5*ABS(ORTH)*GAMMA>ABS(GNORMF)) THEN
         GAMMA=0
         ICOUNT=0
      ENDIF
!---- if GNORM is very small signal calling routine to stop
      IF (CURVET/=0 .AND.ABS((GNORMF)*CURVET*2)<EDIFFG) THEN
        IFLAG=2
      ENDIF

      ICOUNT=ICOUNT+1
      ICOUNT2=ICOUNT2+1
!-----------------------------------------------------------------------
! performe trial step
!-----------------------------------------------------------------------
      E1ORD1=0
      DMOVED=0
      SNORM =1E-10

!----- set GAMMA to (0._q,0._q) for initial steepest descent steps (Robin Hirschl)
!      have to discuss this
!      IF (ICOUNT2<=NFREE) GAMMA=0
      
      DO NI=1,NIONS
!----- store last gradient
        FL(1,NI)=F(1,NI)
        FL(2,NI)=F(2,NI)
        FL(3,NI)=F(3,NI)
!----- conjugate the direction to the last direction
        S(1,NI) = F(1,NI)+ GAMMA * S(1,NI)
        S(2,NI) = F(2,NI)+ GAMMA * S(2,NI)
        S(3,NI) = F(3,NI)+ GAMMA * S(3,NI)

!-------  Zhang hui and Weile , 2014, 12, 12

!        S(1, NI) = S(1, NI) * imov_at(1, NI)
!        S(2, NI) = S(2, NI) * imov_at(2, NI)
!        S(3, NI) = S(3, NI) * imov_at(3, NI)
        

        IF (FACT/=0) THEN
        SNORM = SNORM+1/FACT*(S(1,NI)*S(1,NI)+S(2,NI)*S(2,NI) +S(3,NI)*S(3,NI))
        ENDIF
      ENDDO
     
!!    deleted 40 lines.  
 
      DO I=1,3
         DO J=1,3
            FSIFL(I,J)=FSIF(I,J)
            AC(I,J)   = A(I,J)
        SSIF(I,J) = FSIF(I,J)+ GAMMA* SSIF(I,J)
      ENDDO
      ENDDO

      IF (ABS(FACTSI)>1E-20) THEN
         DO I=1,3
            DO J=1,3
               SNORM = SNORM  + 1/FACTSI *      SSIF(I,J)* SSIF(I,J)
            ENDDO
         ENDDO
      ENDIF

      !CALL  sum_chain( SNORM)
      
!     if (inode_tot.eq.1) then 
      if (inode_tot.eq.-1) then 
      open(unit=11,file="Progress_relax",status="old",position="append")
        write(11,*) 'Search Vector S='
        write(11,*) S
        write(11,58) SNORM,SNORMOLD
     58 format('|S*S|=',F10.5,'   |SL*SL|=',F10.5)
        write(11,*) ''
      endif

!----- if SNORM increased, rescale STEP (to avoid too large trial steps) 
!      (Robin Hirschl)
      IF (SNORM>SNORMOLD) THEN
         STEP=STEP*(SNORMOLD/SNORM)
      ENDIF
      SNORMOLD=SNORM
      
      DO NI=1,NIONS
!----- search vector from cartesian to direct lattice
         TMP(1) = S(1,NI)
         TMP(2) = S(2,NI)
         TMP(3) = S(3,NI)
         CALL KARDIR(1,TMP,B)
         !if(inode_tot.eq.1) then
         !   WRITE(73,*) "Update in ",NI,"atom"
         !   WRITE(73,*) S(1,NI),S(2,NI),S(3,NI)
         !   WRITE(73,*) TMP
         !endif

!----- trial step in direct grid
         POSIOC(1,NI)= POSION(1,NI)
         POSIOC(2,NI)= POSION(2,NI)
         POSIOC(3,NI)= POSION(3,NI)

         POSION(1,NI)= TMP(1)*STEP+POSIOC(1,NI)
         POSION(2,NI)= TMP(2)*STEP+POSIOC(2,NI)
         POSION(3,NI)= TMP(3)*STEP+POSIOC(3,NI)
         DMOVED= MAX( DMOVED,S(1,NI)*STEP,S(2,NI)*STEP,S(3,NI)*STEP)

!----- keep ions in unit cell (Robin Hirschl)
!        POSION(1,NI)= MOD(POSION(1,NI),1.000)
!        POSION(2,NI)= MOD(POSION(2,NI),1.000)
!        POSION(3,NI)= MOD(POSION(3,NI),1.000)

!----- force * trial step = 1. order energy change
         IF (FACT/=0) THEN
            E1ORD1= E1ORD1 - 1.0 * STEP / FACT * &
     &           (S(1,NI)*F(1,NI)+ S(2,NI)*F(2,NI) + S(3,NI)*F(3,NI))
         ENDIF
      ENDDO

      IF (ABS(FACTSI)>1E-20) THEN
      DO I=1,3
      DO J=1,3
         E1ORD1= E1ORD1 - STEP / FACTSI * SSIF(I,J)* FSIF(I,J)
      ENDDO
      ENDDO
      ENDIF

      DO J=1,3
         DO I=1,3
            A(I,J)=AC(I,J)
            DO K=1,3
               A(I,J)=A(I,J) + SSIF(I,K)*AC(K,J)*STEP
            ENDDO
         ENDDO
      ENDDO


      !CALL  sum_chain( E1ORD1 )

      LRESET = .TRUE.
      X=0
      Y=TOTEN
      FP=E1ORD1
      IFAIL=0

      CALL ZBRENT(IU0,LRESET,EBREAK,X,Y,FP,XNEW,XNEWH,YNEW,YD,IFAIL,inode_tot)
      DMOVEL=1

      IF (inode_tot.eq.1) THEN
!         WRITE(IU0,10) GAMMA,GNORM1,GNORM2,ORTH,STEP
! 10      FORMAT(' trial: gam=',F8.5,' g(F)= ',E10.3, &
!     &        ' g(S)= ',E10.3,' ort =',E10.3,' (trialstep =',E10.3,')')
!         WRITE(IU0,11) SNORM
 11      FORMAT(' search vector abs. value= ',E10.3)
      ENDIF
      TOTEN1= TOTEN
      E1TEST=E1ORD1
      LTRIAL=.TRUE.
      dt=STEP
      dt_trial=STEP
      
      correction_step=0 !a new trial step
!      if (inode_tot.eq.1) then !ZHANGHUI CHEN
!        write(11,7337) ionstep,IFLAG
!   7337 format('This ',I3,' move is a Trial step with the following information: (IFLAG=',I1,')') 
!        write(11,59) STEP,DMOVEL
!     59 format('Step_size=',F8.5,'   DMOVEL=',F8.5)
!        write(11,60) X,Y,FP
!     60 format('X(e.g. A)=',F8.5,'   Y(YA)=',F10.5,'   FP(FA)=',F10.5)
!        write(11,61) Xnew,Ynew,YD
!     61 format('Xnew=',F8.5,'   Ynew=',F10.5,'   YD=',F10.5)
!        write(11,*) '-------------------------------------------------------------------------'
!        write(11,*) ''
!        write(11,*) ''
!        close(11)
!      endif
     
      RETURN
!=======================================================================
! calculate optimal step-length and go to the minimum
!=======================================================================
!-----------------------------------------------------------------------
!  1. order energy change due to displacement at the new position
!-----------------------------------------------------------------------
  400 CONTINUE
      E1ORD2=0
      IF (FACT/=0) THEN
      DO NI=1,NIONS
        E1ORD2= E1ORD2 - 1.0 * STEP / FACT * &
     &  (S(1,NI)*F(1,NI)+ S(2,NI)*F(2,NI) + S(3,NI)*F(3,NI))
      ENDDO
      ENDIF
      
      IFLAG=0
      if (inode_tot.eq.-1) then !ZHANGHUI CHEN
     open(unit=11,file="Progress_relax",status="old",position="append")
        write(11,7373) ionstep,correction_step+1,IFLAG
7373 format('This ',I3,' move is a ',I2,' Correction step with the &
  following information: (IFLAG=',I1,')') 
        write(11,62) X,Y,FP
     62 format('X(e.g. B)=',F8.5,'   Y(YB)=',F10.5,'   FP(FB)=',F10.5)
      endif

      IF (ABS(FACTSI)>1E-20) THEN
      DO I=1,3
      DO J=1,3
        E1ORD2= E1ORD2 - STEP / FACTSI * SSIF(I,J)* FSIF(I,J)
      ENDDO
      ENDDO
      ENDIF

      !CALL  sum_chain( E1ORD2 )
!-----------------------------------------------------------------------
!  calculate position of minimum
!-----------------------------------------------------------------------
      CONTINUE
      LRESET = .FALSE.
      X=DMOVEL
      Y=TOTEN
      FP=E1ORD2
      IFAIL=0

      CALL ZBRENT(IU0,LRESET,EBREAK,X,Y,FP,XNEW,XNEWH,YNEW,YD,IFAIL,inode_tot)
!     estimate curvature
      CURVET=YD/(E1ORD2/STEP/SQRT(SNORM))**2

      DMOVE =XNEW
      DMOVEH=XNEWH

!    previous step was trial step than give long output
      DISMAX=DMOVE*DMOVED
      IF (LTRIAL) THEN
      LBRENT=.TRUE.
      E2ORD  = TOTEN-TOTEN1
      E2ORD2 = (E1ORD1+E1ORD2)/2
!      IF (inode_tot.eq.1) &
!      WRITE(IU0,45) E2ORD,E2ORD2,E1ORD1,E1ORD2
!   45 FORMAT(' trial-energy change:',F12.6,'  1 .order',3F12.6)

      E1TEST=TOTEN1-YNEW
      !DISMAX=DMOVE*DMOVED
!      IF (inode_tot.eq.1) &
!      WRITE(IU0,20) DMOVE*STEP,DMOVEH*STEP,DISMAX,YNEW,YNEW-TOTEN1
!   20 FORMAT(' step: ',F8.4,'(harm=',F8.4,')', &
!     &       '  dis=',F8.5,'  next Energy=',F13.6, &
!     &       ' (dE=',E10.3,')')

!      IF (GAMMA==0) THEN
!       IF ((IU6>=0) .and. (inode_tot .eq. 1)) &
!        WRITE(IU6,*)'Steepest descent step on ions:'
!      ELSE
!      IF ((IU6>=0) .and.(inode_tot .eq. 1)) &
!        WRITE(IU6,*)'Conjugate gradient step on ions:'
!      ENDIF

!      IF ((IU6>=0) .and. (inode_tot .eq. 1) ) &
!      WRITE(IU6,40) E2ORD,(E1ORD1+E1ORD2)/2,E1ORD1,E1ORD2, &
!     &         GNORM,GNORMF,GNORML, &
!     &         GNORM1,GNORM2,ORTH,GAMMA,STEP, &
!     &         DMOVE*STEP,DMOVEH*STEP,DISMAX,YNEW,YNEW-TOTEN1

!   40 FORMAT(' trial-energy change:',F12.6,'  1 .order',3F12.6/ &
!     &       '  (g-gl).g =',E10.3,'      g.g   =',E10.3, &
!     &       '  gl.gl    =',E10.3,/ &
!     &       ' g(Force)  =',E10.3,'   g(Stress)=',E10.3, &
!     &       ' ortho     =',E10.3,/ &
!     &       ' gamma     =',F10.5,/ &
!     &       ' trial     =',F10.5,/ &
!     &       ' opt step  =',F10.5,'  (harmonic =',F10.5,')', &
!     &       ' maximal distance =',F10.8/ &
!     &       ' next E    =',F13.6,'   (d E  =',F10.5,')')
      ELSE
!      IF (inode_tot.eq.1) &
!      WRITE(IU0,25) DMOVE*STEP,YNEW,YNEW-TOTEN1
!   25 FORMAT(' opt : ',F8.4,'  next Energy=',F13.6, &
!     &       ' (dE=',E10.3,')')
!    do not make another call to ZBRENT if reuqired accuracy was reached
      IF (ABS(YD)<EDIFFG) LBRENT=.FALSE.
      ENDIF
!-----------------------------------------------------------------------
!    move ions to the minimum
!-----------------------------------------------------------------------
      DO NI=1,NIONS
!----- search vector from cartesian to direct lattice
        TMP(1) = S(1,NI)
        TMP(2) = S(2,NI)
        TMP(3) = S(3,NI)
        CALL KARDIR(1,TMP,B)

        POSIOC(1,NI)= POSION(1,NI)
        POSIOC(2,NI)= POSION(2,NI)
        POSIOC(3,NI)= POSION(3,NI)

        POSION(1,NI)= TMP(1)*(DMOVE-DMOVEL)*STEP+POSIOC(1,NI)
        POSION(2,NI)= TMP(2)*(DMOVE-DMOVEL)*STEP+POSIOC(2,NI)
        POSION(3,NI)= TMP(3)*(DMOVE-DMOVEL)*STEP+POSIOC(3,NI)

!----- keep ions in unit cell (Robin Hirschl)
!       POSION(1,NI)=MOD(POSION(1,NI),1.0)
!       POSION(2,NI)=MOD(POSION(2,NI),1.0)
!       POSION(3,NI)=MOD(POSION(3,NI),1.0)
        
      ENDDO

      DO J=1,3
      DO I=1,3
      A(I,J)=AC(I,J)
      DO K=1,3
        A(I,J)=A(I,J) + SSIF(I,K)*AC(K,J)*DMOVE*STEP
      ENDDO
      ENDDO
      ENDDO
      DMOVEL=DMOVE
      LTRIAL=.FALSE.
      dt=dt+(DMOVE-DMOVEL)*STEP
      
      correction_step=correction_step+1
!#ifdef TIMING
!      if (inode_tot.eq.1) then !ZHANGHUI CHEN
!        write(11,63) Xnew,Ynew,YD
!     63 format('Xnew(DMOVE)=',F9.5,'   Ynew=',F10.5,'   YD=',F10.5)
!        write(11,*) '-------------------------------------------------------------------------'
!        write(11,*) ''
!        write(11,*) ''
!        close(11)
!      endif
!#endif
      
      RETURN

      END

!**************** SUBROUTINE KARDIR ************************************
! transform a set of vectors from cartesian coordinates to
! ) direct lattice      (BASIS must be equal to B reciprocal lattice)
! ) reciprocal lattice  (BASIS must be equal to A direct lattice)
!***********************************************************************
        
      SUBROUTINE KARDIR(NMAX,V,BASIS)
      !USE prec
      IMPLICIT REAL(8) (A-H,O-Z)
      DIMENSION V(3,NMAX),BASIS(3,3)
      
      DO N=1,NMAX
        V1=V(1,N)*BASIS(1,1)+V(2,N)*BASIS(2,1)+V(3,N)*BASIS(3,1)
        V2=V(1,N)*BASIS(1,2)+V(2,N)*BASIS(2,2)+V(3,N)*BASIS(3,2)
        V3=V(1,N)*BASIS(1,3)+V(2,N)*BASIS(2,3)+V(3,N)*BASIS(3,3)
        V(1,N)=V1
        V(2,N)=V2
        V(3,N)=V3
      ENDDO
      
      RETURN
      END SUBROUTINE



!*********************************************************************
! RCS:  $Id: brent.F,v 1.1 2000/11/15 08:13:54 kresse Exp $
! subroutine ZBRENT
!  variant of Brents root finding method used for
!  minimization of a function
!
!  derivatives and function values have to be supplied
!  the derivatives are considered to be more accurate than the
!  function values !
! ICALL determines behavior and is incremented by the routine
! until a external reset is 1._q
! ICALL
!  0      unit step is 1._q i.e. XNEW=1
!  1      cubic interpolation / extrapolation to minimum
!         hopefully 2. call will give sufficient precision
!  2      init Brents method
!  3      variant of Brendts method
!          bracketing, bisectioning + inverse quadratic interpolation
!          using derivatives only
!
!  on call
!  LRESET    restart algorithm
!  EBREAK    break condition for using the cubic interpolation
!  X,Y,F     position and values for function and derivatives at X
!  on exit
!  XNEW,YNEW position where function has to be evalueated
!            and expected function value for this position
!  XNEWH     harmonic step with calculated for ICALL=1
!  YD        expected change of energy in this step relative to the
!             previous step
!
!*********************************************************************

      SUBROUTINE ZBRENT(IU,LRESET,EBREAK,X,Y,F,XNEW,XNEWH,YNEW,YD,IFAIL,inode_tot)
      !USE prec

      IMPLICIT REAL(8) (A-H,O-Z)
      REAL(8) KOEFF1,KOEFF2,KOEFF3

      LOGICAL LRESET,LBRACK
      SAVE ICALL,LBRACK,A,B,YA,YB,FA,FB,C,FC,YC,FSTART
      SAVE D,E,XM
      PARAMETER (EPS=1E-8,TOL=1E-8)
      INTEGER ierror

!     parameter controls the maximum step width
!     DMAX is maximum step for 1. step
!     DMAXBR is maximum step for Brents algorithm, we use golden ratio
      PARAMETER (DMAX=4,DMAXBR=2)
      DATA ICALL/0/
!     minimum not found up to now
      IFAIL=1
      IDUMP=1
      IF (IU<0) IDUMP=0

      IF (LRESET) ICALL=0
!-----------------------------------------------------------------------
!    case 0: trial step 1, into trial direction
!-----------------------------------------------------------------------
      IF (ICALL==0) THEN
        A   =X
        YA  =Y
        FA  =F
        YD  =F
        YNEW=Y+F
        XNEW=X+1
        ICALL=1
        IFAIL=1
      RETURN
      ENDIF
!-----------------------------------------------------------------------
!    case 1:  cubic interpolation / extrapolation
!    if precision of energy is not sufficient use secant method
!-----------------------------------------------------------------------
      IF (ICALL==1) THEN
      B   =X
      YB  =Y
      FB  =F

      FFIN= YB-YA
      FAB = (FA+FB)/2

      KOEFF3=3.00*(FB+   FA-2.00*FFIN)
      KOEFF2=    FB+2.00*FA-3.00*FFIN
      KOEFF1=    FA
!----cubic extrapolation - secant method break condition
      IF ( (ABS(KOEFF3/KOEFF1)< 1E-2)  .OR. &
     &     (ABS(KOEFF3)       < EBREAK*24.0) .OR. &
     &     ( (KOEFF1*KOEFF3/KOEFF2/KOEFF2) >= 1.00 )) THEN

!----- harmonic  case
         DMOVE  = -FA/(FAB-FA)/2.00
         DMOVEH = -FA/(FAB-FA)/2.00
         IF (DMOVE<0) THEN
           DMOVE =DMAX
           DMOVEH=DMAX
         ENDIF
         DY=(FA+(FAB-FA)*DMOVE)*DMOVE
         YNEW =YA+DY
      ELSE

!----- anharmonic interpolation (3rd order in energy) (jF)
         DMOVE1=KOEFF2/KOEFF3*(1.00-SQRT(1.00-KOEFF1*KOEFF3/KOEFF2/KOEFF2))
         DMOVE2=KOEFF2/KOEFF3*(1.00+SQRT(1.00-KOEFF1*KOEFF3/KOEFF2/KOEFF2))
!----- 3rd order polynomial has (1._q,0._q) minimum and (1._q,0._q) maximum ... :
         DY1=-(KOEFF1-(KOEFF2-KOEFF3*DMOVE1/3.00)*DMOVE1)*DMOVE1
         DY2=-(KOEFF1-(KOEFF2-KOEFF3*DMOVE2/3.00)*DMOVE2)*DMOVE2
!----- select the correct extremum:
         IF (DY1>DY2) THEN
           DMOVE=DMOVE1
           DY=DY1
         ELSE
           DMOVE=DMOVE2
           DY=DY2
         ENDIF
         YNEW=YA-DY

         DMOVEH = -FA/(FAB-FA)/2.00
         IF (DMOVEH<0) DMOVEH=DMAX
!-----  extremely unharmonic and/or
!       minimum should be on the right  side of B but it is due to
!       3. order interpolation on left side
!     -> take the save harmonic extrapolation
         IF (DMOVE>(2*DMOVEH).OR. DMOVEH>(2*DMOVE) &
     &   .OR. (FA*FB>0 .AND.DMOVE<1.00 ) &
     &   .OR. (FA*FB<0 .AND.DMOVE>1.00 ))THEN
           DMOVE=DMOVEH
           DY   =(FA+(FAB-FA)*DMOVE)*DMOVE
           YNEW =YA+DY
         ENDIF
      ENDIF

      IF (DMOVE>DMAX) DMOVE=DMAX
      IF (inode_tot.eq.1) THEN
!      IF (DMOVE== DMAX .AND. IDUMP>=1) &
!     &     WRITE(IU,*)'PWmat: can''t locate minimum, use default step'
      ENDIF
      XNEW =DMOVE+A
      XNEWH=DMOVEH+A
!     estimated change relative to B
      YD   =(FB+(FAB-FB)/(A-B)*(DMOVE-B))*(DMOVE-B)
      ICALL=2
      IFAIL=1
      RETURN
      ENDIF
!-----------------------------------------------------------------------
!    case 2:  cubic interpolation / extrapolation failed
!      and was not accurate enough
!      start to use Brents algorithm
!-----------------------------------------------------------------------
      IF (ICALL==2) THEN
      LBRACK=.TRUE.
!  1.  X > B    start intervall [B,X]
      IF (X>=B) THEN
         A =B
         YA=YB
         FA=FB
         B =X
         YB=Y
         FB=F
!     check for bracketing
      IF (FA*FB>0) LBRACK=.FALSE.
      FSTART=FA
!  2.  X < B
      ELSE
        IF (FA*F<=0) THEN
!  2a. minimum between [A,X]
          B   =X
          YB  =Y
          FB  =F
!  2b. minimum between [X,B]
        ELSE IF (FB*F<=0) THEN
           A =B
           YA=YB
           FA=FB
           B   =X
           YB  =Y
           FB  =F
!  2c. here we have some serious problems no miniumum between [A,B]
!      but X (search lies between) [A,B] -> complete mess
!      happens only beacuse of cubic interpolations
!      work-around search between [X,B] LBRACK=.FALSE.
        ELSE
          IF (IDUMP>=1) THEN
!           IF (inode_tot.eq.1) THEN
!             WRITE(IU,*)'ZBRENT:  no minimum in in bracket'
!           ENDIF
          ENDIF
          A =B
          YA=YB
          FA=FB
          B   =X
          YB  =Y
          FB  =F
          LBRACK=.FALSE.
        ENDIF
      ENDIF
      ICALL=3
      C =B
      FC=FB
      YC=YB
      ENDIF
!-----------------------------------------------------------------------
!  fall trough to this line for ICALL > 4 set: B=X
!-----------------------------------------------------------------------
      IF (ICALL>=4) THEN
        B =X
        FB=F
        YB=Y
!   maybe a bracketing interval was found ?
        IF (.NOT. LBRACK.AND. FSTART*F < 0) THEN
!   if bracketing is started forget C
          LBRACK=.TRUE.
           C =B
           FC=FB
           YC=YB
           IF (IDUMP/=0) THEN
!              IF (inode_tot.eq.1) THEN
!                WRITE(IU,*)'ZBRENT: bracketing found'
!              ENDIF
           ENDIF
        ENDIF
      ENDIF
!-----------------------------------------------------------------------
! modified brent algorithm if no bracketing intervall exists
! here we have three points [C,A] and B, where B is the new guess
! and lies at the right  hand side of A
! A is the last best guess
!-----------------------------------------------------------------------
      IF (ICALL>=3) THEN
      ICALL=ICALL+1
      IF (ICALL==20) THEN
        IF (IDUMP>=1) THEN
!          IF (inode_tot.eq.1) THEN
!           WRITE(IU,*)'ZBRENT:  can not reach accuracy'
!          ENDIF
         ENDIF
        IFAIL=2
        RETURN
      ENDIF
      IF (IDUMP>=2) THEN
!        IF (inode_tot.eq.1) THEN
!            WRITE(IU,*)
!            WRITE(IU,'(A5,3E14.7)') 'A',A,YA,FA
!            WRITE(IU,'(A5,3E14.7)') 'B',B,YB,FB
!            WRITE(IU,'(A5,3E14.7)') 'C',C,YC,FC
!            WRITE(IU,*) LBRACK,D,E
!        ENDIF
      ENDIF


      IF (.NOT.LBRACK) THEN
! ABS(FC) < ABS(FB) or AC < BA
        IF(ABS(FC)<=ABS(FB).OR.(A-C)<(B-A)) THEN
          C=A
          FC=FA
          YC=YA
          D=B-A
          E=B-A
        ENDIF
        TOL1=2.00*EPS*ABS(B)+0.500*TOL
        XM=.500*(C-B)
      IF (IDUMP>=2) THEN
!        IF (inode_tot.eq.1) THEN
!            WRITE(IU,'(A5,3E14.7)') 'A',A,YA,FA
!            WRITE(IU,'(A5,3E14.7)') 'B',B,YB,FB
!            WRITE(IU,'(A5,3E14.7)') 'C',C,YC,FC
!        ENDIF
      ENDIF
! just for savety check for correct ordering
      IF ( .NOT.(C<=A .AND. A<=B) ) THEN
        IF (IU>0) THEN
!           IF (inode_tot.eq.1) THEN
!                WRITE(IU,*)'ZBRENT: fatal error: bracketing interval incorrect'
!                WRITE(IU,*)'    please rerun with smaller EDIFF, or copy CONTCAR to'
!                WRITE(IU,*)'    to POSCAR and continue'
!           ENDIF
        ENDIF
!      CALL MPI_finalize( ierror ) 
       stop
      ENDIF
!     return if accuracy ca not be improved
       IF(ABS(XM)<=TOL1 .OR. FB==0.00)THEN
          IF (IDUMP>=1) THEN
!           IF (inode_tot.eq.1) THEN
!            WRITE(IU,*) 'ZBRENT:  accuracy reached'
!           ENDIF
          ENDIF
          IFAIL=0
          RETURN
        ENDIF
        IF(ABS(E)>=TOL1 .AND. ABS(FA)>ABS(FB)) THEN
          S=FB/FA
!  A.eq.C secant method (only (1._q,0._q) information)
          IF(A==C) THEN
            P=2.00*XM*S
            QQ=1.00-S
!  A.ne.C attempt inverse quadratic interpolation
            ELSE
            QQ=FA/FC
            R=FB/FC
            P=S*(2.00*XM*QQ*(QQ-R)-(B-A)*(R-1.00))
            QQ=(QQ-1.00)*(R-1.00)*(S-1.00)
          ENDIF
          IF(P>0.00) QQ=-QQ
          P=ABS(P)
!  are we within the bounds; correct but tricky :-<
          IF (IDUMP>=2) THEN
!           IF (inode_tot.eq.1) THEN
!             WRITE(IU,*)'would go to ',A+P/QQ,P/QQ,E
!           ENDIF
          ENDIF
          IF(P < MIN(DMAXBR*(B-A)*QQ-ABS(TOL1*QQ),ABS(E*QQ)/2)) THEN
            IF (IDUMP>=1) THEN
!              IF (inode_tot.eq.1) THEN
!                   WRITE(IU,*) 'ZBRENT: extrapolating'
!              ENDIF
            ENDIF
            E=D
            D=P/QQ
          ELSE
! interpolation increase intervall
            IF (IDUMP>=1) THEN
!              IF (inode_tot.eq.1) THEN
!                    WRITE(IU,*) 'ZBRENT: increasing intervall'
!              ENDIF
            ENDIF
            D=DMAXBR*(B-A)
            E=D
          ENDIF
        ELSE
! bounds decrease to slowly increase intervall
          IF (IDUMP>=1) THEN
!           IF (inode_tot.eq.1) THEN
!            WRITE(IU,*) 'ZBRENT: increasing intervall'
!           ENDIF
          ENDIF
          D=DMAXBR*(B-A)
          E=D
        ENDIF
! estimate new function value using B and A
         FAB = (FA+FB)/2

         YD   =(FB+(FAB-FB)/(A-B)*D)*D
         YNEW =YB+YD
! move A to C and last best guess (B) to A
        C =A
        FC=FA
        YC=YA
        A =B
        FA=FB
        YA=YB
        IF(ABS(D) > TOL1) THEN
          B=B+D
        ELSE
          B=B+TOL1
        ENDIF
      XNEW = B
      IFAIL=1
      RETURN
      ELSE
!-----------------------------------------------------------------------
! original brents algorithm take from numberical recipies
! (to me this is absolute mess and not
!  a genius pice of code, but I do not want to change a single line...)
! here we have three points [A,B,C] or [C,B,A] where B is the new guess
!   A is the last best guess
! if the new guess is no improvement or min between [A,B] A is set to C
!    -> secant method is applied
! the new intervall is stored in  [B,C]  B is set to best guess
!-----------------------------------------------------------------------
!  just for savety check whether intervall is correct
        IF ( .NOT.(A<=B .AND. B<=C .OR. C<=B .AND. B<=A)) THEN
          IF (IU>0) THEN
!           IF (inode_tot.eq.1) THEN
!                WRITE(IU,*)'ZBRENT: fatal internal in brackting'
!                WRITE(IU,*)'        system-shutdown; contact gK immediately'
!            ENDIF
          ENDIF
!        call MPI_finalize( ierror )
         stop
        ENDIF

        IF((FB>0 .AND. FC>0).OR. (FB<0 .AND. FC<0))THEN
          C=A
          YC=YA
          FC=FA
          D=B-A
          E=D
        ENDIF
! C and B are rearanged so that ABS(FC)>=ABS(FB)
        IF(ABS(FC)<ABS(FB)) THEN
          A=B
          B=C
          C=A
          FA=FB
          FB=FC
          FC=FA
          YA=YB
          YB=YC
          YC=YA
        ENDIF
        TOL1=2.00*EPS*ABS(B)+0.500*TOL
        XM=.500*(C-B)
      IF (IDUMP>=2) THEN
!        IF (inode_tot.eq.1) THEN
!            WRITE(IU,'(A5,3E14.7)') 'A',A,YA,FA
!            WRITE(IU,'(A5,3E14.7)') 'B',B,YB,FB
!            WRITE(IU,'(A5,3E14.7)') 'C',C,YC,FC
!        ENDIF
      ENDIF
!     return if accuracy is ok
       IF(ABS(XM)<=TOL1 .OR. FB==0.00)THEN
          IF (IDUMP>=1) THEN
!            IF (inode_tot.eq.1) THEN
!                WRITE(IU,*) 'ZBRENT:  accuracy reached'
!            ENDIF
          ENDIF
          IFAIL=0
          RETURN
        ENDIF
        IF(ABS(E)>=TOL1 .AND. ABS(FA)>ABS(FB)) THEN
          S=FB/FA
!  A.eq.C secant method (only (1._q,0._q) information)
          IF(A==C) THEN
            P=2.00*XM*S
            QQ=1.00-S
!  A.ne.C attempt inverse quadratic interpolation
            ELSE
            QQ=FA/FC
            R=FB/FC
            P=S*(2.00*XM*QQ*(QQ-R)-(B-A)*(R-1.00))
            QQ=(QQ-1.00)*(R-1.00)*(S-1.00)
          ENDIF
          IF(P>0.00) QQ=-QQ
          P=ABS(P)
!  this is the strangest line but it is ok (trust me)
          IF(2.00*P < MIN(3.00*XM*QQ-ABS(TOL1*QQ),ABS(E*QQ))) THEN
            IF (IDUMP>=1) THEN
!              IF (inode_tot.eq.1) THEN
!                  WRITE(IU,*) 'ZBRENT: interpolating'
!              ENDIF
            ENDIF
            E=D
            D=P/QQ
          ELSE
! interpolation failed bisectioning
            IF (IDUMP>=1) THEN
!               IF (inode_tot.eq.1)  THEN 
!                  WRITE(IU,*) 'ZBRENT: bisectioning'
!               ENDIF
            ENDIF
            D=XM
            E=D
          ENDIF
        ELSE
! bounds decrease to slowly bisectioning
          IF (IDUMP>=1) THEN
!            IF (inode_tot.eq.1) THEN
!               WRITE(IU,*) 'ZBRENT: bisectioning'
!            ENDIF
          ENDIF
          D=XM
          E=D
        ENDIF
! estimate new function value using B and A
         FAB = (FA+FB)/2

         YD   =(FB+(FAB-FB)/(A-B)*D)*D
         YNEW =YB+YD
! move last best guess to A
        A=B
        FA=FB
        YA=YB
        IF(ABS(D) > TOL1) THEN
          B=B+D
        ELSE
          B=B+SIGN(TOL1,XM)
        ENDIF
      XNEW = B
      IFAIL=1
      RETURN
      ENDIF
      ENDIF

      END
      
      
  subroutine LJfitting(natom,axis,posion,F,epison,sigma)
      
      integer natom,i,j,k,k1,k2,k3,info
      real*8 axis(3,3),posion(3,natom),xyz(3,natom)
      real*8 F(3,natom),epison(natom,natom),sigma(natom,natom)
      real*8 AX(natom,natom),AY(natom,natom),AZ(natom,natom),A(3*natom,natom*natom)
      real*8 Y(3*natom),Y2(max(3*natom,natom*(natom-1)/2)),Y3(max(3*natom,natom*(natom-1)/2))  
      real*8 B(3*natom,natom*(natom-1)/2),Bold(3*natom,natom*(natom-1)/2),P(natom,natom)
      real*8 EX(natom)
      real*8 work(60000),S(min(3*natom,natom*(natom-1)/2))
      real*8 rij,mid
      integer RANK,JPVT(natom*(natom-1)/2)
      !1. n*n matrix
      AX=0
      AY=0
      AZ=0
      xyz=0
      do i=1,natom
        do j=1,3
          xyz(1,i)=xyz(1,i)+posion(j,i)*axis(j,1)
        enddo
        do j=1,3
          xyz(2,i)=xyz(2,i)+posion(j,i)*axis(j,2)
        enddo
        do j=1,3
          xyz(3,i)=xyz(3,i)+posion(j,i)*axis(j,3)
        enddo
      enddo     
        do i=1,natom
            do j=1,natom
                if (i==j)  cycle
                rij=sqrt((xyz(1,i)-xyz(1,j))**2+(xyz(2,i)-xyz(2,j))**2+(xyz(3,i)-xyz(3,j))**2)
                AX(i,j)=(xyz(1,i)-xyz(1,j))/rij
                AY(i,j)=(xyz(2,i)-xyz(2,j))/rij
                AZ(i,j)=(xyz(3,i)-xyz(3,j))/rij
            enddo
        enddo
        !2. convert to 3n*n^2 matrix
        A=0
        do i=1,natom
             do j=1,natom
                A(i,(i-1)*natom+j)=AX(i,j)
                A(i+natom,(i-1)*natom+j)=AY(i,j)
                A(i+2*natom,(i-1)*natom+j)=AZ(i,j)
             enddo
        enddo
        do i=1,natom
            Y(i)=F(1,i)
            Y(i+natom)=F(2,i)
            Y(i+2*natom)=F(3,i)
        enddo
        !X=pinv(A)*Y
        !3. convert to 3n*(n(n-1)/2) matrix 
        B=0
        do i=2,natom
            do j=1,i-1        
                k1=(i-1)*natom+j
                k2=(j-1)*natom+i
                k3=(i-2)*(i-1)/2+j
                do k=1,3*natom 
                    B(k,k3)=A(k,k1)+A(k,k2)
                enddo
            enddo
        enddo
!        if(inode_tot .eq. 1) write(*,*) "B"
!        do i=1,3*natom
!            if(inode_tot .eq. 1) write(*,*) B(i,:)
!        enddo
!        if(inode_tot .eq. 1) write(*,*) "Y"
!        if(inode_tot .eq. 1) write(*,*) Y
        Y2=0
        do i=1,3*natom
           Y2(i)=Y(i)
        enddo
        Bold=B
        call DGELS('N', 3*natom, natom*(natom-1)/2, 1, B, 3*natom, Y2, max(3*natom,natom*(natom-1)/2), work, 60000, info)
        !call DGELSS( 3*natom, natom*(natom-1)/2, 1, B, 3*natom, Y2, max(3*natom,natom*(natom-1)/2), S, 0.0001, RANK, work, 60000, info )
        !JPVT=0
        !call DGELSS( 3*natom, natom*(natom-1)/2, 1, B, 3*natom, Y2, max(3*natom,natom*(natom-1)/2), JPVT, 0.0001, RANK, work, 60000, info )
!        if(inode_tot .eq. 1) write(*,*) "-------Solution--------"
!        if(inode_tot .eq. 1) write(*,*) Y2
        Y3=0
        do i=1,3*natom
           do j=1,natom*(natom-1)/2
              Y3(i)=Y3(i)+Bold(i,j)*Y2(j) 
           enddo
        enddo
!        if(inode_tot .eq. 1) write(*,*) "B*Y"
!        if(inode_tot .eq. 1) write(*,*) Y3
        !4. back to n*n matrix
        P=0
        do i=2,natom
            do j=1,i-1        
                k=(i-2)*(i-1)/2+j
                P(i,j)=Y2(k)
                P(j,i)=Y2(k)
            enddo
        enddo
!        if(inode_tot .eq. 1) write(*,*) P
!        if(inode_tot .eq. 1) write(*,*) AX
        !5. verify the results
        EX=0
        do i=1,natom
            do j=1,natom
              EX(i)=EX(i)+AX(i,j)*P(i,j)
            enddo
            EX(i)=EX(i)-F(1,i)
        enddo
!        if(inode_tot .eq. 1) write(*,*) "-------Error--------"
!        if(inode_tot .eq. 1) write(*,*) AX
!        if(inode_tot .eq. 1) write(*,*) F
!        if(inode_tot .eq. 1) write(*,*) EX
        !6. sigma
        epison=0.4
        sigma=0
        do i=2,natom
            do j=1,i-1        
                rij=sqrt((xyz(1,i)-xyz(1,j))**2+(xyz(2,i)-xyz(2,j))**2+(xyz(3,i)-xyz(3,j))**2)
                mid=0.25+sqrt(1/16-rij*P(i,j)/(2*epison(i,j)))
                sigma(i,j)=rij*(mid**(1/6))
                sigma(j,i)=sigma(i,j)
            enddo
        enddo
!        if(inode_tot .eq. 1) write(*,*) sigma
        
        end subroutine


