#
#
# bond switching algorithm (WWW) to generate amorphous SiO2
#
#

import sys
import numpy as np
import copy


MAXITER = 100
TYPICAL_BONDLENGTH = {'SiSi':3.2,'SiO':1.7,'OSi':1.7}   # A: Si;  B: O

kb_sio = 27.0  # ev/A^2
ktheta_siosi = 0.75 # eV
ktheta_osio = 4.32  # eV
bsio = 1.6 # Angstrom
costheta_siosi = -1.0 
costheta_osio = -1.0/3.0


def gen_initial_bonds( Name, Coord, Lat ):
    BondsSiSi = []; BondsSiO = []
    Natom = len(Name)
    Si_AtomList = [ x for x in range(0,Natom) if Name[x] == 'Si' ]
    O_AtomList  = [ x for x in range(0,Natom) if Name[x] == 'O'  ]
    for i,iatom in enumerate(Si_AtomList):
        SingleAtomSiSiBonds = []
        SingleAtomSiOBonds = []
        for j,jatom in enumerate(Si_AtomList):
            dist, distdir = distance( Coord[iatom,:], Coord[jatom,:], Lat )
            if dist > 0.01 and dist <= TYPICAL_BONDLENGTH['SiSi']:
                SingleAtomSiSiBonds.append([iatom, jatom])
        for k,katom in enumerate(O_AtomList):
            dist, distdir = distance( Coord[iatom,:], Coord[katom,:], Lat )
            if dist > 0.01 and dist <= TYPICAL_BONDLENGTH['SiO']:
                SingleAtomSiOBonds.append([iatom, katom])
        if len(SingleAtomSiSiBonds) != 4: print 'Non four bonds for Si-Si', len(SingleAtomSiSiBonds), iatom, SingleAtomSiSiBonds; sys.exit()
        BondsSiSi.append(SingleAtomSiSiBonds)
        if len(SingleAtomSiOBonds) != 4: print 'Non four bonds for Si-O', len(SingleAtomSiOBonds), iatom, SingleAtomSiOBonds; sys.exit()
        BondsSiO.append(SingleAtomSiOBonds)
    return BondsSiSi, BondsSiO

def gen_sisio_bond( BondsSiSi, BondsSiO ):
    BondsSiSi_Copy = copy.deepcopy(BondsSiSi)
    for iSi,iSiAtom in enumerate(BondsSiSi_Copy):
        iatom = iSiAtom[0][0]
        for jSi,jSiAtom in enumerate(iSiAtom):
            jatom = jSiAtom[1]
            for j, jbond in enumerate( BondsSiO ):  # here, BondsSiO is not flat
                if jbond[0][0] == iatom: iatomBond = [ x[1] for x in jbond ]
                if jbond[0][0] == jatom: jatomBond = [ x[1] for x in jbond ]
            CommonOAtom = list(set(iatomBond).intersection(jatomBond))
            if len(CommonOAtom) == 0: print 'Dangling Si-O bond, exit'; ibond, iatomBond, jatomBond
            BondsSiSi_Copy[iSi][jSi].append( CommonOAtom[0] )
    return BondsSiSi_Copy
        
def gen_angles(BondsSiSiO,BondsSiO,Coord,Lat):
    AngleSiOSi = []; AngleOSiO = []
    BondsSiSiO_Flat = copy.deepcopy( sum(BondsSiSiO,[]) )
    BondsSiSiO_Flat_AtomIndex = [ [x[2],x[0],x[1]] for x in BondsSiSiO_Flat ]
    BondsSiSiO_Flat_AtomIndex_NoDup = []
    for i, ibond in enumerate( BondsSiSiO_Flat_AtomIndex ):
        if ibond not in BondsSiSiO_Flat_AtomIndex_NoDup and [ibond[0],ibond[2],ibond[1]] not in BondsSiSiO_Flat_AtomIndex_NoDup:
            BondsSiSiO_Flat_AtomIndex_NoDup.append( ibond )
    for iSi, iSiAtom in enumerate( BondsSiSiO_Flat_AtomIndex_NoDup ):
        Si1 = iSiAtom[1]; Si2 = iSiAtom[2]; OAtom = iSiAtom[0]
#       distOSi1, distdirOSi1 = distance( Coord[OAtom,:], Coord[Si1,:], Lat )
#       distOSi2, distdirOSi2 = distance( Coord[OAtom,:], Coord[Si2,:], Lat )
        AngleSiOSi.append( [OAtom,Si1,Si2] )
    for iSi, iSiAtom in enumerate( BondsSiO ):
        SiAtom = iSiAtom[0][0]
        AngleOSiO.append([])
        AngleOSiO[-1].append( [SiAtom,iSiAtom[0][1],iSiAtom[1][1]] )
        AngleOSiO[-1].append( [SiAtom,iSiAtom[0][1],iSiAtom[2][1]] )
        AngleOSiO[-1].append( [SiAtom,iSiAtom[0][1],iSiAtom[3][1]] )
        AngleOSiO[-1].append( [SiAtom,iSiAtom[1][1],iSiAtom[2][1]] )
        AngleOSiO[-1].append( [SiAtom,iSiAtom[1][1],iSiAtom[3][1]] )
        AngleOSiO[-1].append( [SiAtom,iSiAtom[2][1],iSiAtom[3][1]] )
    return AngleSiOSi, AngleOSiO

def remove_dup( Bonds ):
    Bonds_NoDup = []
    for i in Bonds:
        if i not in Bonds_NoDup:
           Bonds_NoDup.append(i)
    return Bonds_NoDup

def print_neighborlist( BondsSiO, AngleOSiO, AngleSiOSi ):
    with open('Si-O.bond.dat','w') as f:
        for iSi in BondsSiO:
            for jSi in iSi:
                f.write( '%d %d\n'%(jSi[0],jSi[1]) )
    with open('O-Si-O.angle.dat','w') as g:
        for iSi in AngleOSiO:
            for jAngle in iSi:
                g.write('%d %d %d\n'%(jAngle[0],jAngle[1],jAngle[3]))
    with open('Si-O-Si.bond-angle.dat','w') as h:
        for iO in AngleSiOSi:
            OAtom = iO[0]
            h.write( '%d %d %d\n'%(OAtom,iO[1],iO[3]) )

def _read_coord_single_file( filename ):
    # use Angstrom unit
    with open(filename,'r') as f:
        raw = f.readlines()
    coord = []; name = []; lat = np.zeros( (3,3) )
    for order,i in enumerate(raw[0:3]):
        tmp_ = [float(j.strip().strip('\n')) for j in i.split()]
        lat[order,:] = np.array( tmp_ )
    lat_const = float(raw[3].strip().strip('\n'))
    lat = lat * lat_const   # lat is Angstrom
    if len(raw) > 4:
        for i in raw[4:]:
            tmp_ = [j.strip().strip('\n') for j in i.split()]
            name.append(tmp_[0])
            tmp_2 = map(float, tmp_[1:4])
            coord.append( tmp_2 )
    else:
        print 'No coordinates found, exit'; sys.exit()
    return name, lat, np.array( coord ), np.array( coord )  # name, lattice vector, coord_frac, coord_cart

def distance( coord1, coord2, Lat ):
    '''check x, y direction periodic boundary condition'''
    dist = []; distDirection = []
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            for z in [-1, 0, 1]:
                dist.append( np.linalg.norm( coord1 - (coord2+x*Lat[0,:]+y*Lat[1,:]+z*Lat[2,:]) ) )
                distDirection.append([x,y,z])
    SmallestDistIndex = np.argmin( np.array(dist) )
    return dist[SmallestDistIndex], distDirection[SmallestDistIndex]

def sio_bond_energy( dist ):
    return kb_sio*(dist-bsio)**2

def osio_angle_energy( r21, dist1, r31, dist2 ):
    return ktheta_osio*( np.dot(r21,r31)/(dist1*dist2)-costheta_osio )**2

def siosi_angle_energy( r21, dist1, r31, dist2 ):
    return ktheta_siosi*( np.dot(r21,r31)/(dist1*dist2)-costheta_siosi )**2

def compute_energy(BondsSiO, AngleOSiO, AngleSiOSi, Coord, Lat):
    # bonding energy Si-O, eV
    EBond = 0.0
    for iSi, iSibond in enumerate(BondsSiO):
        for jSi, jSibond in enumerate(iSibond):
            dist, distdir = distance( Coord[jSibond[0],:],Coord[jSibond[1],:], Lat )
            EBond = EBond + sio_bond_energy( dist )
    # angle energy O-Si-O, eV
    EAngleOSiO = 0.0
    for iSi, iSibond in enumerate(AngleOSiO):
        for jSi, jSibond in enumerate(iSibond):
            dist1, dir1 = distance( Coord[jSibond[0],:], Coord[jSibond[1],:], Lat )
            dist2, dir2 = distance( Coord[jSibond[0],:], Coord[jSibond[2],:], Lat )
            r21 = Coord[jSibond[1],:]+dir1[0]*Lat[0,:]+dir1[1]*Lat[1,:]+dir1[2]*Lat[2,:] - Coord[jSibond[0],:]
            r31 = Coord[jSibond[2],:]+dir2[0]*Lat[0,:]+dir2[1]*Lat[1,:]+dir2[2]*Lat[2,:] - Coord[jSibond[0],:]
            EAngleOSiO = EAngleOSiO + osio_angle_energy(r21,dist1,r31,dist2)
    # angle energy Si-O-Si, eV
    EAngleSiOSi = 0.0
    for iO, iObond in enumerate(AngleSiOSi):
        dist1, dir1 = distance( Coord[iObond[0],:], Coord[iObond[1],:], Lat )
        dist2, dir2 = distance( Coord[iObond[0],:], Coord[iObond[2],:], Lat )
        r21 = Coord[iObond[1],:]+dir1[0]*Lat[0,:]+dir1[1]*Lat[1,:]+dir1[2]*Lat[2,:] - Coord[iObond[0],:]
        r31 = Coord[iObond[2],:]+dir2[0]*Lat[0,:]+dir2[1]*Lat[1,:]+dir2[2]*Lat[2,:] - Coord[iObond[0],:]
        EAngleSiOSi = EAngleSiOSi + siosi_angle_energy(r21,dist1,r31,dist2)
    Etot = EBond + EAngleOSiO + EAngleSiOSi
    return Etot

def relax( BondsSiO, AngleOSiO, AngleSiOSi, Coord, Lat):
    pass


##
Name, Lat, Coord, Coord = _read_coord_single_file( sys.argv[1] )   # Coord needs to be cartesian
BondsSiSi, BondsSiO  = gen_initial_bonds( Name, Coord, Lat )
#BondsSiSi_Flat = sum( copy.deepcopy( BondsSiSi ),[])
#BondsSiSi_Flat_NoDup = remove_dup( [sorted(x) for x in BondsSiSi_Flat] )
#BondsSiO_Flat = sum( BondsSiO,[] )
BondsSiSiO = gen_sisio_bond( BondsSiSi, BondsSiO )
AngleSiOSi, AngleOSiO = gen_angles( BondsSiSiO, BondsSiO, Coord, Lat )

print 'Generating initial neighbor list'

# initial relaxation
#Coord_Old = relax(BondsSiO, AngleOSiO, AngleSiOSi, Coord, Lat)

energy = compute_energy( BondsSiO, AngleOSiO, AngleSiOSi, Coord, Lat)
print energy
print 'Relax initial coordinate'

sys.exit()
# entering mover
for it in range(0,MAXITER):
    Energy_Old = compute_energy(BondsSiO, AngleOSiO, AngleSiOSi, Coord_Old, Lat)
    BondsSiO_New, AngleOSiO_New, AngleSiOSi_New = bond_switching( BondsSiO, AngleOSiO, AngleSiOSi )
    Coord_New = relax(BondsSiO_New, AngleOSiO_New, AngleSiOSi_New, Coord_Old, Lat)
    Energy_New = compute_energy( BondsSiO_New, AngleOSiO_New, AngleSiOSi_New, Coord_New, Lat )
    if Energy_New < Energy_Old:
        Coord_Old = np.copy(Coord_New)
        Energy_Old = Energy_New
        BondsSiO = copy.deepcopy(BondsSiO_New); AngleOSiO = copy.deepcopy(AngleOSiO_New); AngleSiOSi = copy.deepcopy(AngleSiOSi_New)
    else:
        scalar = exp(-1e0*delta_E/(kb*temperature)) # eV unit for kb
        prob   = np.random.random_sample()
        if prob < scalar:
            Coord_Old = np.copy(Coord_New)
            Energy_Old = Energy_New
            BondsSiO = copy.deepcopy(BondsSiO_New); AngleOSiO = copy.deepcopy(AngleOSiO_New); AngleSiOSi = copy.deepcopy(AngleSiOSi_New)





##
##
