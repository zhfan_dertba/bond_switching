#!/usr/bin/python2

import sys
import numpy as np

# generate BONDS file from coordinates
#   input coord format should be in cartesian coord


TYPICAL_BONDLENGTH = {'SiSi':2.85,'SiO':2.5,'OSi':2.5,'OO':0.2} 
Type = {'Si':14, 'O':8}

def _read_coord_single_file( filename ):
    # use Angstrom unit
    with open(filename,'r') as f:
        raw = f.readlines()
    coord = []; name = []; lat = np.zeros( (3,3) ); imov_at = []
    for order,i in enumerate(raw[0:3]):
        tmp_ = [float(j.strip().strip('\n')) for j in i.split()]
        lat[order,:] = np.array( tmp_ )
    lat_const = float(raw[3].strip().strip('\n').split()[0])
    lat = lat * lat_const   # lat is Angstrom
    if len(raw) > 4:
        for i in raw[4:]:
            tmp_ = [j.strip().strip('\n') for j in i.split()]
            name.append(tmp_[0])
            tmp_2 = list(map(float, tmp_[1:]))
            coord.append( tmp_2[:3] )
            imov_at.append( map(int, tmp_2[3:]) )
    else:
        print ('No coordinates found, exit'); sys.exit()
    return name, lat, np.array( coord ), np.array( coord ), np.array(imov_at)  # name, lattice vector, coord_frac, coord_cart

def distance( coord1, coord2, Lat ):
    '''check x, y direction periodic boundary condition'''
    dist = []; distDirection = []
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            for z in [-1, 0, 1]:
                dist.append( np.linalg.norm( coord1 - (coord2+x*Lat[0,:]+y*Lat[1,:]+z*Lat[2,:]) ) )
                distDirection.append([x,y,z])
    SmallestDistIndex = np.argmin( np.array(dist) )
    return dist[SmallestDistIndex], distDirection[SmallestDistIndex]

def find_bondss( Coord, Name, Lat, imov_at ):
    Natom = len(Name)
    Bondss = []
    print Natom
    for iatom in range(0,Natom):
        Bond = []; Angle = []
        for jatom in range(0,Natom):
            dist, distdir = distance( Coord[iatom,:], Coord[jatom,:], Lat )
            if dist > 0.10 and dist <= TYPICAL_BONDLENGTH[Name[iatom]+Name[jatom]]:
                if Name[jatom]=='O' and Name[iatom]=='Si': Bond.append([11, jatom])
                if Name[jatom]=='Si' and Name[iatom]=='O': Bond.append([11, jatom])
                if Name[jatom]=='Si' and Name[iatom]=='Si': Bond.append([12, jatom])
        if Name[iatom] == 'Si':
            if len(Bond) != 4:
                print 'wrong bonding Si', iatom+1
            Angle = [ [0,Bond[x][1], Bond[y][1], Name[Bond[x][1]], Name[Bond[y][1]] ] for x in range(0,3) for y in range(x+1,4) ]
            print iatom+1, Type[Name[iatom]]
            for i in Bond:
                if np.any(imov_at[i[1],:]==0) or np.any(imov_at[iatom,:]==0): 
                    fix = 0
                else:
                    fix = 1
                print i[0],i[1]+1, fix
            for i in Angle:
                if np.any(imov_at[i[1],:]==0) or np.any(imov_at[i[2],:]==0) or np.any(imov_at[iatom,:]==0):
                    fix = 0
                else:
                    fix = 1
                if i[3]==i[4] and i[3]=='O':
                    print 21,  i[1]+1, i[2]+1, fix
                if i[3]==i[4] and i[3]=='Si':
                    print 24,  i[1]+1, i[2]+1, fix
                if i[3]=='O' and i[4]=='Si' or i[3]=='Si' and i[4]=='O':
                    print 23,  i[1]+1, i[2]+1, fix
        if Name[iatom] == 'O':
            if len(Bond) != 2:
                print 'wrong bonding O', iatom+1
            Angle = [ 22, Bond[0][1], Bond[1][1] ]
            print iatom+1, Type[Name[iatom]]
            for i in Bond:
                if np.any(imov_at[i[1],:]==0) or np.any(imov_at[iatom,:]==0):
                    fix = 0
                else:
                    fix = 1
                print i[0],i[1]+1, fix
            if np.any(imov_at[Angle[1],:]==0) or np.any(imov_at[Angle[2],:]==0) or np.any(imov_at[iatom,:]==0):
                fix = 0
            else:
                fix = 1
            print Angle[0], Angle[1]+1, Angle[2]+1, fix


Name, Lat, Coord, Coord, imov_at = _read_coord_single_file( sys.argv[1] )   # Coord needs to be cartesian

find_bondss( Coord, Name, Lat, imov_at )
