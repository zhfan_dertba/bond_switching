module sd
use omp_lib

    implicit none

    integer, parameter                 :: dp=kind(1d0)
    integer, parameter                 :: neighbor=4
    integer, parameter                 :: numneigh=50
    integer, parameter                 :: update_neighbor=1
    integer, parameter                 :: ATOM_TYPE1=14
    integer, parameter                 :: ATOM_TYPE2=8
    integer, parameter                 :: NLEVEL=4
    real*8,parameter                   :: eq_tol=1d-6

    real*8,parameter                   :: kb_sio=27.0d0                 ! ev/A^2
    real*8,parameter                   :: ktheta_siosi=0.75d0           ! eV
    real*8,parameter                   :: ktheta_osio=4.32d0            ! eV
    real*8,parameter                   :: bsio=1.64041d0                ! Angstrom 1.62041d0
    real*8,parameter                   :: costheta_siosi=-0.79119d0     ! 142 degree
    real*8,parameter                   :: costheta_osio=-1.0d0/3.0d0    ! 109.5 degree
    real*8,parameter                   :: kb_sisi=9.08d0                ! ev/A^2
    real*8,parameter                   :: bsisi=2.36d0                  ! Angstrom
    real*8,parameter                   :: ktheta_sisio=3.93d0           ! eV
    real*8,parameter                   :: ktheta_sisisi=3.58d0          ! eV
    real*8,parameter                   :: costheta_sisio=-1.0d0/3.0d0   ! 109.5 degree
    real*8,parameter                   :: costheta_sisisi=-1.0d0/3.0d0  ! 109.5 degree

    real*8,parameter                   :: kr=1.4d0                      ! ev/A^4
    real*8,parameter                   :: bondswitch_sisi_cutoff=9.5    ! Angstrom
    real*8,parameter,dimension(2)      :: neigh_cutoff=(/4.1,4.1/)      ! Angstrom, Si-O and O-Si
    ! kr, neigh_cutoff: PRB, 68, 073203 (2003)
    ! other parameters: PRL, 19, 4393 (2000)
    real*8,parameter,dimension(3)      :: neigh_cutoff2=(/2.58,3.20,3.84/)   ! Angstrom, O-O and O-Si and Si-Si

    real*8,parameter                   :: ang2bohr=1.889725989d0        ! Angstrom to Borh
    real*8,parameter                   :: hartree_ev=27.211396132       ! Hartree to eV
    real*8,parameter                   :: A_AU_1=0.529177249            ! Borh to Angstrom

    real*8,parameter                   :: maxdisplace=0.03d0            ! Angstrom

    type bonds
      integer                          :: ind   ! atom index
      integer                          :: typ   ! atom typ
      integer,dimension(4,4)           :: bond
      integer,dimension(4,6)           :: angle
      integer,dimension(4,12)          :: angle2
      integer,dimension(3,1,4)         :: bond_lat
      integer,dimension(3,2,6)         :: angle_lat
      integer,dimension(3,2,12)        :: angle2_lat
    end type

    type bonds_l
      integer,dimension(:,:),allocatable   :: bond1
      integer,dimension(:,:),allocatable   :: bond2
      integer,dimension(:,:),allocatable   :: angle1
      integer,dimension(:,:),allocatable   :: angle2
      integer,dimension(:,:),allocatable   :: angle3
      integer,dimension(:,:),allocatable   :: angle4
      integer,dimension(:,:,:),allocatable :: bond1_lat
      integer,dimension(:,:,:),allocatable :: bond2_lat
      integer,dimension(:,:,:),allocatable :: angle1_lat
      integer,dimension(:,:,:),allocatable :: angle2_lat
      integer,dimension(:,:,:),allocatable :: angle3_lat
      integer,dimension(:,:,:),allocatable :: angle4_lat
    end type

contains

    subroutine relax_sd(bondss,bonds_list,coord,lat,names,natom,epsilon0,energy_new, &
force_thred_ev,n_table,nn_table,maxit,iteration)
        type(bonds),dimension(:),intent(in)    :: bondss
        type(bonds_l),intent(in)               :: bonds_list
        real*8,dimension(:,:),intent(inout)    :: coord
        real*8,dimension(3,3),intent(in)       :: lat          ! input
        integer,dimension(:),intent(in)        :: names        ! input
        integer,intent(in)                     :: natom     
        real*8,intent(inout)                   :: epsilon0  
        real*8,intent(out)                     :: energy_new
        real*8,intent(in)                      :: force_thred_ev
        integer,dimension(:,:)                 :: n_table
        integer,dimension(:,:)                 :: nn_table
        integer,intent(in   )                  :: maxit
        integer,intent(inout)                  :: iteration 
        integer                                :: stat      

        real*8,dimension(3,size(coord,2))      :: coord_old1
        real*8,dimension(3,size(coord,2))      :: coord_old2
        real*8                                 :: maxforce,maxf1,maxf2
        real*8,dimension(natom)                :: force_l
        real*8,dimension(3,natom)              :: force
        real*8                                 :: epsilon
        real*8                                 :: energy


        integer,dimension(numneigh,natom)     :: neigh_table
        integer,dimension(3,numneigh,natom)   :: neigh_table_lat
    
        integer,dimension(1)                  :: maxforce_ind
        integer                               :: iter, j

        !
        call find_neighbors(coord,lat,names,n_table,nn_table,neigh_table,neigh_table_lat)

        energy=compute_energy(bonds_list,coord,names,lat,natom,neigh_table,neigh_table_lat)

        maxf1=1d0
    
        do iter=1, maxit

            if (mod(iter,update_neighbor)==0) then
                call find_neighbors(coord,lat,names,n_table, &
nn_table,neigh_table,neigh_table_lat)
            end if
     
            force=compute_force(bondss,coord,names,lat,natom,neigh_table,neigh_table_lat)
    
            forall(j=1:natom) force_l(j)=norm(force(:,j))
            maxforce=maxval(force_l)
            maxforce_ind=maxloc(force_l)
            maxf2=maxforce
            if (maxforce * epsilon0 >= maxdisplace) then
                epsilon = maxdisplace/maxforce
            else
                epsilon = epsilon0
            end if
   
            call mover(coord,force,epsilon)
    
            energy_new=compute_energy(bonds_list,coord,names,lat,natom,neigh_table,neigh_table_lat)
    
            if (iter<maxit .and. maxforce <= force_thred_ev ) exit
            maxf1=maxf2

        end do

        iteration=iter
        if (iter<maxit) then
            stat=0
        else
            stat=1
        end if

!       write(6,*) 'Tot Iter = ', iter, 'Energy = ', energy
    end subroutine


    subroutine mover( coord_old, force, epsilon)
        real*8,dimension(:,:),intent(inout)           :: coord_old
        real*8,dimension(:,:),intent(in)              :: force
        real*8                                        :: epsilon

        coord_old=coord_old+epsilon*force

    end subroutine

    function find_boundary(coord, lat, bond_neighbor_list)
        integer,dimension(:,:),intent(in)       :: bond_neighbor_list
        real*8,dimension(:,:),intent(in)      :: coord
        real*8,dimension(3,3),intent(in)      :: lat
        integer,dimension(3,size(bond_neighbor_list,1),size(bond_neighbor_list,2))  :: find_boundary
        integer                                 :: nbond, i, atom1_ind, atom2_ind
        real*8,dimension(3)                   :: coord1, coord2, coord3
        logical                                 :: angle

        nbond=size(bond_neighbor_list,2)
        if (size(bond_neighbor_list,1)==3) angle=.true.


        do i=1,nbond
            atom1_ind=bond_neighbor_list(1,i)
            atom2_ind=bond_neighbor_list(2,i)
            coord1=coord(:,atom1_ind)
            coord2=coord(:,atom2_ind)
            find_boundary(:,1,i)=(/0,0,0/)
            find_boundary(:,2,i)=lat_shift(coord1,coord2,lat)
            if (size(bond_neighbor_list,1)==3) then
                coord3=coord(:,bond_neighbor_list(3,i))
                find_boundary(:,3,i)=lat_shift(coord1,coord3,lat)
            end if
        end do

    end function

    function table_atom_index(bondsio,angleosio,anglesiosi,names)
        integer,dimension(:,:),intent(in)         :: bondsio
        integer,dimension(:,:),intent(in)         :: angleosio
        integer,dimension(:,:),intent(in)         :: anglesiosi
        integer,dimension(:),intent(in)           :: names
        integer,dimension(3,size(names))          :: table_atom_index

        integer                                   :: iatom, num_si, num_o
        integer                                   :: i,find_o,nangle,natom
        integer,dimension(size(names))            :: o_atom_list

        table_atom_index=-99

        num_si=0; num_o=0; nangle=size(anglesiosi,2); natom=size(names)

        forall(i=1:nangle) o_atom_list(i)=anglesiosi(1,i)

        ! first column is sio bond, then osio angle, then siosi angle
        do iatom=1,natom
            if (names(iatom)==1) then
                num_si=num_si+1
                table_atom_index(1,iatom)=num_si   ! bond sio is ordered with the order of si
                table_atom_index(2,iatom)=num_si   ! angle osio is ordered with the order of si
            end if
            if (names(iatom)==2) then
                do i=1,nangle
                    if (o_atom_list(i)==iatom) find_o=i
                end do
                table_atom_index(3,iatom)=find_o
            end if
        end do
    end function

    function lat_shift(coord1, coord2, lat)
        real*8,dimension(3),intent(in)       :: coord1, coord2
        real*8,dimension(3,3),intent(in)     :: lat
        integer,dimension(3)                   :: lat_shift

        real*8,dimension(27)     :: dist
        integer,dimension(3,27)     :: dist_lat
        integer                                :: x,y,z,iline
        integer,dimension(1)   :: lat_shift_index

        dist=0d0
        dist_lat=0
        iline=0
        do x=-1,1
            do y=-1,1
                do z=-1,1
           iline=iline+1
           dist(iline)=dist_norm(coord1,coord2+x*lat(1,:)+y*lat(2,:)+z*lat(3,:))
           dist_lat(1,iline)=x
           dist_lat(2,iline)=y
           dist_lat(3,iline)=z
            end do
         end do
        end do

        lat_shift_index=minloc(dist)
        lat_shift=dist_lat(:,lat_shift_index(1))

    end function

    ! coord2+latshift to coord1 distance
    function distance(coord1, coord2, lat, latshift)
        real*8,dimension(3),intent(in)       :: coord1, coord2
        real*8,dimension(3,3),intent(in)     :: lat
        integer,dimension(3)                   :: latshift
        real*8                               :: distance
        integer                                :: x,y,z

        x=latshift(1); y=latshift(2); z=latshift(3)
        distance=dist_norm(coord1,coord2+x*lat(1,:)+y*lat(2,:)+z*lat(3,:))

    end function
         
    pure function dist_norm(coord1,coord2)
        real*8,dimension(3),intent(in)       :: coord1, coord2
        real*8                               :: dist_norm

        dist_norm=sqrt(dot_product(coord1-coord2,coord1-coord2))

    end function

    pure function norm(coord)
        real*8,dimension(3),intent(in)       :: coord
        real*8                               :: norm

        norm=sqrt(dot_product(coord,coord))
    end function

    subroutine shift_to_unitcell(coord,lat,reclat)
        real*8,dimension(:,:)                  :: coord
        real*8,dimension(3,3)                  :: lat
        real*8,dimension(3,3)                  :: reclat

        real*8,dimension(size(coord,1),size(coord,2)) :: xatom
        integer                                :: i,j, natom

        natom=size(coord,2)
!       reclat=transpose(rinverse(lat))
        xatom=matmul(reclat,coord)
        forall(i=1:natom,j=1:3) xatom(j,i)=modulo(xatom(j,i),1d0)
        coord=matmul(transpose(lat),xatom)  ! coord with cartesian coord
    end subroutine

    function compute_force(bondss,coord,names,lat,natom,neigh_table,neigh_table_lat )
        type(bonds),dimension(:),intent(in)    :: bondss
        real*8,dimension(:,:),intent(in)       :: coord
        real*8,dimension(3,3),intent(in)       :: lat
        integer,dimension(:),intent(in)        :: names
        integer,intent(in)                     :: natom
        integer,dimension(:,:),intent(in)      :: neigh_table
        integer,dimension(:,:,:),intent(in)    :: neigh_table_lat
        real*8,dimension(3,natom)              :: compute_force
       
        integer                 :: iatom,r2atom_index,ibond,i,iangle,iangle2
        integer                 :: r3atom_index,io_angl, r1atom_index
        integer                 :: isi_angl,isi_bond, ineigh, jneigh,ii_angl
        integer,dimension(3)    :: latshift,latshift1,latshift2
        real*8,dimension(3)     :: force_atom
        real*8,dimension(3)     :: force_atom_bond1,force_atom_bond2
        real*8,dimension(3)     :: force_atom_angle1,force_atom2_angle1
        real*8,dimension(3)     :: force_atom_angle2,force_atom2_angle2
        real*8,dimension(3)     :: force_atom_angle3,force_atom2_angle3
        real*8,dimension(3)     :: force_atom_angle4,force_atom2_angle4
        real*8,dimension(3)     :: force_atom_repl
        real*8,dimension(3)     :: r1, r2, r3, r21, r31, r1_tmp
        real*8                  :: dist, dist1, dist2, neigh_cut

        integer                  :: chunk, nthread, OMP_GET_NUM_THREADS

        compute_force=0d0

!$OMP PARALLEL SHARED(bondss,names,compute_force,lat,coord,neigh_table,neigh_table_lat)  &
!$OMP PRIVATE(i,force_atom,force_atom_bond1,force_atom_bond2,force_atom_angle1,force_atom_angle2, &
!$OMP force_atom_angle3,force_atom_angle4,force_atom2_angle1,force_atom2_angle2,force_atom2_angle3, &
!$OMP force_atom2_angle4,force_atom_repl,iatom,ibond,iangle,iangle2,r1,r2,r3,r21,r31,dist,dist1,neigh_cut, &
!$OMP dist2,r1atom_index,r2atom_index,r3atom_index,latshift,latshift1,latshift2,r1_tmp,ineigh,jneigh)
        nthread = OMP_GET_NUM_THREADS()
        chunk = ceiling( size(bondss)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
        do i=1,size(bondss)
            force_atom=0d0
            force_atom_bond1=0d0
            force_atom_bond2=0d0
            force_atom_angle1=0d0
            force_atom_angle2=0d0
            force_atom_angle3=0d0
            force_atom_angle4=0d0
            force_atom2_angle1=0d0
            force_atom2_angle2=0d0
            force_atom2_angle3=0d0
            force_atom2_angle4=0d0
            force_atom_repl=0d0
            iatom=bondss(i)%ind   ! iatom: atom index
            ! Si atom
            if (bondss(i)%typ==ATOM_TYPE1) then
                do ibond=1,4
                    r1=coord(:,iatom)
                    r2atom_index=bondss(i)%bond(2,ibond)
                    r2=coord(:,r2atom_index)
                    latshift=bondss(i)%bond_lat(:,1,ibond)
                    dist=distance(r1,r2,lat,latshift)
                    r21=r2+latshift(1)*lat(1,:)+latshift(2)*lat(2,:)+ &
latshift(3)*lat(3,:) - r1
                    if (bondss(i)%bond(1,ibond)==11) then   ! Si-O
                    force_atom_bond1 = force_atom_bond1 +  &
kb_sio*(r21-bsio*(r21)/dist)
                    end if
                    if (bondss(i)%bond(1,ibond)==12) then   ! Si-Si
                    force_atom_bond2 = force_atom_bond2 +  &
kb_sisi*(r21-bsisi*(r21)/dist)
                    end if
                end do
                do iangle=1,6
                    r1=coord(:,iatom)
                    r2atom_index=bondss(i)%angle(2,iangle)
                    r3atom_index=bondss(i)%angle(3,iangle)
                    r2=coord(:,r2atom_index)
                    r3=coord(:,r3atom_index)
                    latshift1=bondss(i)%angle_lat(:,1,iangle)
                    latshift2=bondss(i)%angle_lat(:,2,iangle)
                    dist1=distance(r1,r2,lat,latshift1)
                    dist2=distance(r1,r3,lat,latshift2)
                    r21=r2+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+ &
latshift1(3)*lat(3,:) - r1
                    r31=r3+latshift2(1)*lat(1,:)+latshift2(2)*lat(2,:)+ &
latshift2(3)*lat(3,:) - r1
                    if (bondss(i)%angle(1,iangle)==21) then  ! O-Si-O
                    force_atom_angle1 = force_atom_angle1 + ktheta_osio*(   &
dot_product(r21,r31)/(dist1*dist2) - costheta_osio)/(dist1*dist2)*          &
(r31 + r21 - dot_product(r21,r31)*(r21/dist1**2+r31/dist2**2))
                    end if
                    if (bondss(i)%angle(1,iangle)==23) then  ! Si-Si-O
                    force_atom_angle3 = force_atom_angle3 + ktheta_sisio*(  &
dot_product(r21,r31)/(dist1*dist2) - costheta_sisio)/(dist1*dist2)*         &
(r31 + r21 - dot_product(r21,r31)*(r21/dist1**2+r31/dist2**2))
                    end if
                    if (bondss(i)%angle(1,iangle)==24) then  ! Si-Si-Si
                    force_atom_angle4 = force_atom_angle4 + ktheta_sisisi*( &
dot_product(r21,r31)/(dist1*dist2) - costheta_sisisi)/(dist1*dist2)*        &
(r31 + r21 - dot_product(r21,r31)*(r21/dist1**2+r31/dist2**2))
                    end if
                end do
                do iangle2=1,12
                    if (bondss(i)%angle2(1,iangle2)==-99) exit
                    r2=coord(:,iatom)
                    r1atom_index=bondss(i)%angle2(2,iangle2)
                    r3atom_index=bondss(i)%angle2(3,iangle2)
                    r1=coord(:,r1atom_index)
                    r3=coord(:,r3atom_index)
                    latshift1=bondss(i)%angle2_lat(:,1,iangle2)
                    latshift2=bondss(i)%angle2_lat(:,2,iangle2)
                    dist1=distance(r2,r1,lat,latshift1)
                    r1_tmp=r1+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+ &
latshift1(3)*lat(3,:)
                    dist2=distance(r1_tmp,r3,lat,latshift2)
                    r21=r2-(r1+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+ &
latshift1(3)*lat(3,:))
                    r31=r3+latshift2(1)*lat(1,:)+latshift2(2)*lat(2,:)+ &
latshift2(3)*lat(3,:) - (r1+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+ &
latshift1(3)*lat(3,:))
                    if (bondss(i)%angle2(1,iangle2)==22) then  ! Si-O-Si
                    force_atom2_angle2 = force_atom2_angle2 - ktheta_siosi*(   &
dot_product(r21,r31)/(dist1*dist2)-costheta_siosi)*(r31/(dist1*dist2)-     &
dot_product(r21,r31)/(dist2*dist1**3)*r21)
                    end if
                    if (bondss(i)%angle2(1,iangle2)==23) then  ! Si-Si-O
                    force_atom2_angle3 = force_atom2_angle3 - ktheta_sisio*(   &
dot_product(r21,r31)/(dist1*dist2)-costheta_sisio)*(r31/(dist1*dist2)-     &
dot_product(r21,r31)/(dist2*dist1**3)*r21)
                    end if
                    if (bondss(i)%angle2(1,iangle2)==24) then  ! Si-Si-Si
                    force_atom2_angle4 = force_atom2_angle4 - ktheta_sisisi*(   &
dot_product(r21,r31)/(dist1*dist2)-costheta_sisisi)*(r31/(dist1*dist2)-     &
dot_product(r21,r31)/(dist2*dist1**3)*r21)
                    end if
                end do
            end if
            if (bondss(i)%typ==ATOM_TYPE2) then
                r1atom_index=iatom
                do ibond=1,2
                    r1=coord(:,iatom)
                    r2atom_index=bondss(i)%bond(2,ibond)
                    r2=coord(:,r2atom_index)
                    latshift1=bondss(i)%bond_lat(:,1,ibond)
                    dist1=distance(r1,r2,lat,latshift1)
                    r21=r2+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+&
latshift1(3)*lat(3,:) - r1
                    force_atom_bond1 = force_atom_bond1 + kb_sio*(r21 -  &
bsio*(r21)/dist1)
                end do
                r1=coord(:,iatom)
                r2atom_index=bondss(i)%angle(2,1)
                r3atom_index=bondss(i)%angle(3,1)
                r2=coord(:,r2atom_index)
                r3=coord(:,r3atom_index)
                latshift1=bondss(i)%angle_lat(:,1,1)
                latshift2=bondss(i)%angle_lat(:,2,1)
                dist1=distance(r1,r2,lat,latshift1)
                dist2=distance(r1,r3,lat,latshift2)
                r21=r2+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+&
latshift1(3)*lat(3,:) - r1
                r31=r3+latshift2(1)*lat(1,:)+latshift2(2)*lat(2,:)+&
latshift2(3)*lat(3,:) - r1
                force_atom_angle2 = force_atom_angle2 + ktheta_siosi*(  &
dot_product(r21,r31)/(dist1*dist2) - costheta_siosi)/(dist1*dist2)*   &
(r31 + r21 - dot_product(r21,r31)*(r21/dist1**2+r31/dist2**2))
                r2=coord(:,iatom)
                do iangle2=1,12
                    if (bondss(i)%angle2(1,iangle2)==-99) exit
                    r1atom_index=bondss(i)%angle2(2,iangle2)
                    r3atom_index=bondss(i)%angle2(3,iangle2)
                    r1=coord(:,r1atom_index)
                    r3=coord(:,r3atom_index)
                    latshift1=bondss(i)%angle2_lat(:,1,iangle2)
                    latshift2=bondss(i)%angle2_lat(:,2,iangle2)
                    dist1=distance(r2,r1,lat,latshift1)
                    r1_tmp=r1+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+ &
latshift1(3)*lat(3,:)
                    dist2=distance(r1_tmp,r3,lat,latshift2)
                    r21=r2 - (r1+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+&
latshift1(3)*lat(3,:))
                    r31=(r3+latshift2(1)*lat(1,:)+latshift2(2)*lat(2,:)+&
latshift2(3)*lat(3,:)) - (r1+latshift1(1)*lat(1,:)+latshift1(2)*lat(2,:)+ &
latshift1(3)*lat(3,:))
                    if (bondss(i)%angle2(1,iangle2)==21) then  ! O-Si-O
                    force_atom2_angle1=force_atom2_angle1-ktheta_osio*(   &
dot_product(r21,r31)/(dist1*dist2)-costheta_osio)*(r31/(dist1*dist2)-     &
dot_product(r21,r31)/(dist2*dist1**3)*r21)
                    end if
                    if (bondss(i)%angle2(1,iangle2)==23) then  ! Si-Si-O
                    force_atom2_angle3=force_atom2_angle3-ktheta_sisio*(   &
dot_product(r21,r31)/(dist1*dist2)-costheta_sisio)*(r31/(dist1*dist2)-     &
dot_product(r21,r31)/(dist2*dist1**3)*r21)
                    end if
                end do
            end if

            do ineigh=1,numneigh
                r1=coord(:,iatom)
                if (neigh_table(ineigh,iatom)/=-99) then
                    r2atom_index=neigh_table(ineigh,iatom)
                    r2=coord(:,r2atom_index)
                    latshift=neigh_table_lat(:,ineigh,iatom)
                    dist=distance(r1,r2,lat,latshift)
!                   neigh_cut=neigh_cutoff2(1)
                    if (names(iatom)==ATOM_TYPE1 .and. names(r2atom_index)==ATOM_TYPE1) then
                        neigh_cut=neigh_cutoff2(3)
                    else if (names(iatom)==ATOM_TYPE1 .and. names(r2atom_index)==ATOM_TYPE2 .or. &
                             names(iatom)==ATOM_TYPE2 .and. names(r2atom_index)==ATOM_TYPE1) then
                        neigh_cut=neigh_cutoff2(2)
                    else if (names(iatom)==ATOM_TYPE2 .and. names(r2atom_index)==ATOM_TYPE2) then
                        neigh_cut=neigh_cutoff2(1)
                    end if
                    if (dist<=neigh_cut) then
                        r21=r2+latshift(1)*lat(1,:)+latshift(2)*lat(2,:)+ &
latshift(3)*lat(3,:) - r1
                        force_atom_repl=force_atom_repl+   &
  2d0*kr*(dist-neigh_cut)**3*r21/dist
                    end if
                end if
            end do
            force_atom=force_atom_bond1+force_atom_bond2+ &
force_atom_angle1+force_atom_angle2+force_atom_angle3+force_atom_angle4+ &
force_atom2_angle1+force_atom2_angle2+force_atom2_angle3+force_atom2_angle4+ &
force_atom_repl
            compute_force(:,iatom)=force_atom
        end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

    

    end function


    function compute_energy(bonds_list,coord,names,lat,natom,neigh_table,neigh_table_lat,f)
        type(bonds_l)             ,intent(in)           :: bonds_list
        real*8,dimension(:,:),intent(in)                :: coord
        integer,dimension(:),intent(in)                 :: names
        real*8,dimension(3,3),intent(in)                :: lat
        integer,dimension(:,:),intent(in)               :: neigh_table
        integer,dimension(:,:,:),intent(in)             :: neigh_table_lat
        real*8                                          :: compute_energy
        integer,intent(in)                              :: natom
        integer,optional                                :: f
       
        integer                :: iatom, r2atom_index,ibondatom,i, iangle, ibond
        integer                :: r3atom_index,io_angl, i_angl
        integer                :: isi_angl,isi_bond,r1atom_index
        integer                :: io_angle, ineigh, jneigh
        integer,dimension(3)   :: latshift, latshift1, latshift2
        real*8,dimension(3)    :: r1, r2, r3, r21, r31
        real*8                 :: dist, dist1, dist2
        real*8                 :: ebond1,ebond2,eangle1,eangle2,eangle3,eangle4,erepulsion
        logical                :: initialize

        real*8                 :: ebond_max, dist_max, neigh_cut
        real*8,dimension(3)    :: r1max,r2max
        integer                :: ibond_max, r1atom_max, r2atom_max
        integer,dimension(3)   :: latshift_max

        integer                  :: chunk, nthread

        compute_energy=0d0
        ebond1=0d0; ebond2=0d0; eangle1=0d0; eangle2=0d0; eangle3=0d0; eangle4=0d0
        erepulsion=0d0
        ebond_max=0d0

        nthread = OMP_GET_NUM_THREADS()

!$OMP PARALLEL SHARED(bonds_list,lat,coord,natom,neigh_table,neigh_table_lat) &
!$OMP PRIVATE(ibond,r1atom_index,r2atom_index,r1,r2,latshift,dist) &
!$OMP REDUCTION(+:ebond1)
            chunk = ceiling( size(bonds_list%bond1,2)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
            do ibond=1,size(bonds_list%bond1,2)   ! Si-O bond
                if (bonds_list%bond1(1,ibond)/=-99) then
                r1atom_index=bonds_list%bond1(1,ibond); r2atom_index=bonds_list%bond1(2,ibond)
                r1=coord(:,r1atom_index)
                r2=coord(:,r2atom_index)
                latshift=bonds_list%bond1_lat(:,1,ibond)
                dist=distance(r1,r2,lat,latshift)
                ebond1=ebond1+0.5d0*kb_sio*(dist-bsio)**2
                end if
            end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

!$OMP PARALLEL SHARED(bonds_list,lat,coord,natom,neigh_table,neigh_table_lat) &
!$OMP PRIVATE(ibond,r1atom_index,r2atom_index,r1,r2,latshift,dist) &
!$OMP REDUCTION(+:ebond2)
            chunk = ceiling( size(bonds_list%bond2,2)/float(nthread) )
            !$OMP DO SCHEDULE(DYNAMIC,CHUNK)
            do ibond=1,size(bonds_list%bond2,2)   ! Si-Si bond
                if (bonds_list%bond2(1,ibond)/=-99) then
                r1atom_index=bonds_list%bond2(1,ibond); r2atom_index=bonds_list%bond2(2,ibond)
                r1=coord(:,r1atom_index)
                r2=coord(:,r2atom_index)
                latshift=bonds_list%bond2_lat(:,1,ibond)
                dist=distance(r1,r2,lat,latshift)
                ebond2=ebond2+0.5d0*kb_sisi*(dist-bsisi)**2
!               write(400,*) r1atom_index, r2atom_index, dist,bsisi, 0.5d0*kb_sisi*(dist-bsisi)**2
                end if
            end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

!$OMP PARALLEL SHARED(bonds_list,lat,coord,natom,neigh_table,neigh_table_lat) &
!$OMP PRIVATE(iangle,r1atom_index,r2atom_index,r3atom_index,r1,r2,r3,latshift1,latshift2,dist1,dist2, &
!$OMP          r21,r31) &
!$OMP REDUCTION(+:eangle1)
            chunk = ceiling( size(bonds_list%angle1,2)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
            do iangle=1,size(bonds_list%angle1,2) ! O-Si-O angle
                if (bonds_list%angle1(1,iangle)/=-99) then
                r1atom_index=bonds_list%angle1(1,iangle)
                r2atom_index=bonds_list%angle1(2,iangle)
                r3atom_index=bonds_list%angle1(3,iangle)
                r1=coord(:,r1atom_index)
                r2=coord(:,r2atom_index)
                r3=coord(:,r3atom_index)
                latshift1=bonds_list%angle1_lat(:,1,iangle)
                latshift2=bonds_list%angle1_lat(:,2,iangle)
                dist1=distance(r1,r2,lat,latshift1)
                dist2=distance(r1,r3,lat,latshift2)
                r21=r2+latshift1(1)*lat(1,:)+  &
latshift1(2)*lat(2,:)+latshift1(3)*lat(3,:) - r1
                r31=r3+latshift2(1)*lat(1,:)+  &
latshift2(2)*lat(2,:)+latshift2(3)*lat(3,:) - r1
                eangle1=eangle1+0.5d0*ktheta_osio*  &
(dot_product(r21,r31)/(dist1*dist2)-costheta_osio)**2
                end if
            end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

!$OMP PARALLEL SHARED(bonds_list,lat,coord,natom,neigh_table,neigh_table_lat) &
!$OMP PRIVATE(iangle,r1atom_index,r2atom_index,r3atom_index,r1,r2,r3,latshift1,latshift2,dist1, &
!$OMP         dist2,r21,r31) &
!$OMP REDUCTION(+:eangle2)
            chunk = ceiling( size(bonds_list%angle2,2)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
            do iangle=1,size(bonds_list%angle2,2) ! Si-O-Si angle
                if (bonds_list%angle2(1,iangle)/=-99) then
                r1atom_index=bonds_list%angle2(1,iangle)
                r2atom_index=bonds_list%angle2(2,iangle)
                r3atom_index=bonds_list%angle2(3,iangle)
                r1=coord(:,r1atom_index)
                r2=coord(:,r2atom_index)
                r3=coord(:,r3atom_index)
                latshift1=bonds_list%angle2_lat(:,1,iangle)
                latshift2=bonds_list%angle2_lat(:,2,iangle)
                dist1=distance(r1,r2,lat,latshift1)
                dist2=distance(r1,r3,lat,latshift2)
                r21=r2+latshift1(1)*lat(1,:)+ &
latshift1(2)*lat(2,:)+latshift1(3)*lat(3,:) - r1
                r31=r3+latshift2(1)*lat(1,:)+ &
latshift2(2)*lat(2,:)+latshift2(3)*lat(3,:) - r1
                eangle2=eangle2+0.5d0*ktheta_siosi*  &
(dot_product(r21,r31)/(dist1*dist2)-costheta_siosi)**2
                end if
            end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

!$OMP PARALLEL SHARED(bonds_list,lat,coord,natom,neigh_table,neigh_table_lat) &
!$OMP PRIVATE(iangle,r1atom_index,r2atom_index,r3atom_index,r1,r2,r3,latshift1,latshift2,dist1, &
!$OMP         dist2,r21,r31)  &
!$OMP REDUCTION(+:eangle3)
            chunk = ceiling( size(bonds_list%angle3,2)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
            do iangle=1,size(bonds_list%angle3,2) ! Si-Si-O angle
                if (bonds_list%angle3(1,iangle)/=-99) then
                r1atom_index=bonds_list%angle3(1,iangle)
                r2atom_index=bonds_list%angle3(2,iangle)
                r3atom_index=bonds_list%angle3(3,iangle)
                r1=coord(:,r1atom_index)
                r2=coord(:,r2atom_index)
                r3=coord(:,r3atom_index)
                latshift1=bonds_list%angle3_lat(:,1,iangle)
                latshift2=bonds_list%angle3_lat(:,2,iangle)
                dist1=distance(r1,r2,lat,latshift1)
                dist2=distance(r1,r3,lat,latshift2)
                r21=r2+latshift1(1)*lat(1,:)+ &
latshift1(2)*lat(2,:)+latshift1(3)*lat(3,:) - r1
                r31=r3+latshift2(1)*lat(1,:)+ &
latshift2(2)*lat(2,:)+latshift2(3)*lat(3,:) - r1
                eangle3=eangle3+0.5d0*ktheta_sisio*  &
(dot_product(r21,r31)/(dist1*dist2)-costheta_sisio)**2
                end if
            end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

!$OMP PARALLEL SHARED(bonds_list,lat,coord,natom,neigh_table,neigh_table_lat) &
!$OMP PRIVATE(iangle,r1atom_index,r2atom_index,r3atom_index,r1,r2,r3,latshift1,latshift2,dist1,&
!$OMP         dist2,r21,r31)  &
!$OMP REDUCTION(+:eangle4)
            chunk = ceiling( size(bonds_list%angle4,2)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
            do iangle=1,size(bonds_list%angle4,2) ! Si-Si-Si angle
                if (bonds_list%angle4(1,iangle)/=-99) then
                r1atom_index=bonds_list%angle4(1,iangle)
                r2atom_index=bonds_list%angle4(2,iangle)
                r3atom_index=bonds_list%angle4(3,iangle)
                r1=coord(:,r1atom_index)
                r2=coord(:,r2atom_index)
                r3=coord(:,r3atom_index)
                latshift1=bonds_list%angle4_lat(:,1,iangle)
                latshift2=bonds_list%angle4_lat(:,2,iangle)
                dist1=distance(r1,r2,lat,latshift1)
                dist2=distance(r1,r3,lat,latshift2)
                r21=r2+latshift1(1)*lat(1,:)+ &
latshift1(2)*lat(2,:)+latshift1(3)*lat(3,:) - r1
                r31=r3+latshift2(1)*lat(1,:)+ &
latshift2(2)*lat(2,:)+latshift2(3)*lat(3,:) - r1
                eangle4=eangle4+0.5d0*ktheta_sisisi*  &
(dot_product(r21,r31)/(dist1*dist2)-costheta_sisisi)**2
                end if
            end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

!$OMP PARALLEL SHARED(lat,names,coord,natom,neigh_table,neigh_table_lat) &
!$OMP PRIVATE(ineigh,jneigh,r2atom_index,r1,r2,latshift,dist,neigh_cut) &
!$OMP REDUCTION(+:erepulsion)
        chunk = ceiling( natom/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
        do ineigh=1,natom
            r1=coord(:,ineigh)
            do jneigh=1,numneigh
                if (neigh_table(jneigh,ineigh)/=-99) then
                    r2atom_index=neigh_table(jneigh,ineigh)
                    r2=coord(:,r2atom_index)
                    latshift=neigh_table_lat(:,jneigh,ineigh)
                    dist=distance(r1,r2,lat,latshift)
!                   neigh_cut=neigh_cutoff2(3)
                    if (names(ineigh)==ATOM_TYPE1 .and. names(r2atom_index)==ATOM_TYPE1) then
                        neigh_cut=neigh_cutoff2(3)
                    else if (names(ineigh)==ATOM_TYPE1 .and. names(r2atom_index)==ATOM_TYPE2 .or. &
                             names(ineigh)==ATOM_TYPE2 .and. names(r2atom_index)==ATOM_TYPE1) then
                        neigh_cut=neigh_cutoff2(2)
                    else if (names(ineigh)==ATOM_TYPE2 .and. names(r2atom_index)==ATOM_TYPE2) then
                        neigh_cut=neigh_cutoff2(1)
                    end if
                    if (dist<=neigh_cut) then
                        erepulsion=erepulsion+    &
   0.5d0*kr*(dist-neigh_cut)**4
!                       write(96,'(A,2I7,2F12.5)') 'Neigh Energy ', ineigh, r2atom_index, dist, 0.5d0*kr*(dist-neigh_cut)**4
                    end if
                end if
            end do
        end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

        compute_energy=ebond1+ebond2+eangle1+eangle2+eangle3+eangle4+erepulsion
!       compute_energy=(/ebond,eanglesi,eangleo,erepulsion/)
!       write(96,'(A,8F12.5)') '***', compute_energy, ebond1,ebond2,eangle1,eangle2,eangle3,eangle4,erepulsion

    end function


    function decide_accept_proc(accept_procs,energy_procs,nproc)
         logical,dimension(nproc)      :: accept_procs
         real*8 ,dimension(nproc)      :: energy_procs
         integer                       :: nproc
         integer                       :: decide_accept_proc

         integer                       :: ic, accept_proc_counts, accept_proc_counts2
         integer                       :: accept_proc_id_order, energy_min_proc
         real*8                        :: energy_min

         accept_proc_counts=0
!        energy_min=9.0d6
         do ic=1,nproc
             if (accept_procs(ic) .eqv. .true.)  then
                 accept_proc_counts=accept_proc_counts+1
!                if (energy_procs(ic)<energy_min) then
!                    energy_min_proc=ic
!                    energy_min=energy_procs(ic)
!                end if
             end if
         end do
         if (accept_proc_counts>1) then
             accept_proc_id_order=get_rand_int(1,accept_proc_counts)
             accept_proc_counts2=0
             do ic=1,nproc
                 if (accept_procs(ic).eqv. .true.)  accept_proc_counts2=accept_proc_counts2+1
                 if (accept_procs(ic).eqv. .true.) then
                     if (accept_proc_counts2==accept_proc_id_order) then
                         decide_accept_proc=ic-1
                         exit
                     end if
                 end if
             end do
!            decide_accept_proc=energy_min_proc-1
         else if (accept_proc_counts==1) then
             do ic=1,nproc
                 if (accept_procs(ic).eqv. .true.)  decide_accept_proc=ic-1
             end do
         else
             decide_accept_proc=-1
         end if

    end function



    !!does an inverse based on lup decomposition
    pure function rinverse(m)
        real*8, dimension(:,:),                                   intent(in)      :: m

        real*8, dimension(size(m,1),size(m,2))                                    :: rinverse



        real*8, dimension(size(m,1),3*size(m,2))                                  :: lup
        integer                                                                     :: n


        n=size(m,1)

        lup=rlup(m)

        !!m^-1=P^T,U^-1,L^-1
        rinverse=matmul(transpose(lup(:,2*n+1:3*n)),matmul(transpose(rlinverse(transpose(lup(:,n+1:2*n)))),rlinverse(lup(:,:n))))


    end function

    !! inverts a lower triangular matrix via back substitution
    pure function rlinverse(l)
        real*8, dimension(:,:),                                   intent(in)      :: l

        real*8, dimension(size(l,1),size(l,2))                                    :: rlinverse


        integer                                                                     :: i,j,n

        n=size(l,1)
        rlinverse=0.0

        do i=n,1,-1
            rlinverse(i,i)=1/l(i,i)
            forall (j=i+1:n) rlinverse(j,i)=-dot_product(rlinverse(j,i+1:j),l(i+1:j,i))/l(i,i)
        enddo

    end function

    pure function rlup(a)
        real*8, dimension(:,:),                                   intent(in)      :: a

        real*8, dimension(size(a,1),3*size(a,2))                                  :: rlup


        real*8, dimension(size(a,1))                                              :: swap
        integer                                                                     :: i,j,n

        n=size(a,1)

        rlup(:,n+1:2*n)=a
        rlup(:,:n)=ridentity(n)
        rlup(:,2*n+1:)=ridentity(n)

        do i=1, n
            if (abs(rlup(i,i+n)) <= eq_tol) then
                j=1
                do while(abs(rlup(i,i+n+j)) <= eq_tol)
                    j=j+1
                enddo
                swap=rlup(:,i+n)
                rlup(:,i+n)=rlup(:,i+n+j)
                rlup(:,i+n+j)=swap

                swap=rlup(:,i+2*n)
                rlup(:,i+2*n)=rlup(:,i+2*n+j)
                rlup(:,i+2*n+j)=swap
            endif

            rlup(i+1:,i)=-rlup(i+1:,i+n)/rlup(i,i+n)
            rlup(i:,i+n:2*n)=matmul(rlup(i:,i:n),rlup(i:,i+n:2*n))
        enddo

        rlup(:,:n)=-rlup(:,:n)+2*ridentity(n)
    end function

    pure function ridentity(n)
        integer,                                                intent(in)          :: n

        real*8, dimension(n,n)                                                    :: ridentity


        integer                                                                     :: i

        ridentity=0d0
        forall (i=1:n) ridentity(i,i)=1d0
    end function

    function find_bond_repeat( list, inquiry, ind )
        integer,dimension(:,:)                :: list
        integer,dimension(:)                  :: inquiry
        integer                               :: ind
        integer                               :: find_bond_repeat  ! 0: no found; 1: found

        integer                               :: i, nsize

        nsize=size(inquiry)
        find_bond_repeat=0 
        if (ind>0) then
            do i=1,ind
                if (list(1,i)==inquiry(1) .and. list(2,i)==inquiry(2) &
                 .or. list(1,i)==inquiry(2) .and. list(2,i)==inquiry(1) ) then
                    find_bond_repeat=1
                    exit
                end if
            end do
        else
            find_bond_repeat=0
        end if

    end function

    subroutine bonds_list_init(names,bondss,bonds_list)
        type(bonds),dimension(:),intent(in)   :: bondss
        type(bonds_l)                         :: bonds_list
        integer,dimension(:)                  :: names


        if (allocated(bonds_list%bond1)) then
            call gen_bonds_list(names,bondss,bonds_list,.false.)
        else
            call gen_bonds_list(names,bondss,bonds_list,.true.)
        end if

    end subroutine


    subroutine gen_bonds_list(names,bondss,bonds_list,initialize)
        type(bonds),dimension(:)              :: bondss
        type(bonds_l)                         :: bonds_list
        integer,dimension(:)                  :: names
        logical                               :: initialize

        type(bonds_l)                         :: bonds_list_tmp

        integer                               :: natom, i, j, a1, a2, a3
        integer                               :: a1_ind,a2_ind,a3_ind,a4_ind,b1_ind,b2_ind
        
        natom=size(bondss)

        if (initialize) then
          allocate(bonds_list%bond1(2,natom*4), bonds_list%bond1_lat(3,1,natom*4))
          allocate(bonds_list%bond2(2,natom*4), bonds_list%bond2_lat(3,1,natom*4))
          allocate(bonds_list%angle1(3,natom*6),bonds_list%angle1_lat(3,2,natom*6))
          allocate(bonds_list%angle2(3,natom*6),bonds_list%angle2_lat(3,2,natom*6))
          allocate(bonds_list%angle3(3,natom*6),bonds_list%angle3_lat(3,2,natom*6))
          allocate(bonds_list%angle4(3,natom*6),bonds_list%angle4_lat(3,2,natom*6))
        end if
  
          bonds_list%bond1=-99;      bonds_list%bond1_lat=-99
          bonds_list%bond2=-99;      bonds_list%bond2_lat=-99
          bonds_list%angle1=-99;     bonds_list%angle2=-99
          bonds_list%angle3=-99;     bonds_list%angle4=-99
          bonds_list%angle1_lat=-99; bonds_list%angle2_lat=-99
          bonds_list%angle3_lat=-99; bonds_list%angle4_lat=-99
  
          b1_ind=0; b2_ind=0; a1_ind=0; a2_ind=0; a3_ind=0; a4_ind=0
  
          call bonds_list_update(bondss,bonds_list,names,b1_ind,b2_ind,a1_ind,a2_ind,a3_ind,a4_ind)

    end subroutine


    subroutine bonds_list_update(bondss,bonds_list,names,b1_ind,b2_ind,a1_ind,a2_ind,a3_ind,a4_ind)
        type(bonds),dimension(:),intent(in)         :: bondss
        type(bonds_l)                               :: bonds_list
        integer,dimension(:)                        :: names
        integer,intent(inout)                       :: b1_ind,b2_ind,a1_ind,a2_ind,a3_ind,a4_ind

        integer                                     :: i, natom, j, a1, a2, a3, find

        natom=size(bondss)

        do i=1,natom
            do j=1,6
                if (bondss(i)%angle(1,j) == -99) exit
                a1=bondss(i)%ind; a2=bondss(i)%angle(2,j); a3=bondss(i)%angle(3,j)
                if (bondss(i)%angle(1,j)==21) then
                    a1_ind=a1_ind+1 ! O-Si-O
                    bonds_list%angle1(:,a1_ind)=(/ a1, a2, a3 /)
                    bonds_list%angle1_lat(:,:,a1_ind)=bondss(i)%angle_lat(:,:,j)
                end if
                if (bondss(i)%angle(1,j)==22) then
                    a2_ind=a2_ind+1 ! Si-O-Si
                    bonds_list%angle2(:,a2_ind)=(/ a1, a2, a3 /)
                    bonds_list%angle2_lat(:,:,a2_ind)=bondss(i)%angle_lat(:,:,j)
                end if
                if (bondss(i)%angle(1,j)==23) then
                    a3_ind=a3_ind+1 ! Si-Si-O
                    bonds_list%angle3(:,a3_ind)=(/ a1, a2, a3 /)
                    bonds_list%angle3_lat(:,:,a3_ind)=bondss(i)%angle_lat(:,:,j)
                end if
                if (bondss(i)%angle(1,j)==24) then
                    a4_ind=a4_ind+1 ! Si-Si-Si
                    bonds_list%angle4(:,a4_ind)=(/ a1, a2, a3 /)
                    bonds_list%angle4_lat(:,:,a4_ind)=bondss(i)%angle_lat(:,:,j)
                end if
            end do
        end do

        do i=1,natom
            do j=1,4
                if (bondss(i)%bond(1,j) == -99) exit
                a1=bondss(i)%ind; a2=bondss(i)%bond(2,j)
                if (bondss(i)%bond(1,j)==11) then   ! Si-O bond
                    find=find_bond_repeat(bonds_list%bond1, (/a1, a2/), b1_ind )
                    if (find==0) then
                        b1_ind=b1_ind+1
                        bonds_list%bond1(:,b1_ind)=(/ a1, a2 /)
                        bonds_list%bond1_lat(:,1,b1_ind)=bondss(i)%bond_lat(:,1,j)
                    end if
                end if
                if (bondss(i)%bond(1,j)==12) then    ! Si-Si bond
                    find=find_bond_repeat(bonds_list%bond2, (/a1, a2/), b2_ind )
                    if (find==0) then
                        b2_ind=b2_ind+1
                        bonds_list%bond2(:,b2_ind)=(/ a1, a2 /)
                        bonds_list%bond2_lat(:,1,b2_ind)=bondss(i)%bond_lat(:,1,j)
                    end if
                end if
            end do
        end do
    
    end subroutine

    subroutine find_angle2(bondss)
        type(bonds),dimension(:),intent(inout) :: bondss

        integer                                :: i, j, k, angle_ind, iatom, jatom, angle_ind2
        integer,dimension(4,15)                :: angle2_tmp

        do i=1,size(bondss)
            iatom=bondss(i)%ind
            angle_ind=0
            angle2_tmp=-99
            do j=1,4
                if (bondss(i)%bond(1,j)==-99) exit
                jatom=bondss(i)%bond(2,j)
                angle_ind2=0
                do k=1,6
                    if (bondss(jatom)%angle(1,k)==-99) exit
                    if (bondss(jatom)%angle(2,k)==iatom) then
                        angle_ind=angle_ind+1
                        angle_ind2=angle_ind2+1
                        angle2_tmp(1,angle_ind)=bondss(jatom)%angle(1,k)
                        angle2_tmp(2,angle_ind)=bondss(jatom)%ind
                        angle2_tmp(3,angle_ind)=bondss(jatom)%angle(3,k)
                        angle2_tmp(4,angle_ind)=bondss(jatom)%angle(4,k)
!                       bondss(i)%angle2(1,angle_ind)=bondss(jatom)%angle(1,k)
!                       bondss(i)%angle2(2,angle_ind)=bondss(jatom)%ind
!                       bondss(i)%angle2(3,angle_ind)=bondss(jatom)%angle(3,k)
!                       bondss(i)%angle2(4,angle_ind)=bondss(jatom)%angle(4,k)
                    end if
                    if (bondss(jatom)%angle(3,k)==iatom) then
                        angle_ind=angle_ind+1
                        angle_ind2=angle_ind2+1
                        angle2_tmp(1,angle_ind)=bondss(jatom)%angle(1,k)
                        angle2_tmp(2,angle_ind)=bondss(jatom)%ind
                        angle2_tmp(3,angle_ind)=bondss(jatom)%angle(2,k)
                        angle2_tmp(4,angle_ind)=bondss(jatom)%angle(4,k)
!                       bondss(i)%angle2(1,angle_ind)=bondss(jatom)%angle(1,k)
!                       bondss(i)%angle2(2,angle_ind)=bondss(jatom)%ind
!                       bondss(i)%angle2(3,angle_ind)=bondss(jatom)%angle(2,k)
!                       bondss(i)%angle2(4,angle_ind)=bondss(jatom)%angle(4,k)
                    end if
                end do
            end do
            if (angle_ind>12) then
                 write(6,*) 'Found wrong angle '
                 write(6,*) angle2_tmp
                 call print_bonds(bondss,'xxxxxx')
                 stop
            end if
            bondss(i)%angle2=angle2_tmp(:,1:12)
        end do

    end subroutine



    subroutine find_bonds_boundary(bondss,coord,lat)
        type(bonds),dimension(:),intent(inout)      :: bondss
        real*8,dimension(:,:),intent(in)      :: coord
        real*8,dimension(3,3),intent(in)      :: lat

        integer                               :: nbond, i,j, atom1_ind, atom2_ind, atom3_ind
        real*8,dimension(3)                   :: coord1, coord2, coord3

        do i=1,size(bondss)
            if (bondss(i)%typ==ATOM_TYPE1) then
                atom1_ind=bondss(i)%ind
                coord1=coord(:,atom1_ind)
                do j=1,4
                    atom2_ind=bondss(i)%bond(2,j)
                    coord2=coord(:,atom2_ind)
                    bondss(i)%bond_lat(:,1,j)=lat_shift(coord1,coord2,lat)
                end do
                do j=1,6
                    atom2_ind=bondss(i)%angle(2,j)
                    atom3_ind=bondss(i)%angle(3,j)
                    coord2=coord(:,atom2_ind)
                    coord3=coord(:,atom3_ind)
                    bondss(i)%angle_lat(:,1,j)=lat_shift(coord1,coord2,lat)
                    bondss(i)%angle_lat(:,2,j)=lat_shift(coord1,coord3,lat)
                end do
                do j=1,12
                    if (bondss(i)%angle2(1,j)==-99) exit
                    atom2_ind=bondss(i)%angle2(2,j)
                    atom3_ind=bondss(i)%angle2(3,j)
                    coord2=coord(:,atom2_ind)
                    coord3=coord(:,atom3_ind)
                    bondss(i)%angle2_lat(:,1,j)=lat_shift(coord1,coord2,lat)
                    bondss(i)%angle2_lat(:,2,j)=lat_shift(coord1,coord3,lat)
                end do
            end if
            if (bondss(i)%typ==ATOM_TYPE2) then
                atom1_ind=bondss(i)%ind
                coord1=coord(:,atom1_ind)
                do j=1,2
                    atom2_ind=bondss(i)%bond(2,j)
                    coord2=coord(:,atom2_ind)
                    bondss(i)%bond_lat(:,1,j)=lat_shift(coord1,coord2,lat)
                end do
                do j=1,1
                    atom2_ind=bondss(i)%angle(2,j)
                    atom3_ind=bondss(i)%angle(3,j)
                    coord2=coord(:,atom2_ind)
                    coord3=coord(:,atom3_ind)
                    bondss(i)%angle_lat(:,1,j)=lat_shift(coord1,coord2,lat)
                    bondss(i)%angle_lat(:,2,j)=lat_shift(coord1,coord3,lat)
                end do
                do j=1,12
                    if (bondss(i)%angle2(1,j)==-99) exit
                    atom2_ind=bondss(i)%angle2(2,j)
                    atom3_ind=bondss(i)%angle2(3,j)
                    coord2=coord(:,atom2_ind)
                    coord3=coord(:,atom3_ind)
                    bondss(i)%angle2_lat(:,1,j)=lat_shift(coord1,coord2,lat)
                    bondss(i)%angle2_lat(:,2,j)=lat_shift(coord1,coord3,lat)
                end do
            end if
        end do

    end subroutine



    subroutine lat_shift_function(coord1, coord2, lat, dist_out, latshift)
        real*8,dimension(3),intent(in)       :: coord1, coord2
        real*8,dimension(3,3),intent(in)     :: lat
        integer,dimension(3),intent(out)       :: latshift
        real*8,intent(out)                   :: dist_out

        real*8,dimension(27)     :: dist
        integer,dimension(3,27)    :: dist_lat
        integer                    :: x,y,z,iline
        integer,dimension(1)       :: lat_shift_index

        dist=0d0
        dist_lat=0
        iline=0
        do x=-1,1
            do y=-1,1
                do z=-1,1
                    iline=iline+1
                    dist(iline)=dist_norm(coord1,  &
coord2+x*lat(1,:)+y*lat(2,:)+z*lat(3,:))
                    dist_lat(1,iline)=x
                    dist_lat(2,iline)=y
                    dist_lat(3,iline)=z
                end do
            end do
        end do

        lat_shift_index=minloc(dist)
        dist_out=minval(dist)
        latshift=dist_lat(:,lat_shift_index(1))

    end subroutine



    subroutine n_nn_neighbors(bondss,n_table,nn_table)
        type(bonds),dimension(:)               :: bondss
        integer,dimension(:,:),intent(out)     :: n_table
        integer,dimension(:,:),intent(out)     :: nn_table

        integer      :: i, j, k, iatom, atom_ind1, atom_ind2, neigh_atom

        n_table=-99
        nn_table=-99

        do i=1,size(bondss)
            iatom=bondss(i)%ind
            atom_ind1=0; atom_ind2=0
            do j=1,4
                if (bondss(i)%bond(2,j)==-99) exit
                atom_ind2=atom_ind2+1
                neigh_atom=bondss(i)%bond(2,j)
                n_table(atom_ind2,iatom)=neigh_atom
                do k=1,4
                    if (bondss(neigh_atom)%bond(2,k)==-99) exit
                    if (bondss(neigh_atom)%bond(2,k)==iatom) cycle
                    atom_ind1=atom_ind1+1
                    nn_table(atom_ind1,iatom)=bondss(neigh_atom)%bond(2,k)
                end do
            end do
        end do
!           write(6,*) '**', i, n_table(:,i), nn_table(:,i)
                 
    end subroutine


    subroutine find_neighbors(coord,lat,names,n_table,nn_table, &
neigh_table,neigh_table_lat)
        real*8,dimension(:,:),intent(in)      :: coord
        real*8,dimension(3,3),intent(in)      :: lat
        integer,dimension(:  ),intent(in)     :: names
        integer,dimension(:,:),intent(in)     :: n_table
        integer,dimension(:,:),intent(in)     :: nn_table
        integer,dimension(:,:),intent(out)    :: neigh_table
        integer,dimension(:,:,:),intent(out)  :: neigh_table_lat

        integer                :: i, natom, j, ineigh
        integer,dimension(3)   :: latshift
        real*8               :: dist_out
        real*8,dimension(size(names))    :: dist_array
        logical                :: num_in_n_nn

        integer                :: nthread, chunk !, OMP_GET_NUM_THREADS

        natom=size(names)
        neigh_table=-99
        neigh_table_lat=-99

!$OMP PARALLEL SHARED(lat,coord,natom,neigh_table,neigh_table_lat)  &
!$OMP PRIVATE(i,dist_array,ineigh,j,num_in_n_nn,latshift,dist_out)
        nthread = OMP_GET_NUM_THREADS()
        chunk = ceiling( natom/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
        do i=1,natom
            dist_array=1000d0
            ineigh=0
            do j=1,natom
                if (j==i) cycle
!               num_in_n_nn=any(n_table(:,i)==j) ! .or. any(nn_table(:,i)==j)  ! treat as neighbor if not bond directly
                num_in_n_nn=any(n_table(:,i)==j)   .or. any(nn_table(:,i)==j)  ! treat as neighbor if not bond or second bond
                if ( .not. num_in_n_nn) then
                    call lat_shift_function(coord(:,i),coord(:,j), &
lat,dist_out,latshift)
                    if (dist_out<=neigh_cutoff(1)) then
                        ineigh=ineigh+1
                        neigh_table(ineigh,i)=j
                        neigh_table_lat(:,ineigh,i)=latshift
                        dist_array(ineigh)=dist_out
                        if (ineigh > numneigh) then
                            write(6,*) 'too many non-bonding neighbors' , i, j
                            stop
                        end if
                    end if
                end if
            end do
        !   write(94,'(A,I10,4I4,12I4)') 'n and nn ', i, (n_table(j,i),j=1,4), (nn_table(j,i),j=1,12)
        !   write(94,'(A,I10,50I4)') 'neighbor ', i, (neigh_table(j,i),j=1,numneigh)
        end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL
        !write(94,*)

    end subroutine




      INTEGER FUNCTION  FindMinimum(x, Start, End)
        IMPLICIT  NONE
        real(dp), DIMENSION(1:), INTENT(IN) :: x
        INTEGER, INTENT(IN)                :: Start, End
        real(dp)                           :: Minimum
        INTEGER                            :: Location
        INTEGER                            :: i

        Minimum  = x(Start)            ! assume the first is the min
        Location = Start                  ! record its position
        DO i = Start+1, End            ! start with next elements
           IF (x(i) < Minimum) THEN      !   if x(i) less than the min?
              Minimum  = x(i)            !      Yes, a new minimum found
              Location = i                !      record its position
           END IF
        END DO
        FindMinimum = Location              ! return the position
      END FUNCTION



!   --------------------------------------------------------------------
!   SUBROUTINE  Swap():
!      This subroutine swaps the values of its two formal arguments.
!   --------------------------------------------------------------------

      SUBROUTINE  Swap(a, b)
        IMPLICIT  NONE
        real(dp)   , INTENT(INOUT) :: a, b
        real(dp)               :: Temp

        Temp = a
        a    = b
        b    = Temp
      END SUBROUTINE
      SUBROUTINE  Swap_int(a, b)
        IMPLICIT  NONE
        integer,     INTENT(INOUT) :: a, b
        integer                :: Temp

        Temp = a
        a    = b
        b    = Temp
      END SUBROUTINE


!   --------------------------------------------------------------------
!   SUBROUTINE  Sort():
!      This subroutine receives an array x() and sorts it into ascending
!   order.
!   --------------------------------------------------------------------

      SUBROUTINE  Sort(x, Size)
        IMPLICIT  NONE
        real(dp)   , DIMENSION(1:), INTENT(INOUT) :: x
        INTEGER, INTENT(IN)                   :: Size
        INTEGER                               :: i
        INTEGER                               :: Location

        DO i = 1, Size-1                  ! except for the last
           Location = FindMinimum(x, i, Size)      ! find min from this to last
           CALL  Swap(x(i), x(Location))      ! swap this and the minimum
        END DO
      END SUBROUTINE

      ! sort y based on sorting x
      SUBROUTINE  Sort_arg(x, y, Size)
        IMPLICIT  NONE
        real(dp)   , DIMENSION(1:), INTENT(INOUT) :: x
        integer,     DIMENSION(1:), INTENT(INOUT) :: y
        INTEGER, INTENT(IN)                   :: Size
        INTEGER                               :: i
        INTEGER                               :: Location

        DO i = 1, Size-1                  ! except for the last
           Location = FindMinimum(x, i, Size)      ! find min from this to last
           CALL  Swap(x(i), x(Location))      ! swap this and the minimum
           CALL  Swap_int(y(i), y(Location))      ! swap this and the minimum
        END DO
      END SUBROUTINE

    function  pick_random_from_arr( arr, mask )
        integer,dimension(:  )         :: arr
        integer                        :: mask
        integer                        :: pick_random_from_arr

        integer                        :: i,j, n,m

        n=0; j=0
        do i=1,size(arr)
            if (arr(i)==mask) n=n+1
        end do
        m=get_rand_int(1,n)
        do i=1,size(arr)
            if (arr(i)==mask) then 
                j=j+1
                if (j==n) then
                    pick_random_from_arr=j
                    exit
                end if
            end if
        end do

    end function

    function check_repeat_element(arr)
        integer,dimension(:),intent(in)         :: arr
        logical                                 :: check_repeat_element

        integer                                 :: i,j

        check_repeat_element=.false.
        do i=1,size(arr)-1
            do j=i+1,size(arr)
                if (arr(i)==arr(j)) check_repeat_element=.true.
            end do
        end do
    end function

    function count_repeat_element(arr,element)
        integer,dimension(:),intent(in)         :: arr
        integer                                 :: count_repeat_element
        integer                                 :: element

        integer                                 :: i,j

        count_repeat_element=0
        do i=1,size(arr)
            if (arr(i)==element) count_repeat_element=count_repeat_element+1
        end do
    end function

    function element_in_arr(arr,element)
        integer,dimension(:),intent(in)         :: arr
        integer                                 :: element_in_arr
        integer                                 :: element

        integer                                 :: i
        element_in_arr=-99
        do i=1,size(arr)
            if (arr(i)==element) then
                element_in_arr=i
                exit
            end if
        end do
    end function

    function check_bonds_integrity(bondss,names)
        type(bonds),dimension(:),intent(in   )  :: bondss
        integer    ,dimension(:),intent(in   )  :: names
        logical                                 :: check_bonds_integrity

        integer                                 :: natom, i, j, a1,a2,a3,a4
        integer                                 :: c1,c2,c3,c4
        integer,dimension(12)                   :: angle_list
       
        check_bonds_integrity=.true.
        if (size(names)/=size(bondss)) then
            check_bonds_integrity=.false.
            write(6,*) 'Error  12, inconsistent number of atoms'
        end if
        do i=1,size(names)
            if (names(i)/=bondss(i)%typ) then
                check_bonds_integrity=.false.
                write(6,*) 'Error  13, inconsistent Names and Type'
            end if
        end do
        do i=1,size(bondss)
            if (bondss(i)%typ==ATOM_TYPE1) then
                if (check_repeat_element(bondss(i)%bond(2,1:4))) then    
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 0'
                    write(6,*) i
                    write(6,*) bondss(i)%bond(2,1:4)
                end if
                if (any(bondss(i)%bond(1,1:4)==-99)) then
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 1'
                    write(6,*) i
                end if
                if (any(bondss(i)%angle(1,1:6)==-99)) then
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 2'
                    write(6,*) i
                end if
                if (any(bondss(i)%angle2(1,1:4)==-99)) then
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 3'
                    write(6,*) i
                    write(6,*) bondss(i)%angle2
                end if
                do j=1,6
                    angle_list(j)=bondss(i)%angle(2,j)
                    angle_list(j+6)=bondss(i)%angle(3,j)
                end do
                if (count_repeat_element(angle_list,bondss(i)%bond(2,1)) /= 3 &
.or. count_repeat_element(angle_list,bondss(i)%bond(2,2)) /= 3 &
.or. count_repeat_element(angle_list,bondss(i)%bond(2,3)) /= 3 &
.or. count_repeat_element(angle_list,bondss(i)%bond(2,4)) /= 3)  then
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 4'
                    write(6,*) i
                    write(6,*) angle_list
                    write(6,*) bondss(i)%bond
                end if
            end if
            if (bondss(i)%typ==ATOM_TYPE2) then
                if (check_repeat_element(bondss(i)%bond(2,1:2))) then    
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 5'
                    write(6,*) i
                end if
                if (any(bondss(i)%bond(1,1:2)==-99)) then
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 6'
                    write(6,*) i
                end if
                if (bondss(i)%angle(1,1)==-99) then
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 7'
                    write(6,*) i
                end if
                if (any(bondss(i)%angle2(1,1:6)==-99)) then
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 8'
                    write(6,*) i
                    write(6,*) bondss(i)%angle2
                end if
                if (bondss(i)%angle(2,1)==bondss(i)%bond(2,1)) then
                    if (bondss(i)%angle(3,1)/=bondss(i)%bond(2,2)) then
                        check_bonds_integrity=.false.
                        write(6,*) 'Error, 9'
                        write(6,*) i
                    end if
                else if (bondss(i)%angle(2,1)==bondss(i)%bond(2,2)) then
                    if (bondss(i)%angle(3,1)/=bondss(i)%bond(2,1)) then
                        check_bonds_integrity=.false.
                        write(6,*) 'Error, 10'
                        write(6,*) i
                    end if
                else
                    check_bonds_integrity=.false.
                    write(6,*) 'Error, 11'
                    write(6,*) i
                end if
            end if
        end do

500 continue
    end function


    function copybonds(bondss)
        type(bonds),dimension(:),intent(in   )  :: bondss
        type(bonds),dimension(:),allocatable    :: copybonds

        integer                                 :: i, natom

        natom=size(bondss)
        allocate(copybonds(natom))
        do i=1,natom
            copybonds(i)%ind=bondss(i)%ind
            copybonds(i)%typ=bondss(i)%typ
            copybonds(i)%bond=bondss(i)%bond
            copybonds(i)%angle=bondss(i)%angle
            copybonds(i)%angle2=bondss(i)%angle2
        end do
    end function
       
 
    subroutine bond_switching(bondss,names, coord,lat,imov_at,no_isolate)
        type(bonds),dimension(:),intent(inout)  :: bondss
        integer,dimension(:),intent(in)         :: names
        real*8,dimension(:,:),intent(in)        :: coord
        real*8,dimension(3,3),intent(in)        :: lat
        integer,dimension(:,:),intent(in)       :: imov_at
        logical,intent(in)                      :: no_isolate

        type(bonds),dimension(:),allocatable    :: bondss_tmp
        integer  :: pickatom, pickatom2, natom, which_bond, pick_trial_atom, level, pick_bond
        integer  :: pickatom_tmp, pickatom2_tmp,pickatom_tmp_sisi, pickatom2_tmp_sisi
        integer  :: pickatom_tmp2, pickatom2_tmp2, pickatom_tmp_ng, pickatom2_tmp_ng
        integer  :: atom1, atom1_bond, atom2, atom2_bond, i, j, m, n, k, through_o
        integer  :: pick_bond_ng, pickatom_tmp_sisi_ng, pickatom2_tmp_sisi_ng, level_ng, pickatom_tmp2_ng, pickatom2_tmp2_ng
        logical  :: applica_atom1, applica_atom2, trial_atom, applica_trial_atom1,applica_trial_atom2
        logical  :: repick_atom1, repick_atom2, linked, linked_tmp, linked_ng, linked_tmp_ng
        integer,dimension(4) :: atom1_fix, atom2_fix, atom1_neigh, atom2_neigh, level_arr, level_plus
        integer,dimension(4,4) :: pickatom_bond, pickatom2_bond
        logical,dimension(4) :: linked_arr

        natom=size(bondss)

300  continue

        pickatom=bondss(get_rand_int(1,natom))%ind
        ! will not choose Oxygen, or fixed atom position, or fixed bonding
        if (names(pickatom)==ATOM_TYPE2 .or. any(imov_at(:,pickatom)==0) .or. all(bondss(pickatom)%bond(4,:)==0)) &
 goto 300

        ! check if pickatom and pickatom2 satisfy
        atom1_fix=bondss(pickatom)%bond(4,1:4)
        applica_atom1=.false.; applica_atom2=.false.
        through_o=-1
        do while(.not. applica_atom1 .and. .not. applica_atom2)
            ! if all bonds that bonding pickatom are tried and not work, choose another pickatom
            if (all(atom1_fix==-99)) goto 300
            which_bond=get_rand_int(1,4)
            atom1_fix(which_bond)=-99
            pick_trial_atom=bondss(pickatom)%bond(2,which_bond)
            ! ok if bonds left (i.e. available bands not bonded to pickatom2) can be switched (but not fixed)
            applica_atom1=any(atom1_fix==1)
            ! if oxygen
            if (names(pick_trial_atom)==ATOM_TYPE2) then
                if (bondss(pick_trial_atom)%bond(2,1)==pickatom) pickatom2=bondss(pick_trial_atom)%bond(2,2)
                if (bondss(pick_trial_atom)%bond(2,2)==pickatom) pickatom2=bondss(pick_trial_atom)%bond(2,1)
                atom2_fix=bondss(pickatom2)%bond(4,1:4)
                do i=1,4
                    if (bondss(pickatom2)%bond(2,i)==pick_trial_atom) atom2_fix(i)=-99
                end do
                ! ok if bonds left (i.e. not bonded to pickatom) can be switched (not fixed)
                applica_atom2=any(atom2_fix==1)
                through_o=pick_trial_atom
            end if
            ! if silicon
            if (names(pick_trial_atom)==ATOM_TYPE1) then
                pickatom2=pick_trial_atom
                atom2_fix=bondss(pickatom2)%bond(4,:)
                do i=1,4
                    if (bondss(pickatom2)%bond(2,i)==pickatom) atom2_fix(i)=-99
                end do
                ! ok if bonds left (i.e. not bonded to pickatom) can be switched (not fixed)
                applica_atom2=any(atom2_fix==1)
                through_o=-1
            end if
        end do
        !

        ! find atom_fix for pickatom/pickatom2
        atom1_fix=bondss(pickatom)%bond(4,1:4)
        atom2_fix=bondss(pickatom2)%bond(4,1:4)
        if (through_o==-1) then
            do i=1,4
                if (bondss(pickatom)%bond(2,i)==pickatom2) atom1_fix(i)=-99
                if (bondss(pickatom2)%bond(2,i)==pickatom) atom2_fix(i)=-99
            end do
        else
            do i=1,4
                if (bondss(pickatom)%bond(2,i)==through_o) atom1_fix(i)=-99
                if (bondss(pickatom2)%bond(2,i)==through_o) atom2_fix(i)=-99
            end do
        end if
!       write(6,*) 'atom1_fix', atom1_fix
!       write(6,*) 'atom2_fix', atom2_fix
        !
301     continue
        ! choose atoms bonded to pickatom/pickatom2
        applica_trial_atom1=.false.
        ! check atom1
        do while(.not. applica_trial_atom1)
            ! if all the atoms bonded to pickatom are tried and not work, choose another pickatom
            if (.not. any(atom1_fix==1)) then
                write(600,*) 'reset atom 301'
                goto 300
            end if
            applica_trial_atom1=.true.
            which_bond=get_rand_int(1,4)
            do while(.not. atom1_fix(which_bond)==1)
                which_bond=get_rand_int(1,4)
            end do
            atom1_fix(which_bond)=-99
            atom1=bondss(pickatom)%bond(2,which_bond)
            atom1_bond=which_bond
            ! atom1 cannot bond to pickatom2
            if (element_in_arr(bondss(pickatom2)%bond(2,1:4),atom1)/=-99) then
                applica_trial_atom1=.false.
                cycle
            end if
            ! find neighbor of atom1 (except pickatom)
            m=0; atom1_neigh=-99
            do i=1,4
                if (bondss(atom1)%bond(2,i)==-99) exit
                if (bondss(atom1)%bond(2,i)/=pickatom) then
                    m=m+1
                    atom1_neigh(m)=bondss(atom1)%bond(2,i)
                end if
            end do
            ! check if neighbor of atom1 is too close to pickatom2
            do i=1,4
                if (atom1_neigh(i)==-99) exit
                ! neighbor of atom1 can't bond to pickatom2
                if (element_in_arr(bondss(pickatom2)%bond(2,1:4),atom1_neigh(i))/=-99) then
                    applica_trial_atom1=.false.
                    exit
                end if
                ! for cases neighbor of atom1 is the next-nearest neighbor of pickatom2
                if (element_in_arr(bondss(pickatom2)%angle2(3,1:12),atom1_neigh(i))/=-99) then
!                   n=element_in_arr(bondss(pickatom2)%angle2(3,1:12),atom1_neigh(i))
!                   ! if atom1 is Oxygen, its neighbor can't bond to pickatom2 via another oxygen
!                   if (names(atom1)==ATOM_TYPE2 .and. names(bondss(pickatom2)%angle2(2,n))==ATOM_TYPE2) then
                        applica_trial_atom1=.false.
                        exit
!                   end if
                end if
            end do
        end do

302     continue
        ! check atom2 (same to atom1)
        applica_trial_atom2=.false.
        do while(.not. applica_trial_atom2)
            if (.not. any(atom2_fix==1)) then
                write(600,*) 'reset atom 302 '
                goto 300
            end if
            applica_trial_atom2=.true.
            which_bond=get_rand_int(1,4)
            do while(.not. atom2_fix(which_bond)==1)
                which_bond=get_rand_int(1,4)
            end do
            atom2_fix(which_bond)=-99
            atom2=bondss(pickatom2)%bond(2,which_bond)
            atom2_bond=which_bond
            ! atom2 cannot bond to pickatom
            if (element_in_arr(bondss(pickatom)%bond(2,1:4),atom2)/=-99) then
                applica_trial_atom2=.false.
                cycle
            end if
            ! find neighbor of atom2
            m=0; atom2_neigh=-99
            do i=1,4
                if (bondss(atom2)%bond(2,i)==-99) exit
                if (bondss(atom2)%bond(2,i)/=pickatom2) then
                    m=m+1
                    atom2_neigh(m)=bondss(atom2)%bond(2,i)
                end if
            end do
            ! check if neighbor of atom2 is close to pickatom
            do i=1,4
                if (atom2_neigh(i)==-99) exit
                ! neighbor of atom1 can't bond to pickatom2
                if (element_in_arr(bondss(pickatom)%bond(2,1:4),atom2_neigh(i))/=-99) then
                    applica_trial_atom2=.false.
                    exit
                end if
                if (element_in_arr(bondss(pickatom)%angle2(3,1:12),atom2_neigh(i))/=-99) then
!                   n=element_in_arr(bondss(pickatom)%angle2(3,1:12),atom2_neigh(i))
!                   ! if atom2 is Oxygen, its neighbor can't bond to pickatom via oxygen
!                   if (names(atom2)==ATOM_TYPE2 .and. names(bondss(pickatom)%angle2(2,n))==ATOM_TYPE2) then
                        applica_trial_atom2=.false.
                        exit
!                   end if
                end if
            end do
        end do

        ! this is rare case and it is a trial bond switching
        if (atom1==atom2) write(600,*) 't', atom1, atom2

        ! check Si-Si bond to avoid isolate this bond
        pickatom_tmp=pickatom; pickatom2_tmp=pickatom2
        if (no_isolate) then
            repick_atom1=.false.; repick_atom2=.false.
            bondss_tmp=copybonds(bondss)
            bondss_tmp(pickatom_tmp)%bond(2,atom1_bond)=atom2
            bondss_tmp(pickatom_tmp)%bond(1,atom1_bond)=decide_bond_type(bondss_tmp(pickatom_tmp)%typ,names(atom2))
            bondss_tmp(pickatom2_tmp)%bond(2,atom2_bond)=atom1
            bondss_tmp(pickatom2_tmp)%bond(1,atom2_bond)=decide_bond_type(bondss_tmp(pickatom2_tmp)%typ,names(atom1))
            if (names(atom1)==ATOM_TYPE2) then
                if (bondss_tmp(atom1)%bond(2,1)==pickatom_tmp) then
                    bondss_tmp(atom1)%bond(2,1)=pickatom2_tmp
                    bondss_tmp(atom1)%bond(1,1)=11
                end if
                if (bondss_tmp(atom1)%bond(2,2)==pickatom_tmp) then
                    bondss_tmp(atom1)%bond(2,2)=pickatom2_tmp
                    bondss_tmp(atom1)%bond(1,2)=11
                end if
            else if (names(atom1)==ATOM_TYPE1) then
                do i=1,4
                    if (bondss_tmp(atom1)%bond(2,i)==pickatom_tmp) then
                        bondss_tmp(atom1)%bond(2,i)=pickatom2_tmp
                        bondss_tmp(atom1)%bond(1,i)=12
                    end if
                end do
            end if
            if (names(atom2)==ATOM_TYPE2) then
                if (bondss_tmp(atom2)%bond(2,1)==pickatom2_tmp) then
                    bondss_tmp(atom2)%bond(2,1)=pickatom_tmp
                    bondss_tmp(atom2)%bond(1,1)=11
                end if
                if (bondss_tmp(atom2)%bond(2,2)==pickatom2_tmp) then
                    bondss_tmp(atom2)%bond(2,2)=pickatom_tmp
                    bondss_tmp(atom2)%bond(1,2)=11
                end if
            else if (names(atom2)==ATOM_TYPE1) then
                do i=1,4
                    if (bondss_tmp(atom2)%bond(2,i)==pickatom2_tmp) then
                        bondss_tmp(atom2)%bond(2,i)=pickatom_tmp
                        bondss_tmp(atom2)%bond(1,i)=12
                    end if
                end do
            end if

            if (count_repeat_element(bondss_tmp(pickatom_tmp)%bond(1,:),12)>=1) then
                linked=.false.
                level_arr=-99; linked_arr=.false.; level_plus=0
                do i=1,4
                    if (bondss_tmp(pickatom_tmp)%bond(1,i)==12) then
                        pick_bond=i
                        pickatom_tmp_sisi=bondss_tmp(pickatom_tmp)%bond(2,pick_bond)
                        level=1; linked_tmp=.false.
                        pickatom_tmp2=pickatom_tmp
!                       write(6,*) 'level1 ',i,'  sta ', level, pick_bond
                        call check_si_si_bond(bondss_tmp, pickatom_tmp2, pickatom_tmp_sisi, imov_at, linked_tmp,level)
!                       write(6,*) 'level1 ',i,'  end ', level, linked_tmp
                        linked=linked .or. linked_tmp
                        level_arr(i)=level
                        linked_arr(i)=linked_tmp
!                       if (linked) exit
                    end if
                end do
                write(600,*) ' this atom 1 ', level_arr, linked_arr
                do i=1,4
                    ! check the case when there is non-linked, and its level<NLEVEL
                    if ((abs(level_arr(i))<NLEVEL) .and. (linked_arr(i).eqv. .false.)) then
!                       write(6,*) '  atom1..', i, abs(level_arr(i))<NLEVEL, linked_arr(i).eqv. .false., (abs(level_arr(i))<NLEVEL) .and. (linked_arr(i).eqv. .false.)
                        do j=1,4
                            if (linked_arr(j).eqv. .true.) then
                                level_plus(j)=level_arr(i)+level_arr(j)
                            else
                                level_plus(j)=99
                            end if
                        end do
!                       write(6,*) '  atom1 ... ', i, level_plus, abs(level_arr(i))<NLEVEL
                        if (all(level_plus-1>=NLEVEL)) then
                            linked=.false.
                            write(600,*) ' too far2 ', pickatom_tmp, level_plus, level_arr, linked_arr
!                           write(6,*) ' too far2 ', pickatom_tmp, level_plus, level_arr, linked_arr
                            exit
                        end if
                    end if
                    ! check the case when there is non-linked, and its level>=nlevel
                    if ((level_arr(i)>=nlevel) .and. (linked_arr(i).eqv. .false.)) then
                        ! find index for its neighbor atom (in the direction level>=nlevel)
                        pickatom_tmp_ng=bondss_tmp(pickatom_tmp)%bond(2,i)
                        linked_ng=.false.
                        ! check this neighbor link condition
                        write(600,*) ' checking neighbor link 1 ...', pickatom_tmp, i, pickatom_tmp_ng
                        do j=1,4
                            if (bondss_tmp(pickatom_tmp_ng)%bond(1,j)==12) then
                                pick_bond_ng=j
                                pickatom_tmp_sisi_ng=bondss_tmp(pickatom_tmp_ng)%bond(2,pick_bond_ng)
                                level_ng=1; linked_tmp_ng=.false.
                                pickatom_tmp2_ng=pickatom_tmp_ng
                                call check_si_si_bond(bondss_tmp, pickatom_tmp2_ng, pickatom_tmp_sisi_ng, imov_at, linked_tmp_ng,level_ng)
                                write(600,*) j, linked_tmp_ng
                                linked_ng=linked_ng .or. linked_tmp_ng
                                if (linked_ng) exit
                            end if
                        end do
                        write(600,*) ' neighbor link ', pickatom_tmp, i, pickatom_tmp_ng, linked_ng
                        ! if this neighbor not linked, pickatom should be re-assign (linked set to false)
                        if (.not. linked_ng) then
                            linked=.false.
                            write(600,*) ' neighbor too far1 ', pickatom_tmp, i, pickatom_tmp_ng
                            exit
                        end if
                    end if
                end do
!               write(6,*) ' this atom 11 ', linked
                if (.not. linked) then
                    ! in this case, repick atom2
                    repick_atom2=.true.
                    atom2_fix(atom2_bond)=-99
                end if
            end if


            if (count_repeat_element(bondss_tmp(pickatom2_tmp)%bond(1,:),12)>=1) then
                linked=.false.
                level_arr=-99; linked_arr=.false.; level_plus=0
                do i=1,4
                    if (bondss_tmp(pickatom2_tmp)%bond(1,i)==12) then
                        pick_bond=i
                        pickatom2_tmp_sisi=bondss_tmp(pickatom2_tmp)%bond(2,pick_bond)
                        level=1; linked_tmp=.false.
                        pickatom2_tmp2=pickatom2_tmp
!                       write(6,*) 'level2 ',i,'  sta ', level, pick_bond
                        call check_si_si_bond(bondss_tmp, pickatom2_tmp2, pickatom2_tmp_sisi, imov_at, linked_tmp,level)
!                       write(6,*) 'level2 ',i,'  end ', level, linked_tmp
                        linked=linked .or. linked_tmp
                        level_arr(i)=level
                        linked_arr(i)=linked_tmp
                        level_plus(i)=0
!                       if (linked) exit
                    end if
                end do
                write(600,*) ' this atom 2 ', level_arr, linked_arr
                do i=1,4
                    if ((abs(level_arr(i))<NLEVEL) .and. (linked_arr(i).eqv. .false.)) then
!                       write(6,*) '  atom2..', i, abs(level_arr(i))<NLEVEL, linked_arr(i).eqv. .false., (abs(level_arr(i))<NLEVEL) .and. (linked_arr(i).eqv. .false.)
                        do j=1,4
                            if (linked_arr(j).eqv. .true.) then
                                level_plus(j)=level_arr(i)+level_arr(j)
                            else
                                level_plus(j)=99
                            end if
                        end do
!                       write(6,*) '  atom2..', i, level_plus, abs(level_arr(i))<NLEVEL
                        if (all(level_plus-1>=NLEVEL)) then
                            linked=.false.
                            write(600,*) ' too far1 ', pickatom2_tmp, level_plus, level_arr, linked_arr
!                           write(6,*) ' too far1 ', pickatom2_tmp, level_plus, level_arr, linked_arr
                            exit
                        end if
                    end if
                    ! check the case when there is non-linked, and level >= nlevel case
                    if ((level_arr(i)>=nlevel) .and. (linked_arr(i).eqv. .false.)) then
                        ! find index for its neighbor atom (which in the direction level>=nlevel)
                        pickatom2_tmp_ng=bondss_tmp(pickatom2_tmp)%bond(2,i)
                        linked_ng=.false.
                        write(600,*) ' checking neighbor link 2 ... ', pickatom2_tmp, i, pickatom2_tmp_ng
                        do j=1,4
                            if (bondss_tmp(pickatom2_tmp_ng)%bond(1,j)==12) then
                                pick_bond_ng=j
                                pickatom2_tmp_sisi_ng=bondss_tmp(pickatom2_tmp_ng)%bond(2,pick_bond_ng)
                                level_ng=1; linked_tmp_ng=.false.
                                pickatom2_tmp2_ng=pickatom2_tmp_ng
                                call check_si_si_bond(bondss_tmp, pickatom2_tmp2_ng, pickatom2_tmp_sisi_ng, imov_at, linked_tmp_ng,level_ng)
                                write(600,*) j, linked_tmp_ng
                                linked_ng=linked_ng .or. linked_tmp_ng
                                if (linked_ng) exit
                            end if
                        end do
                        write(600,*) ' neighbor link ', pickatom2_tmp, i, pickatom2_tmp_ng, linked_ng
                        if (.not. linked_ng) then
                            linked=.false.
                            write(600,*) ' neighbor too far2 ', pickatom2_tmp, i, pickatom2_tmp_ng
                            exit
                        end if
                    end if
                end do
!               write(6,*) ' this atom 22 ', linked
                if (.not. linked) then
                    ! in this case, repick atom1
                    repick_atom1=.true.
                    atom1_fix(atom1_bond)=-99
                end if
            end if


            if (allocated(bondss_tmp)) deallocate(bondss_tmp)
            if ((repick_atom1 .eqv. .true.) .and. (repick_atom2 .eqv. .false.)) then
!               write(6,*) ' r1 '
                write(600,*) ' r1 '
                goto 301
            else if ((repick_atom1 .eqv. .false.) .and. (repick_atom2 .eqv. .true.)) then
!               write(6,*) ' r2 '
                write(600,*) ' r2 '
                goto 302
            else if ((repick_atom1 .eqv. .true.) .and. (repick_atom2 .eqv. .true.)) then
!               write(6,*) ' r1 ', ' r2 '
                write(600,*) ' r1 ', ' r2 '
                goto 301
            end if
        end if

        write(600,*) 'picked ', pickatom, pickatom2, through_o, atom1, atom2
        ! found pickatom and atoms, then execute, change bond and angle index
        ! for pickatom (bond and angle) ...
        ! change bonds (no need to touch index for fixing)
        bondss(pickatom)%bond(2,atom1_bond)=atom2
        bondss(pickatom)%bond(1,atom1_bond)=decide_bond_type(bondss(pickatom)%typ,names(atom2))
        bondss(pickatom2)%bond(2,atom2_bond)=atom1
        bondss(pickatom2)%bond(1,atom2_bond)=decide_bond_type(bondss(pickatom2)%typ,names(atom1))
        ! change angle
        do i=1,6
            if (bondss(pickatom)%angle(2,i)==atom1) bondss(pickatom)%angle(2,i)=atom2
            if (bondss(pickatom)%angle(3,i)==atom1) bondss(pickatom)%angle(3,i)=atom2
            bondss(pickatom)%angle(1,i)=decide_angle_type(bondss(pickatom)%typ, &
names(bondss(pickatom)%angle(2,i)),names(bondss(pickatom)%angle(3,i)))
        end do
        do i=1,6
            if (bondss(pickatom2)%angle(2,i)==atom2) bondss(pickatom2)%angle(2,i)=atom1
            if (bondss(pickatom2)%angle(3,i)==atom2) bondss(pickatom2)%angle(3,i)=atom1
            bondss(pickatom2)%angle(1,i)=decide_angle_type(bondss(pickatom2)%typ, &
names(bondss(pickatom2)%angle(2,i)),names(bondss(pickatom2)%angle(3,i)))
        end do
        ! for atom1 (bond and angle) ...
        if (names(atom1)==ATOM_TYPE2) then
            if (bondss(atom1)%bond(2,1)==pickatom) then
                bondss(atom1)%bond(2,1)=pickatom2
                bondss(atom1)%bond(1,1)=11
            end if
            if (bondss(atom1)%bond(2,2)==pickatom) then
                bondss(atom1)%bond(2,2)=pickatom2
                bondss(atom1)%bond(1,2)=11
            end if
            if (bondss(atom1)%angle(2,1)==pickatom) bondss(atom1)%angle(2,1)=pickatom2
            if (bondss(atom1)%angle(3,1)==pickatom) bondss(atom1)%angle(3,1)=pickatom2
            bondss(atom1)%angle(1,1)=decide_angle_type(names(atom1),names(bondss(atom1)%angle(2,1)),&
names(bondss(atom1)%angle(3,1)))
        else if (names(atom1)==ATOM_TYPE1) then
            do i=1,4
                if (bondss(atom1)%bond(2,i)==pickatom) then
                    bondss(atom1)%bond(2,i)=pickatom2
                    bondss(atom1)%bond(1,i)=12
                end if
            end do
            do i=1,6
                if (bondss(atom1)%angle(2,i)==pickatom) bondss(atom1)%angle(2,i)=pickatom2
                if (bondss(atom1)%angle(3,i)==pickatom) bondss(atom1)%angle(3,i)=pickatom2
                bondss(atom1)%angle(1,i)=decide_angle_type(names(atom1), &
names(bondss(atom1)%angle(2,i)),names(bondss(atom1)%angle(3,i)))
            end do
        end if
        ! atom2 (bond and angle)
        if (names(atom2)==ATOM_TYPE2) then
            if (bondss(atom2)%bond(2,1)==pickatom2) then
                bondss(atom2)%bond(2,1)=pickatom
                bondss(atom2)%bond(1,1)=11
            end if
            if (bondss(atom2)%bond(2,2)==pickatom2) then
                bondss(atom2)%bond(2,2)=pickatom
                bondss(atom2)%bond(1,2)=11
            end if
            if (bondss(atom2)%angle(2,1)==pickatom2) bondss(atom2)%angle(2,1)=pickatom
            if (bondss(atom2)%angle(3,1)==pickatom2) bondss(atom2)%angle(3,1)=pickatom
            bondss(atom2)%angle(1,1)=decide_angle_type(names(atom2),names(bondss(atom2)%angle(2,1)),&
names(bondss(atom2)%angle(3,1)))
        else if (names(atom2)==ATOM_TYPE1) then
            do i=1,4
                if (bondss(atom2)%bond(2,i)==pickatom2) then
                    bondss(atom2)%bond(2,i)=pickatom
                    bondss(atom2)%bond(1,i)=12
                end if
            end do
            do i=1,6
                if (bondss(atom2)%angle(2,i)==pickatom2) bondss(atom2)%angle(2,i)=pickatom
                if (bondss(atom2)%angle(3,i)==pickatom2) bondss(atom2)%angle(3,i)=pickatom
                bondss(atom2)%angle(1,i)=decide_angle_type(names(atom2), &
names(bondss(atom2)%angle(2,i)),names(bondss(atom2)%angle(3,i)))
            end do
        end if
        
        call find_angle2(bondss)
        
        if ( .not. check_bonds_integrity(bondss,names)) then
            write(6,*) 'Nonconsistent Bonds, exit'
            stop
        end if

    end subroutine


    function decide_angle_type(typ1,typ2,typ3)
        integer                        :: typ1,typ2,typ3
        integer                        :: decide_angle_type

        if (typ1==ATOM_TYPE2) then
            if (typ2==ATOM_TYPE1 .and. typ3==ATOM_TYPE1) then
                decide_angle_type=22
            else
                write(6,*) 'Non existing angle type O',typ1, typ2,typ3
                stop
            end if
        else
            if (typ2==ATOM_TYPE1 .and. typ3==ATOM_TYPE1) then
                decide_angle_type=24
            else if (typ2==ATOM_TYPE1 .and. typ3==ATOM_TYPE2 .or.  &
typ2==ATOM_TYPE2 .and. typ3==ATOM_TYPE1)  then
                decide_angle_type=23
            else if (typ2==ATOM_TYPE2 .and. typ3==ATOM_TYPE2) then
                decide_angle_type=21
            else
                write(6,*) 'Non existing angle type Si',typ1, typ2,typ3
                stop
            end if
        end if

    end function

    function decide_bond_type(typ1,typ2)
        integer                        :: typ1, typ2
        integer                        :: decide_bond_type

        if (typ1==ATOM_TYPE1 .and. typ2==ATOM_TYPE1) then
            decide_bond_type=12
        else if (typ1==ATOM_TYPE1 .and. typ2==ATOM_TYPE2) then
            decide_bond_type=11
        else if (typ1==ATOM_TYPE2 .and. typ2==ATOM_TYPE1) then
            decide_bond_type=11
        else
            write(6,*) 'Non existing bond type',typ1, typ2
            stop
        end if

    end function

    recursive subroutine check_si_si_bond( bondss, atom0, atom, imov_at, linked, level )
        type(bonds),dimension(:),intent(in   )  :: bondss
        integer,dimension(:,:),intent(in)       :: imov_at
        integer               ,intent(inout)    :: atom, atom0, level
        logical               ,intent(inout)    :: linked

        integer                                 :: i, atom0_tmp, atom_tmp, level_tmp
        logical                                 :: linked_tmp

        if (linked) then
!           write(6,*) '#1', level, linked
            goto 401 
        else if (level>=NLEVEL) then
            linked=.false.
!           write(6,*) '#5', level, linked
            goto 401
        else if (any(imov_at(:,atom)==0)) then
            linked=.true.
!           write(6,*) '#2', level, linked
            goto 401 
        else if (count_repeat_element(bondss(atom)%bond(1,:),12)==1) then
            level=level+1
            linked=.false.
!           write(6,*) '#3', level, linked
            goto 401 
        else if (count_repeat_element(bondss(atom)%bond(1,:),12)> 1) then
            level=level+1
!           write(6,*) '#4', level, linked, atom0, atom, count_repeat_element(bondss(atom)%bond(1,:),12)
!           do i=1,4
!               if (bondss(atom)%bond(2,i)/=atom0 .and. bondss(atom)%bond(1,i)==12)write(6,*) '#4', i
!           end do
            do i=1,4
                if (bondss(atom)%bond(2,i)/=atom0 .and. bondss(atom)%bond(1,i)==12) then
!                   write(6,*) '##4', level, linked, i
                    atom0_tmp=atom
                    atom_tmp=bondss(atom)%bond(2,i)
                    linked_tmp=.false.
                    level_tmp=level
                    call check_si_si_bond( bondss, atom0_tmp, atom_tmp, imov_at, linked_tmp, level_tmp )
                    linked=linked .or. linked_tmp
                    if (linked) exit
                end if
            end do
            level=level_tmp
            goto 401 
        end if
401     continue
    end subroutine


!   subroutine print_bonds(bondsisio,bondsio,angleosio,anglesiosi,flag)
!       integer,dimension(:,:)                    :: bondsisio
!       integer,dimension(:,:)                    :: bondsio
!       integer,dimension(:,:)                    :: angleosio
!       integer,dimension(:,:)                    :: anglesiosi

!       integer                                   :: i, j
!       character(len=*)                          :: flag


!       open(122,file='SiSi-O.bond.'//trim(flag)//'.dat',form='formatted')
!           do i=1,size(bondsisio,2)
!               write(122,*) (bondsisio(j,i)-1, j=1,3)
!           end do
!       close(122)
!       open(133,file='Si-O.bond.'//trim(flag)//'.dat',form='formatted')
!           do i=1,size(bondsio,2)
!               write(133,*) (bondsio(j,i)-1, j=1,2)
!           end do
!       close(133)
!       open(144,file='O-Si-O.angle.'//trim(flag)//'.dat',form='formatted')
!           do i=1,size(angleosio,2)
!               write(144,*) (angleosio(j,i)-1, j=1,3)
!           end do
!       close(144)
!       open(155,file='Si-O-Si.angle.'//trim(flag)//'.dat',form='formatted')
!           do i=1,size(anglesiosi,2)
!               write(155,*) (anglesiosi(j,i)-1, j=1,3)
!           end do
!       close(155)

!   end subroutine

!   subroutine read_bonds(bondsisio,bondsio,angleosio,anglesiosi,flag)
!       integer,dimension(:,:)                    :: bondsisio
!       integer,dimension(:,:)                    :: bondsio
!       integer,dimension(:,:)                    :: angleosio
!       integer,dimension(:,:)                    :: anglesiosi

!       integer                                   :: i, j
!       character(len=*)                          :: flag

!       open(12,file='SiSi-O.bond.'//trim(flag)//'.dat',form='formatted')
!       do i=1,size(bondsisio,2)
!           read(12,*) (bondsisio(j,i), j=1,3)
!       end do
!       close(12)
!       open(13,file='Si-O.bond.'//trim(flag)//'.dat',form='formatted')
!       do i=1,size(bondsio,2)
!           read(13,*) (bondsio(j,i), j=1,2)
!       end do
!       close(13)
!       open(14,file='O-Si-O.angle.'//trim(flag)//'.dat',form='formatted')
!       do i=1,size(angleosio,2)
!           read(14,*) (angleosio(j,i), j=1,3)
!       end do
!       close(14)
!       open(15,file='Si-O-Si.angle.'//trim(flag)//'.dat',form='formatted')
!       do i=1,size(anglesiosi,2)
!           read(15,*) (anglesiosi(j,i), j=1,3)
!       end do
!       close(15)

!       bondsisio=bondsisio+1
!       bondsio=bondsio+1
!       anglesiosi=anglesiosi+1
!       angleosio=angleosio+1
!   end subroutine


    subroutine print_bonds(bondss,flag)
        type(bonds),dimension(:)                  :: bondss
        character(len=*)                          :: flag

        integer                                   :: i, natom, itype, ind, j

        open(122,file='BONDS.'//trim(flag)//'.dat',form='formatted')
            natom=size(bondss)
            write(122,*) natom
            do i=1,natom
                write(122,*) bondss(i)%ind, bondss(i)%typ
                if (bondss(i)%typ==ATOM_TYPE1) then  ! Si
                    do j=1,4
                        write(122,'(3I12)') bondss(i)%bond(1,j),           &
bondss(i)%bond(2,j),bondss(i)%bond(4,j)
                    end do
                    do j=1,6
                        write(122,'(4I12)') bondss(i)%angle(1,j),          &
bondss(i)%angle(2,j),bondss(i)%angle(3,j), bondss(i)%angle(4,j)
                    end do
!                    do j=1,12
!                        if (bondss(i)%angle2(1,j)==-99) exit
!                        write(122,'(4I12)') bondss(i)%angle2(1,j),         &
!bondss(i)%angle2(2,j),bondss(i)%angle2(3,j), bondss(i)%angle2(4,j)
!                    end do
                end if
                if (bondss(i)%typ==ATOM_TYPE2) then  ! O
                    do j=1,2
                        write(122,'(3I12)') bondss(i)%bond(1,j),           &
bondss(i)%bond(2,j),bondss(i)%bond(4,j)
                    end do
                    write(122,'(4I12)') bondss(i)%angle(1,1),              &
bondss(i)%angle(2,1),bondss(i)%angle(3,1), bondss(i)%angle(4,1)
!                    do j=1,6
!                        if (bondss(i)%angle2(1,j)==-99) exit
!                        write(122,'(4I12)') bondss(i)%angle2(1,j),         &
!bondss(i)%angle2(2,j),bondss(i)%angle2(3,j), bondss(i)%angle2(4,j)
!                    end do
                end if
            end do
        close(12)

    end subroutine


    subroutine read_bonds(bondss,names,flag,angle2)
        type(bonds),dimension(:),allocatable      :: bondss
        integer    ,dimension(:),allocatable      :: names
        character(len=*)                          :: flag

        integer                                   :: i, natom, itype, ind, j
        integer                                   :: btype,a1,a2,a3,fix

        logical                                   :: angle2


        open(12,file='BONDS.'//trim(flag)//'.dat',form='formatted')
            read(12,*) natom
            if (.not. allocated(bondss)) allocate(bondss(natom))
            do i=1,natom
                read(12,*) ind, itype
                bondss(i)%ind=ind
                bondss(i)%typ=itype
                bondss(i)%bond=-99
                bondss(i)%angle=-99
                bondss(i)%angle2=-99
                if (itype==ATOM_TYPE1) then  ! Si
                    do j=1,4
                        read(12,*) btype,a2,fix
                        bondss(i)%bond(:,j)=(/ btype,a2,-99,fix /)
                    end do
                    do j=1,6
                        read(12,*) btype,a2,a3,fix
                        bondss(i)%angle(:,j)=(/ btype,a2,a3,fix /)
                    end do
!                   if (angle2) then
!                       do j=1,4
!                           read(12,*) btype,a2,a3,fix
!                           bondss(i)%angle2(:,j)=(/ btype,a2,a3,fix /)
!                       end do
!                   end if
                end if
                if (itype==ATOM_TYPE2) then  ! O
                    do j=1,2
                        read(12,*) btype,a2,fix
                        bondss(i)%bond(:,j)=(/ btype,a2,-99,fix /)
                    end do
                    read(12,*) btype,a2,a3,fix
                    bondss(i)%angle(:,1)=(/ btype,a2,a3,fix /)
!                   if (angle2) then
!                       do j=1,6
!                           read(12,*) btype,a2,a3,fix
!                           bondss(i)%angle2(:,j)=(/ btype,a2,a3,fix /)
!                       end do
!                   end if
                end if
            end do
        close(12)

        if (.not. angle2) call find_angle2(bondss)

        if ( .not. check_bonds_integrity(bondss,names)) then
            write(6,*) 'Nonconsistent Bonds, exit'
        end if

    end subroutine


    subroutine read_coord(filename,coord,lat,names,natom,iratom,flag)
        character(len=*)                  :: filename
        real*8,dimension(:,:),allocatable :: coord
        integer,dimension(:),allocatable  :: names
        real*8,dimension(3,3)             :: lat
        integer                           :: natom
        integer,dimension(:,:),allocatable :: iratom
        character(len=*),optional         :: flag

        integer                           :: i,j
        real*8                            :: temp
        character(len=256)                :: filename_combine

        if (present(flag)) then
            filename_combine=trim(filename)//'.'//trim(flag)
        else
            filename_combine=trim(filename)
        end if
        open(11,file=trim(filename_combine),form='formatted')
            read(11,*) lat(1,1),lat(1,2),lat(1,3)
            read(11,*) lat(2,1),lat(2,2),lat(2,3)
            read(11,*) lat(3,1),lat(3,2),lat(3,3)
            read(11,*) temp, natom
            allocate( names(natom), coord(3,natom), iratom(3,natom))
            do i=1,natom
                read(11,*) names(i), (coord(j,i), j=1,3), (iratom(j,i), j=1,3)
            end do
        close(11)

    end subroutine

    subroutine print_coord(filename,coord,lat,names,iratom,flag)
        real*8,dimension(:,:)              :: coord
        real*8,dimension(3,3)              :: lat
        integer,dimension(:)               :: names
        character(*)                       :: filename
        integer,dimension(:,:)             :: iratom
        character(len=*)                   :: flag

        integer                             :: i,j

        open(166,file=trim(filename)//'.'//trim(flag),form='formatted')
            do i=1,3
                write(166,*) (lat(i,j),j=1,3)
            end do
            write(166,*) '1.0', size(coord,2)
            do i=1,size(coord,2)
                write(166,'(I4, 3F20.12,3I6)') names(i), (coord(j,i),j=1,3), (iratom(j,i),j=1,3)
            end do
        close(166)

    end subroutine

    subroutine read_temperature(temperature_table_filename,step_table,temperature_table,accepted_steps)
        character(*)                      :: temperature_table_filename
        integer,dimension(:),allocatable  :: step_table
        real*8 ,dimension(:),allocatable  :: temperature_table
        integer                           :: accepted_steps

        integer                           :: i, ntemperature

        open(167,file=trim(temperature_table_filename),form='formatted')
            read(167,*) ntemperature
            allocate(step_table(ntemperature),temperature_table(ntemperature))
            do i=1,ntemperature
                read(167,*) step_table(i), temperature_table(i)
            end do
        close(167)
        accepted_steps=step_table(1)-1

    end subroutine

    subroutine decide_temperature(finished_steps,acceptance_steps, &
temperature_table,step_table,temperature2,jumped_temperature)
        integer,dimension(:),intent(in)        :: step_table
        real*8,dimension(:),intent(in)         :: temperature_table
        integer             ,intent(in)        :: finished_steps
        integer             ,intent(in)        :: acceptance_steps
        real*8                                 :: temperature2
        integer                                :: jumped_temperature

        integer                                :: i

        if (finished_steps>acceptance_steps) then
            do i=1,size(step_table)
                if (finished_steps<step_table(i)) then
                    temperature2=temperature_table(i-1)
                    jumped_temperature=i-1
                    exit
                end if
            end do
        else
            jumped_temperature=0
            temperature2=0.00001d0
        end if
    end subroutine


    SUBROUTINE init_random_seed(my)
        INTEGER :: i, n, clock
        integer,optional :: my
        INTEGER, DIMENSION(:), ALLOCATABLE :: seed
        CALL RANDOM_SEED(size = n)
        ALLOCATE(seed(n))
  
        CALL SYSTEM_CLOCK(COUNT=clock)
  
        if (present(my)) then
            seed =         my
        else
            seed = clock +       37  *  (/ (i  -  1, i = 1, n) /)
        end if
        CALL RANDOM_SEED(PUT = seed)
  
        DEALLOCATE(seed)
    END SUBROUTINE
  
    ! inclusive both ends
    function get_rand_int(i1,i2)
      integer,             intent(in) :: i1,i2
      integer                         :: get_rand_int
      real*8                        :: c_tmp
      call random_number( c_tmp )
      get_rand_int = int(c_tmp*(i2+1-i1))+i1
    end function
  
    function get_rand_real(r1,r2)
      real*8                        :: get_rand_real
      real*8,          intent(in)   :: r1,r2
      real*8                        :: c_tmp
      call random_number( c_tmp )
      get_rand_real = (c_tmp*(r2-r1))+r1
    end function


    subroutine calculate_energy_force(coord,Etot,fatom,bondss,bonds_list,names,lat, &
natom,neigh_table,neigh_table_lat)

        real*8,dimension(:,:),intent(inout)    :: coord        ! coord is cartesian coord
        real*8                                 :: Etot
        real*8,dimension(:,:)                  :: fatom
        type(bonds),dimension(:)               :: bondss
        type(bonds_l)                          :: bonds_list
        integer,dimension(:)                   :: names        ! input
        real*8,dimension(3,3)                  :: lat          ! input
        integer,intent(in)                     :: natom        ! input

        integer,dimension(numneigh,size(names))     :: neigh_table
        integer,dimension(3,numneigh,size(names))   :: neigh_table_lat

        Etot=compute_energy(bonds_list,coord,names,lat,natom,neigh_table,neigh_table_lat)

        fatom=compute_force(bondss,coord,names,lat,natom,neigh_table,neigh_table_lat)

        Etot=Etot/hartree_ev ! change eV to hartree unit
        fatom=-1d0*fatom/hartree_ev/ang2bohr ! change eV/Angstrom to Hartree/Bohr unit; here fatom=\div Etot, no minus sign

    end subroutine


    subroutine relaxation(coord,energy_new,lat,natom,tolforce_ev,epsilon0,irmethod,num_mov, &
num_mov_sd,fatom,imov_at,bondss,bonds_list,names,n_table,nn_table,converged,iteration,inner)
        real*8,dimension(:,:),intent(inout)        :: coord
        real*8,intent(out)                         :: energy_new
        real*8,dimension(3,3)                      :: lat
        integer,intent(in)                         :: natom
        real*8                                     :: tolforce_ev
        real*8                                     :: epsilon0
        integer                                    :: irmethod     
        integer                                    :: num_mov, num_mov_sd
        real*8,dimension(:,:)                      :: fatom
        integer,dimension(:,:)                     :: imov_at
        type(bonds),dimension(:)                   :: bondss
        type(bonds_l)                              :: bonds_list
        integer,dimension(:)                       :: names
        integer,dimension(:,:)                     :: n_table
        integer,dimension(:,:)                     :: nn_table
        integer                                    :: converged, iteration, inner

        real*8,dimension(size(coord,1),size(coord,2)) :: coord_new
        integer                                    :: iteration1, iteration2

        coord_new=coord
        converged=1

        call atomic_relax_standalone(coord_new,lat,natom,tolforce_ev,irmethod, &
num_mov,energy_new,fatom,imov_at,bondss,bonds_list,names,n_table,nn_table,inner,iteration1)

!        if (iteration1>=num_mov) then
!            write(6,'(A10,1F20.12,A)') '        **', energy_new, '    cycle'
!            call relax_sd(bondss,bonds_list,coord_new,lat,names,natom,epsilon0,energy_new, &
!tolforce_ev,n_table,nn_table,num_mov_sd,iteration2)
!            if (iteration2>=num_mov_sd) then
!                energy_new=1e8
!                write(6,'(A10,A,A)') '        **', ' +Infinit ', '   Non-converge, jump to next bond switching'
!                converged=0   ! not converge when 0
!            else
!               coord=coord_new
!               iteration=iteration1+iteration2
!            end if
!         else
!            coord=coord_new
!            iteration=iteration1+iteration2
!         end if
!       coord=coord_new
!        iteration=iteration1
        if (iteration1>=num_mov) then
             write(6,'(A10,E20.10,A)') '    **    ', energy_new, '   Non-converge, jump to next bond switching'
             converged=0   ! not converge when 0
        else
             coord=coord_new
             iteration=iteration1
        end if


    end subroutine
!
!
!
  subroutine atomic_relax_standalone(coord,lat,natom,tolforce_ev,irmethod, &
num_mov,Etot,fatom,imov_at, bondss,bonds_list,names,n_table,nn_table,inner,iteration)
       
      implicit none

      integer natom           !  input: number of atoms
      real*8 coord(3,natom)   !  atomic positions in cartesian coord, this is the input
      real*8 xatom(3,natom)   !  atomic positions in fractional coordinate,
      real*8 lat(3,3)          !  input: Cell lattices in Bohr unit, not changed
      real*8 tolforce_ev      !  input: tolerance of max force (Hartree/Bohr)
      integer irmethod        !  input: 1: CG; 2: BFGS; 3: ion
      integer num_mov         !  input: the maximum number of movement
      real*8  Etot            ! output: total energy
      real*8 fatom(3,natom)   ! output: the final output force (Hartree/Bohr) 
      real*8 force_max_ev      !  output: tolerance of max force (Hartree/Bohr)
      integer imov_at(3,natom)  ! input: 1,1,1, or 0,0,0, or 1 0 0 (etc), to control the movement of each atom 
      integer iteration

      type(bonds),dimension(:)               :: bondss
      type(bonds_l)                          :: bonds_list
      integer,dimension(natom)               :: names        ! input

      integer,dimension(:,:),intent(in)      :: n_table
      integer,dimension(:,:),intent(in)      :: nn_table
      integer,dimension(numneigh,natom)      :: neigh_table
      integer,dimension(3,numneigh,natom)    :: neigh_table_lat
      real*8,dimension(3,3)                  :: reclat

      integer iQijL0_GS, ntype,igga , iwg_out, ivr_rho_out
      integer mov,i, line_step, iislda, kpt, ido_rho, ido_vr
      integer interp
      integer niter1, nline1
      integer xgga, ierr
      integer iforce_cal ! in fact, should be passed 
      integer istress_cal
      integer islda, nkpt, irho_out
      integer istop_line
      integer imv_cont,itest
      integer ivr_out, niter0, nline0
      integer ido_stop, ido_ns
      integer isbf
      integer i1,i2
      integer inode_tot, inode_tmp
      
      real*8 unit_factor_latt,e_ave
      real*8 E_pred, dt_fact_nextlinemin, dd_max, ave_dx, dd_max_tmp
      real*8 fac1,fac2
      real*8 vol_factor,vol0
      real*8 force_max, dt, dtstart, dt_trial
      real*8 Etot_old, Etot_relax
      real*8 Etot_old2,dE_force_ratio,sum
      real*8 convergE_old, convergE
      real*8 fatom_relax(3,natom)
      real*8 tolforce
      real*8 e_str,dd_AL
      real*8 AL(3, 3), ALI(3,3)
      real*8 xatom_old(3,natom)
      real*8 xatom_old2(3,natom),fatom_old2(3,natom)
      real*8 dx1,dx2,dx3,dx,dy,dz,dd,dd_ave,ddx
      real*8 dd_limit
      real*8 tolug, tolE,tolRHO
      real*8 FermidE1(100)
      real*8 FermidE0(100)
      real*8 amx_mth1(100), amx_mth0(100)
      real*8 force_proj,force_proj2,sum2
      real*8 ak1_tmp,ak2_tmp,ak3_tmp
      real*8 voltmp
      real*8 tol_stress
      !real*8 totNel_987, 
      real*8 time00, time11
      character*20 f_tmp, fwg_out(2), fxatom_out
      character*20 fforce_out, fvr_out(2), frho_out(2)
      character*60 message
      character(len=4) filename
      character(len=2) it
      integer inner
      real*8 force_l(natom)
      real*8 maxforce


!!!!  added for atomic relaxation CG/BFGS algorithm.
      real*8,allocatable :: HessianInv(:,:)
      integer iflag, j, k
      real*8  EDIFF, EDIFFG, EACC
      real*8 BL(3,3),AOPT(3,3),BOPT(3,3),AOPT0(3,3)
      real*8 FSIF(3,3),fatoml(3,natom),px(3,natom)
!      integer LSTOP2
      logical LSTOP2
      real*8  EVTOJ, AMTOKG, FACT, E1TEST,FACTSI
      integer iat_max,iat,iat_max_trial
      real*8  w_cg, w_scf
      real*8,allocatable,dimension(:,:) :: drho_nL_st
      real*8,allocatable,dimension(:,:) :: rho_atom_nL_st
      real*8 f1,f2
      real*8 ion_stepsize, dist_tmp
      integer converge_flag

      inode_tot=0
      tolforce=tolforce_ev/hartree_ev/ang2bohr

      AL=transpose(lat)*ang2bohr
      ALI=transpose(rinverse(AL))
      reclat=transpose(rinverse(lat))
      xatom=matmul(reclat,coord)

      call find_neighbors(coord,lat,names,n_table,  &
nn_table,neigh_table,neigh_table_lat)

      xatom_old2=xatom

      ido_stop=0
      fatom_old2=0.d0   ! it is used with wg_scf as stoping criterion
      ido_ns = 1 ! used for LDAU
      
      call calculate_energy_force(coord,Etot,fatom,bondss,bonds_list,names,lat, &
natom,neigh_table,neigh_table_lat)



      fatom_old2=fatom  ! this fatom_old correspond to xatom_old2
      Etot_old2=Etot
      dE_force_ratio=0.d0

      if(irmethod.eq.2) then
         allocate(HessianInv(natom*3,natom*3))
         do i=1,3*natom
         HessianInv(i,i)=1.d0
         enddo
      endif


!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!cccc Now, begin the atomic movements
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      E_pred=0.d0
      dt=1.0d0*dtstart        ! first trial dt    
      dt_trial=dt
      dt_fact_nextlinemin=1.d0
      dd_max=0.d0
      ave_dx=0.d0
!***********************************************************
      if(inode_tot.eq.1) then
        write(6,*) "**** finished input atom config calc.  ***" 
        write(6,*) "**** following are atomic  relaxation  ***"
        write(22,*) "**"
        write(22,*) "**"
        write(22,996) 0, Etot*hartree_ev
        write(22,*) "**** finished input atom config calc.  ***" 
        write(22,*) "**** following are atomic  relaxation  ***"
        write(22,*) "**"
        write(22,*) "**"
      endif
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      force_max=-100.d0
      do i=1,natom
        if(dabs(fatom(1,i)*imov_at(1,i)).gt.force_max) &
         force_max=dabs(fatom(1,i)*imov_at(1,i))
        if(dabs(fatom(2,i)*imov_at(2,i)).gt.force_max) &
         force_max=dabs(fatom(2,i)*imov_at(2,i))
        if(dabs(fatom(3,i)*imov_at(3,i)).gt.force_max) &
         force_max=dabs(fatom(3,i)*imov_at(3,i))
      enddo

!!!!!!! the big do loop for atomic movement
!!!!!!! the big do loop for atomic movement
!!!!!!! the big do loop for atomic movement
   DO 2000 mov=1,num_mov   
              
!     write(filename,fmt='(I4.4)') mov
!      write(6,*) "** atomic relaxation, atom_mov_step: ", mov

!******************* update some quantities

       Etot_old=Etot      ! the _old quantities correspond to xatom_oldx
       convergE_old=convergE
!******************* check whether to stop for small remaining force
         
       force_max=-100.d0
       do i=1,natom
         if(dabs(fatom(1,i)*imov_at(1,i)).gt.force_max) &
          force_max=dabs(fatom(1,i)*imov_at(1,i))
         if(dabs(fatom(2,i)*imov_at(2,i)).gt.force_max) &
          force_max=dabs(fatom(2,i)*imov_at(2,i))
         if(dabs(fatom(3,i)*imov_at(3,i)).gt.force_max) &
          force_max=dabs(fatom(3,i)*imov_at(3,i))
       enddo

      if(force_max.lt.tolforce) then
!       write(6,*) "force_max < tolforce, finished", &
!        force_max*hartree_ev/A_AU_1,tolforce*hartree_ev/A_AU_1
        Etot_relax=Etot/13.6058
        converge_flag=1
        goto 2001  
      endif


!***********************************************************

      iflag=1
      if (mov==1) then
        iflag=0
      endif
      EDIFF=tolE
      EDIFFG=-1.0E-4 
      EACC=MAX(ABS(EDIFF),ABS(convergE))

      AOPT=AL*A_AU_1
      BOPT=ALI/A_AU_1

      LSTOP2=.FALSE.
      EVTOJ=1.60217733E-19
      AMTOKG=1.6605402E-27
      if(ion_stepsize<0.001)then !use default 0.5 when unexpected value input or no value input
        FACT=10*0.5*EVTOJ/AMTOKG *1E-10
      else
        FACT=10*ion_stepsize*EVTOJ/AMTOKG *1E-10 
      endif


      do I=1,3
       do J=1,natom
        fatom_relax(i,j)=-fatom(i,j)*FACT*27.211396/A_AU_1   !old version wrong. only *, not **
        fatom_relax(i,j)=fatom_relax(i,j)*imov_at(i,j)
       enddo
      enddo 
      Etot_relax=Etot/13.6058;    ! why divide, should it be multiple?

      FSIF =0.d0
      unit_factor_latt=0.d0  ! fix AL

      vol_factor=1
      FACTSI=0.d0
      inode_tmp=1
   

      if(irmethod.eq.1) then
       call atomMV_CG(iflag,natom,Etot_relax,AOPT,BOPT,1,xatom,      &
       xatom_old,FACT,fatom_relax,FACTSI,FSIF,fatoml,px,dd_max_tmp,22 &
       ,6,EACC,EDIFFG,E1TEST,LSTOP2,dt,dt_trial,inode_tmp, imov_at)
      elseif (irmethod.eq.2) then
       call atomMV_BFGS(iflag,natom,Etot_relax,AOPT,BOPT,1,xatom,&
       xatom_old,FACT,fatom_relax,0.0,FSIF,fatoml,px,dd_max,22,6,&
       EACC,EDIFFG,E1TEST,LSTOP2,inode_tot, imov_at,HessianInv)
      elseif (irmethod.eq.3) then
       call IONSD(iflag,natom,Etot_relax,AOPT,BOPT,1,xatom,            &
       xatom_old,FACT,fatom_relax,0.0,FSIF,fatoml,px,dd_max_tmp,22,6,  &
       EACC,EDIFFG,E1TEST,LSTOP2,dt,dt_trial,inode_tot, imov_at)
      else
       write(6,*) "irmethod.ne.1,2,3 stop", irmethod
       stop
      endif


!ccc**********************************
!ccc    When iflag=0 (input), the xatom-xatom_old, are not in the same direction as in the 
!ccc    subsequent correction direction !
!ccccccccccccc
!ccccc  It does not use the input for iflag (except for the very first one). However, it gives a output for iflag
!cccc    It has an internal, saved LTRIAL flag to know things internally
!ccccc   output: iflag=1  or 2: from xatom_old to xatom is a new direction (called trial),
!ccccc                          for 2, it suggests the program can be stopped now. But we are not use that
!ccccc           iflag=0: from xatom_old to xatom is along the same step as before (correction)                    

          !! output the RELAXSTEPS and "MOVEMENT" files.
!          converge_flag=0
!          call output_fxatom_out(converge_flag)      


!ccccccccccc  xatmo_old2,fatom_old2,Etot_old2 record the values at the beginning of a
!ccccccccccc new line minization
           if(iflag.ne.0) then       ! this is a new direction
             xatom_old2=xatom_old
             fatom_old2=fatom
             Etot_old2=Etot
           endif

!ccccccccc   Not clear from atomMV_CG, what is the definition of dd_max, and when it is defined, when it is not
!ccccccccc   we will use xatom (the new position), xatom_old to define dd_max here
            dd_max=0.d0
            dd_ave=0.d0
            iat_max=0
            do iat=1,natom
              dx1=xatom(1,iat)-xatom_old2(1,iat)
              if(abs(dx1+1).lt.abs(dx1)) dx1=dx1+1
              if(abs(dx1-1).lt.abs(dx1)) dx1=dx1-1
              dx2=xatom(2,iat)-xatom_old2(2,iat)
              if(abs(dx2+1).lt.abs(dx2)) dx2=dx2+1
              if(abs(dx2-1).lt.abs(dx2)) dx2=dx2-1
              dx3=xatom(3,iat)-xatom_old2(3,iat)
              if(abs(dx3+1).lt.abs(dx3)) dx3=dx3+1
              if(abs(dx3-1).lt.abs(dx3)) dx3=dx3-1
              dx=AL(1,1)*dx1+AL(1,2)*dx2+AL(1,3)*dx3
              dy=AL(2,1)*dx1+AL(2,2)*dx2+AL(2,3)*dx3
              dz=AL(3,1)*dx1+AL(3,2)*dx2+AL(3,3)*dx3
              dd=dx**2+dy**2+dz**2
              dd_ave=dd_ave+dd
              if(dd.gt.dd_max) then
                dd_max=dd
                iat_max=iat
              endif
            enddo


!cccccccccccccccccccccccccccccccccccccccccccccc

            dd_max=dsqrt(dd_max)
            dd_ave=dsqrt(dd_ave/natom)

!cccccccccccccc  iflag.ne.0 is the new direction
            if(iflag.ne.0) then  ! for iflag.eq.0, correction, keep use iat_max_trial  !!!!! IFLAG!=0 is correct, Zhanghui 2015/3/17
              iat_max_trial=iat_max
            endif

            iat=iat_max_trial
            dx1=xatom(1,iat)-xatom_old2(1,iat)
            if(abs(dx1+1).lt.abs(dx1)) dx1=dx1+1
            if(abs(dx1-1).lt.abs(dx1)) dx1=dx1-1
            dx2=xatom(2,iat)-xatom_old2(2,iat)
            if(abs(dx2+1).lt.abs(dx2)) dx2=dx2+1
            if(abs(dx2-1).lt.abs(dx2)) dx2=dx2-1
            dx3=xatom(3,iat)-xatom_old2(3,iat)
            if(abs(dx3+1).lt.abs(dx3)) dx3=dx3+1
            if(abs(dx3-1).lt.abs(dx3)) dx3=dx3-1
            if(abs(dx1).ge.0.913*abs(dx2).and.abs(dx1) &
             .ge. 0.913*abs(dx3)) then
             dx=dx1
            elseif(abs(dx2).ge.0.913*abs(dx3)) then
             dx=dx2
            else
             dx=dx3
            endif
            dd_max=dd_max*dx/(abs(dx)+1.D-15)   ! give a sense for its direction
            ddx=dx

!cccccc dd_max is the distance from the original point of this line_miniz: xatom_old2 \
!cccccc to the current position
!cccccccccccc use dx as the direction and step length 
!ccccccccccccccccccccccccccccccccccccc
!ccc    When iflag=0 (input), the xatom-xatom_old, are not in the same direction as in the 
!ccc    subsequent correction direction !
!cccccccccccccccccccccccccccccccccccc


!***************************************************************
!          if(inode_tot.eq.1) then
!             write(6,*) "**** within atom_mov_step:", mov
!             write(6,994) dt,dd_max,force_max*hartree_ev/A_AU_1
!          endif
994       format(" dt= ",E10.4,", dd_max=",E10.4,", force_max=",E10.4)
!***************************************************************
!ccccc for IFLAG=1, 2: (trial, first step) store the initial value of this line minimization (the value at xatom_old)
!ccccc for IFLAG=0: (correction step) do interpolation before Etotcal

!     call MPI_Bcast(xatom,natom*3,MPI_REAL8,0,MPI_COMM_WORLD,ierr)

      coord=matmul(transpose(lat),xatom)  ! coord with cartesian coord
      if (mod(mov,update_neighbor)==0) then
          call find_neighbors(coord,lat,names,n_table, &
nn_table,neigh_table,neigh_table_lat)
      end if


      call calculate_energy_force(coord,Etot,fatom,bondss,bonds_list,names,lat, &
natom,neigh_table,neigh_table_lat)
!     write(6,*)  mov


!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
          sum=0.d0
          do iat=1,natom
            dx1=xatom(1,iat)-xatom_old2(1,iat)
            if(abs(dx1+1).lt.abs(dx1)) dx1=dx1+1
            if(abs(dx1-1).lt.abs(dx1)) dx1=dx1-1
            dx2=xatom(2,iat)-xatom_old2(2,iat)
            if(abs(dx2+1).lt.abs(dx2)) dx2=dx2+1
            if(abs(dx2-1).lt.abs(dx2)) dx2=dx2-1
            dx3=xatom(3,iat)-xatom_old2(3,iat)
            if(abs(dx3+1).lt.abs(dx3)) dx3=dx3+1
            if(abs(dx3-1).lt.abs(dx3)) dx3=dx3-1
            dx=AL(1,1)*dx1+AL(1,2)*dx2+AL(1,3)*dx3
            dy=AL(2,1)*dx1+AL(2,2)*dx2+AL(2,3)*dx3
            dz=AL(3,1)*dx1+AL(3,2)*dx2+AL(3,3)*dx3
            sum=sum+dx*(fatom(1,iat)+fatom_old2(1,iat))/2+ &
             dy*(fatom(2,iat)+fatom_old2(2,iat))/2+ &
             dz*(fatom(3,iat)+fatom_old2(3,iat))/2
          enddo
          dE_force_ratio=(Etot-Etot_old2)/sum

!cccccccccccc    xatom, fatom, xatom_old2,fatom_old2

!************ calculate force_max just for the report
         force_max=-100.d0
         force_proj=0.d0
         force_proj2=0.d0    ! this is the projection using the initial force fatom_old2 at the beginning of line miniz
         sum2=0.d0
         do i=1,natom
           if(dabs(fatom(1,i)*imov_at(1,i)).gt.force_max) &
              force_max=dabs(fatom(1,i)*imov_at(1,i))
           if(dabs(fatom(2,i)*imov_at(2,i)).gt.force_max) &
              force_max=dabs(fatom(2,i)*imov_at(2,i))
           if(dabs(fatom(3,i)*imov_at(3,i)).gt.force_max) &
              force_max=dabs(fatom(3,i)*imov_at(3,i))

           force_proj=force_proj+fatom(1,i)*imov_at(1,i)* &
            px(1,i)+fatom(2,i)*imov_at(2,i)*px(2,i)+      &
            fatom(3,i)*imov_at(3,i)*px(3,i)
           force_proj2=force_proj2+fatom_old2(1,i)*imov_at(1,i)*  &
            px(1,i)+fatom_old2(2,i)*imov_at(2,i)*px(2,i)+         &
            fatom_old2(3,i)*imov_at(3,i)*px(3,i)
           sum2=sum2+px(1,i)**2+px(2,i)**2+px(3,i)**2
         enddo

         force_proj=force_proj/dsqrt(sum2)
         force_proj2=force_proj2/dsqrt(sum2)

!***************************************************************

         if(inode_tot.eq.1) then
!            write(6,*) "================================="
!             write(6,*) 
!             write(6,*) "**** Final result for atom_mov_step: ",mov
!             write(6,998) dd_max,force_max*hartree_ev/A_AU_1
!             write(6,996) mov, Etot*hartree_ev
             write(6,999) mov,iflag,Etot*hartree_ev,dd_max,force_max*hartree_ev/A_AU_1,force_proj
!             write(6,*) "================================="
999          format(i4,2x,i2,2x,"Etot= ",E17.10, 2x, "dd= ", E10.3, 2x, "fmax= ", E10.3,2x, "proj= ", E17.10)
995          format(" **** E_prev,E_pred,E_final", 3x, 3(E23.15,1x))
997          format(" ****      dt= ", E9.3, ",   line_step= ",i3, &
              ",  dt_incr_next_step= ", E9.3)
998          format(" ****  dd_max= ", E9.3, ",   max_force= ",E9.3)
996          format(" RESULT: atom_move_step, E_tot: ", i4,1x,E25.15)
         endif
!*********************************************************
!*******  output everything after each atomic movements
!******* xatom, fatom_all corresponds to the current step
!******* xatom_old2, fatom_old2 correspond to the value at the begining of this step
!******* dd_max correspond to xatom-xatom_old2
         converge_flag=0
!         call output_fxatom_out(converge_flag)
!*********************************************************
2000     continue  
!***************************************************************
!****   end of the atomic movement
!***************************************************************

2001   continue     ! exit point



       !********* transform back to cartesian ***********
       coord=matmul(transpose(lat),xatom)
       Etot=Etot*hartree_ev
       fatom=fatom*hartree_ev/A_AU_1
       iteration=mov
       !*************************************************


       !close(73)
       if(irmethod.eq.2) then
       deallocate(HessianInv)
       endif

!      write(6,*)  mov
!***************************************************************
!****   end of the atomic movement
!***************************************************************
       return 
      end subroutine


       


end module sd
